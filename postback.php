<?php
/*
 * Gilbarco NMI LPG Form Postback
 * Copyright (c) 2016 Verified Pty Ltd
 * All rights reserved.
 */

/** Include config */
require_once('config/config.php');
/** Include database config */
require_once('config/db.config.php');
/** Include classes */
require_once('classes/class.gilbarconmi_lpg.php');
require_once('classes/class.gilbarconmi_lpg_excel_sheet.php');
/** Include Php library */
//require_once('phpLibs/lzwCompression/lzwc.php');

// Check if postback?
if ($_SERVER['REQUEST_METHOD'] == 'POST' || isset($_REQUEST['ex_gilbarco_veeder_root_non_lpg'])) {

//die($_POST['param']);
/*
  header("Content-Type: text/plain");
  $l = new LZW;
  $de = $l->decode($_POST['param']);
  die($de);
*/

  $data = array();
  $form = new Gilbarconmi_lpg();
    if (stristr($_POST['param'], '*;')) {
    $array_fields = explode('*;', $_POST['param']);
  } else {
    $array_fields = explode('~~', $_POST['param']);
  }
  foreach ($array_fields as $field => $value) {
    $array_field = explode('|', $value);
    $data[$array_field[0]] = $array_field[1];
  }
  $data['QA_276'] = $_POST['signature_param'];
  $data['GPSlocation'] = $_POST['loc'];

  // Output data backup flatfile.
  $form->outputDataBackupFlatfile($data);

  // Decide whether https or http?
  $ssl      = ( ! empty( $_SERVER['HTTPS'] ) && $_SERVER['HTTPS'] == 'on' );
  $sp       = strtolower( $_SERVER['SERVER_PROTOCOL'] );
  $protocol = substr( $sp, 0, strpos( $sp, '/' ) ) . ( ( $ssl ) ? 's' : '' );

  // Get script name's path and then recreate a new return url path
  $array_script_name = explode('/', $_SERVER['SCRIPT_NAME']);
  if (sizeof($array_script_name) > 1) {
    $script_name_path = '';
    for ($i = 0; $i < sizeof($array_script_name) - 1; ++$i) {
      if ($i > 0) {
        $script_name_path .= '/';
      }
      $script_name_path .= $array_script_name[$i];
    }
    $excel_sheet_url = $protocol . '://' . $_SERVER['HTTP_HOST'] . $script_name_path . '/generate_excel_sheet.php?actno=' . urlencode($data['QA_002']);
    $pdf_url = $protocol . '://' . $_SERVER['HTTP_HOST'] . $script_name_path . '/html2pdf_lpg.php?actno=' . urlencode($data['QA_002']);
  } else {
    $excel_sheet_url = $protocol . '://' . $_SERVER['HTTP_HOST'] . '/generate_excel_sheet.php?actno=' . urlencode($data['QA_002']);
    $pdf_url = $protocol . '://' . $_SERVER['HTTP_HOST'] . '/html2pdf_lpg.php?actno=' . urlencode($data['QA_002']);
  }

  $excel_sheet_url = str_replace('s://' . DOMAIN_NAME,'://localhost', strtolower($excel_sheet_url));
  $pdf_url = str_replace('s://' . DOMAIN_NAME,'://localhost', strtolower($pdf_url));
  $excel_sheet_url = str_replace('://' . DOMAIN_NAME,'://localhost', strtolower($excel_sheet_url));
  $pdf_url = str_replace('://' . DOMAIN_NAME,'://localhost', strtolower($pdf_url));

  $excel_sheet_url = str_replace('s://prtl.verified.com.au','://localhost', strtolower($excel_sheet_url));
  $pdf_url = str_replace('s://prtl.verified.com.au','://localhost', strtolower($pdf_url));
  $excel_sheet_url = str_replace('://prtl.verified.com.au','://localhost', strtolower($excel_sheet_url));
  $pdf_url = str_replace('://prtl.verified.com.au','://localhost', strtolower($pdf_url));

  $excel_sheet_url = str_replace('s://melb.verified.com.au','://localhost', strtolower($excel_sheet_url));
  $pdf_url = str_replace('s://melb.verified.com.au','://localhost', strtolower($pdf_url));
  $excel_sheet_url = str_replace('://melb.verified.com.au','://localhost', strtolower($excel_sheet_url));
  $pdf_url = str_replace('://melb.verified.com.au','://localhost', strtolower($pdf_url));

  $excel_sheet_url = str_replace('s://nsw.verified.com.au','://localhost', strtolower($excel_sheet_url));
  $pdf_url = str_replace('s://nsw.verified.com.au','://localhost', strtolower($pdf_url));
  $excel_sheet_url = str_replace('://nsw.verified.com.au','://localhost', strtolower($excel_sheet_url));
  $pdf_url = str_replace('://nsw.verified.com.au','://localhost', strtolower($pdf_url));

  $excel_sheet_url = str_replace('s://www.verified.com.au','://localhost', strtolower($excel_sheet_url));
  $pdf_url = str_replace('s://www.verified.com.au','://localhost', strtolower($pdf_url));
  $excel_sheet_url = str_replace('://www.verified.com.au','://localhost', strtolower($excel_sheet_url));
  $pdf_url = str_replace('://www.verified.com.au','://localhost', strtolower($pdf_url));

  $excel_sheet_url = str_replace('s://verified.com.au','://localhost', strtolower($excel_sheet_url));
  $pdf_url = str_replace('s://verified.com.au','://localhost', strtolower($pdf_url));
  $excel_sheet_url = str_replace('://verified.com.au','://localhost', strtolower($excel_sheet_url));
  $pdf_url = str_replace('://verified.com.au','://localhost', strtolower($pdf_url));

  // Save form fields
  $array_return = $form->saveData($data);
  $return_id = $array_return[0];
  $add_new = $array_return[1];
  if ($return_id) {

    $cls_excel_sheet = new Gilbarconmi_lpg_excel_sheet();
    $excel_filename =  $cls_excel_sheet->makeUpExcelSheetFileName($data['QA_002']);
    $pdf_filename = $form->makeUpPDFFileName($data['QA_002']);

    $excel_sheet_url .= '&filename=' . urlencode($excel_filename) . '&id=' . $return_id . '&addnew=' . $add_new;
    $pdf_url .= '&filename=' . urlencode($pdf_filename) . '&id=' . $return_id . '&addnew=' . $add_new;

    // Generate Excel Sheet
    $x_excel = 'c:\SiteShoter\CutyCapt.exe --url="' . $excel_sheet_url . '" ';
    $x_excel .= '--min-width=440 --delay=12500 --out=' . dirname(__FILE__) . '\outputs\excel\excelsheet_dummy_screenshot.png';
    //exec($xpdf.' > /dev/null 2>/dev/null &');
    pclose(popen("start " . $x_excel, "r"));

    // Generate PDF
    $x_pdf = 'c:\SiteShoter\CutyCapt.exe --url="' . $pdf_url . '" ';
    $x_pdf .= '--min-width=440 --delay=26500 --out=' . dirname(__FILE__) . '\outputs\pdf\pdf_dummy_screenshot.png';
    //exec($xpdf.' > /dev/null 2>/dev/null &');
    pclose(popen("start " . $x_pdf, "r"));

    $return_id = str_pad($return_id, 7, '0', STR_PAD_LEFT);
    if (stristr(strtolower($_SERVER['HTTP_HOST']), 'verified.com.au')) {
      $return_data = array('answer' => 'OK|' . $return_id . '|http://verified.com.au/GilbarcoNMI_lpg|' . urlencode($data['QA_002']));
    } else {
      if (sizeof($array_script_name) > 1) {
        $return_data = array('answer' => 'OK|' . $return_id . '|' . $protocol . '://' . $_SERVER['HTTP_HOST'] . $script_name_path . '|' . $excel_sheet_url . '|' . $pdf_url);
      } else {
        $return_data = array('answer' => 'OK|' . $return_id . '|' . $protocol . '://' . $_SERVER['HTTP_HOST'] . '|' . $excel_sheet_url . '|' . $pdf_url);
      }
    }
  } else {
    if (stristr(strtolower($_SERVER['HTTP_HOST']), 'verified.com.au')) {
      $return_data = array('answer' => 'FAIL|0|' . $protocol . '://' . 'verified.com.au/GilbarcoNMI_lpg');
    } else {
      if (sizeof($array_script_name) > 1) {
        $return_data = array('answer' => 'FAIL|0|' . $protocol . '://' . $_SERVER['HTTP_HOST'] . $script_name_path);
      } else {
        $return_data = array('answer' => 'FAIL|0|' . $protocol . '://' . $_SERVER['HTTP_HOST']);
      }
    }
  }

  header('Content-Type: application/json');
  echo json_encode($return_data);

}

/* End of file postback.php */
/* Location: ./postback.php */