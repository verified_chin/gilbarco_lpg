<?php
/*
 * Gilbarco NMI LPG Generate Excel Sheets
 * @desc To generating LPG's Excelsheet report.
 * @author Chin Lim
 * Copyright (c) 2016 Verified Pty Ltd
 * All rights reserved.
 */
/*
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
*/
/** Include database config */
require_once('config/config.php');
require_once('config/db.config.php');
/** Include classes */
require_once('classes/class.gilbarconmi_lpg_excel_sheet.php');

// Output Excel sheet

// Get query string
// Confirmation No.
@$id = $_GET['id'];
if ( ! isset($id)) {
  if (strlen(@$argv[1])) {
    @$id = $argv[1];
  } else {
    echo 'id (Confirmation No.) is missing...';
    die;
  }
}
// Activity No.
@$act_no = $_GET['actno'];
if ( ! isset($act_no)) {
  if (strlen(@$argv[2])) {
    @$act_no = $argv[2];
  } else {
    echo 'actno (Activity No.) is missing...';
    die;
  }
}
// Excelsheet filename
@$filename = $_GET['filename'];
if ( ! isset($filename)) {
  if (strlen(@$argv[3])) {
    @$filename = $argv[3];
  } else {
    echo 'filename is missing...';
    die;
  }
}
// Wether to overwrite Excelsheet filename field?
@$add_new = $_GET['addnew'];
if ( ! isset($add_new)) {
  if (strlen(@$argv[4])) {
    @$add_new = $argv[4];
  } else {
    echo 'addnew (Whether to overwrite Excesheet field) is missing...';
    die;
  }
}
if (trim($id) != '' && trim($act_no) != '' && trim($filename) != '' && trim($add_new) != '') {
  $phpExcel = new Gilbarconmi_lpg_excel_sheet();
  $phpExcel->setExcelSheetFilename($filename);
  $phpExcel->generateExcelSheet($id, $act_no, $add_new);
} else {
  echo 'Error, missing one or more query strings.';
}

/* End of file generate_excel_sheets.php */
/* Location: ./generate_excel_sheets.php */