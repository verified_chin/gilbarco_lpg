<?php
/*
 * Configurations
 * Copyright (c) 2018 Verified Pty Ltd
 * All rights reserved.
 */

//define('DOMAIN_NAME', '192.168.14.21');
define('DOMAIN_NAME', 'uat.verified.com.au');
define('IS_WINDOWS_SERVER', TRUE);

if (IS_WINDOWS_SERVER === TRUE) {
  // Windows server
  //PHP exe's path
  define('PHP_EXE', 'C:/Program Files (x86)/PHP/v5.6/php.exe');
  // Site's path
  define('WEBSERVER_PATH', 'F:/WebServer/GilbarcoNMI/gilbarco_lpgv2/');
} else {
  // Lynx server
  //PHP exe's path
  define('PHP_EXE', '/usr/bin/php');
  // Site's path
  define('WEBSERVER_PATH', __DIR__ . '/../');
}