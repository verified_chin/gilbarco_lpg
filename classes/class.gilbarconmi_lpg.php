<?php
/*
 * Gilbarco NMI LPG Form's postback class
 * @author Chin Lim
 * Copyright (c) 2016 Verified Pty Ltd
 * All rights reserved.
 */

class Gilbarconmi_lpg {

  // Private fields

  // Number of QA_ fields
  // Fields array
  public $fields = array();
  private $conn;
  private $errors = array();

  // Class constructor
  public function __construct() {

    $this->connectedDatabase();
  }

  // Class destructor
  public function __destruct() {

    // Close database connnection
    $this->closeDatabaseConnection();
    if (count($this->errors)) {
      // Output error log
      $this->printCustomErrorLog();
    }
  }

  /**
   * filterXSS
   * @access  public
   * @desc  Filter for XSS
   */
  public function filterXSS($field_name) {

    return htmlspecialchars($this->fields[$field_name], ENT_QUOTES);
  }

  /**
   * getDBConnectionStringParam
   * @access  private
   * @param Connection String's parameter name
   * @desc  Get the database connection string parameter
   */
  private function getDBConnectionStringParam($arg) {

    global $dsn;

    $return = '';
    $array = explode(';', $dsn);
    foreach ($array as $index => $value) {
      $array_inner = explode('=', $value);
      if (count($array_inner) > 1) {
        if (strcasecmp($array_inner[0], $arg) === 0) {
          $return = $array_inner[1];
        }
      }
    }

    if (strcasecmp($arg, 'database') === 0) {
      $return = trim($return);
      if (substr($return, 0, 1) == '[') {
        $return = substr($return, 1, strlen($return) - 2);
      }
    }
    return $return;
  }

  /**
   * connectedDatabase
   * @access  private
   * @desc  Connect Gilbarco NMI LPG's database.
   */
  private function connectedDatabase() {

    $success = FALSE;

    // Database host
    $db_host = $this->getDBConnectionStringParam('server');
    // Database name
    $db_name = $this->getDBConnectionStringParam('database');
    // Database user ID
    $db_user = $this->getDBConnectionStringParam('uid');
    // Database user password
    $db_pass = $this->getDBConnectionStringParam('pwd');

    try {
      //$this->conn = new PDO('sqlsrv:Server=' . SERVER_NAME . ';Database=' . DB_NAME, DB_USER, DB_PASSWORD);
      //$this->conn = new PDO('dblib:host=' . $db_host . ';dbname=' . $db_name, $db_user, $db_pass);
      if (IS_WINDOWS_SERVER === true) {
        $this->conn = new PDO('sqlsrv:Server=' . $db_host . ';Database=' . $db_name, $db_user, $db_pass);
      } else {
        $this->conn = new PDO('dblib:host=' . $db_host . ';dbname=' . $db_name, $db_user, $db_pass);
      }
      $success = TRUE;
    } catch (PDOException $e) {
      $this->errors[] = date("l jS \of F Y h:i:s A") . ' Connection failed: ' . $e->getMessage();
    }

    return $success;
  }

  /**
   * connect_database
   * @access  private
   * @desc  Connect Gilbarco NMI LPG's database.
   */
  private function closeDatabaseConnection() {

    $this->conn = null;

  }

  /**
   * freeQueryResult
   * @access  private
   * @desc  Connect Gilbarco NMI LPG's database.
   */
  private function freeQueryResult($stmt) {

    $stmt = null;

  }

  /**
   * printCustomErrorLog
   * @access  private
   * @desc  Print custom error log file.
   */
  private function printCustomErrorLog() {

    $h = fopen(__DIR__ . '\..\errorLogs\errorlog'. date('Y-m-d') . '_' . substr(strtolower(preg_replace("/[^A-Za-z0-9_]/", '', $this->fields['QA_002'])), 0, 25) . '.txt', 'a'); // Open a file
    fwrite($h, PHP_EOL);
    fwrite($h, print_r($this->errors, TRUE) . PHP_EOL); // Write output
    fwrite($h, 'Occurred on: ' . date('Y-m-d H:i:s') . PHP_EOL);
    fclose($h); // Close file

  }

 /**
   * getTechProfile
   * @access  public
   * @desc  Load from database.
   */
  public function getTechProfile($act_no) {

    $return = FALSE;
    $stmt = $this->conn->prepare(
      'SELECT TOP 1 SiteCode, MailActivity.ContractorName, ClientName, SiteAddress, SiteName, SiteAddress, VerifierNo, Make, Model, SerialNo, FlowRateRange, RegulationCertNo, CertExpiryDate, JobNo, '
        . 'StreetName, Suburb, Postcode, OwnerStreetName, OwnerSuburb, OwnerPostcode, OwnerABN, GilbarcoCodeRef '
        . 'FROM [' . DB_NAME . '].[dbo].[MailActivity] '
        . 'LEFT JOIN [' . DB_NAME . '].[dbo].[Tech_Profile] ON [' . DB_NAME . '].[dbo].[MailActivity].ContractorNo = [' . DB_NAME . '].[dbo].[Tech_Profile].ContractorNo '
        . 'WHERE ActivityNo = :act_no ORDER BY MailId DESC, ProfileID DESC'
      );
    $stmt->bindValue(':act_no', $act_no, PDO::PARAM_STR);
    $stmt->execute();
    if ($stmt->rowCount()) {
      $this->fields = $stmt->fetch(PDO::FETCH_ASSOC);
      $return = TRUE;
    }
    $this->freeQueryResult($stmt);
    return $return;

  }

  /**
   * fromPost
   * @access  private
   * @desc  Load all postback fields to class variable.
   */
  private function fromPost($data) {

    $this->fields = array_merge($this->fields, $data);

  }

  /**
   * saveSignatureImage
   * @access  private
   * @desc  Save Signature Image File.
   */
/*
  private function saveSignatureImage($id) {

    $data = substr($this->fields['QA_276'], strpos($this->fields['QA_276'], ',') + 1);
    $decoded_data = base64_decode($data);
    $fp = fopen(__DIR__ . '\..\outputs\signatures\sig_' . $id . '_' . substr(strtolower(preg_replace("/[^A-Za-z0-9_]/", '', $this->fields['QA_002'])), 0, 25) . '.png', 'wb');
    fwrite($fp, $decoded_data);
    fclose($fp);

  }
*/

  /**
   * save
   * @access  private
   * @desc  Save Gilbarco NMI LPG form captures.
   */
  private function save() {

    $add_new = 1;
    $edit_id = 0;
    /* Commented out as found many contractors are able to finish the online form and submit within 3 minutes, CTL 26/08/2019
    $sql = 'SELECT TOP 1 id, CONVERT(datetime, created_date, 105) created_date '
            . 'FROM [' . DB_NAME . '].[dbo].[gilbarconmi_lpg] '
            . 'WHERE QA_002 = :act_no AND IncrementNumber = :IncrementNumber '
            . 'ORDER BY id DESC';
    $stmt = $this->conn->prepare($sql);
    $stmt->bindValue(':act_no', $this->fields['QA_002'], PDO::PARAM_STR);
    $stmt->bindValue(':IncrementNumber', $this->fields['IncrementNumber'], PDO::PARAM_STR);
    $stmt->execute();
    if ($stmt->rowCount()) {
      $row1 = $stmt->fetch(PDO::FETCH_ASSOC);

      $stmt = $this->conn->prepare( 'SELECT CONVERT(datetime, GETDATE(), 105) AS current_date_time' );
      $stmt->execute();
      $row2 = $stmt->fetch(PDO::FETCH_ASSOC);
      $stmt = $this->conn->prepare( "SELECT DATEDIFF(mi, '" . $row1['created_date'] . "' , '" . $row2['current_date_time'] . "') AS diff_minutes" );
      $stmt->execute();
      $row = $stmt->fetch(PDO::FETCH_ASSOC);
      if ($row['diff_minutes'] <= 3) {
        $edit_id = $row1['id'];
        $add_new = 0;
      }
    }
    */

    $temp_array1 = array();
    $temp_array2 = array();
    $temp_array3 = array();
    $success = FALSE;

    if ($add_new) {
      // Add new

      // Create SQL insert string
      $display_sql_field_values1 = '';
      $display_sql_field_values2 = '';
      $display_sql_field_values3 = '';
      $sql_field_names = '';
      $sql_field_values = '';
      $sql_field_names1 = '';
      $sql_field_values1 = '';
      $sql_field_names2 = '';
      $sql_field_values2 = '';
      $sql_field_names3 = '';
      $sql_field_values3 = '';
      foreach ($this->fields as $field => $value) {
        // Field name
        $array_field_name = explode('_', $field);
        if (count($array_field_name) > 1) {
          if (strtoupper($array_field_name[0]) == 'QA' && count($array_field_name) == 2) {
            if (is_numeric($array_field_name[1])) {
              $qa_seq = (int)$array_field_name[1];
              if ($qa_seq > 0 && $qa_seq <= 100) {
                $sql_field_names1 .= $field . ', ';
                // Field value
                $sql_field_values1 .= ':' . $field . ', ';
                $temp_array1[':' . $field] = $value;
                $display_sql_field_values1 .= "'" . $value . "', ";
              } else if ($qa_seq > 100 && $qa_seq <= 200) {
                $sql_field_names2 .= $field . ', ';
                // Field value
                $sql_field_values2 .= ':' . $field . ', ';
                $temp_array2[':' . $field] = $value;
                $display_sql_field_values2 .= "'" . $value . "', ";
              } else if ($qa_seq > 200 && $qa_seq <= 300) {
                $sql_field_names3 .= $field . ', ';
                // Field value
                $sql_field_values3 .= ':' . $field . ', ';
                $temp_array3[':' . $field] = $value;
                $display_sql_field_values3 .= "'" . $value . "', ";
              }
            } else {
              $sql_field_names1 .= $field . ', ';
              // Field value
              $sql_field_values1 .= ':' . $field . ', ';
              $temp_array1[':' . $field] = $value;
              $display_sql_field_values1 .= "'" . $value . "', ";
            }
          } else {
            $sql_field_names1 .= $field . ', ';
            // Field value
            $sql_field_values1 .= ':' . $field . ', ';
            $temp_array1[':' . $field] = $value;
            $display_sql_field_values1 .= "'" . $value . "', ";
          }
        } else {
          $sql_field_names1 .= $field . ', ';
          // Field value
          $sql_field_values1 .= ':' . $field . ', ';
          $temp_array1[':' . $field] = $value;
          $display_sql_field_values1 .= "'" . $value . "', ";
        }
      }

      $sql_field_values1 = substr($sql_field_values1, 0, strlen($sql_field_values1) - 2);
      $sql_field_names1 = substr($sql_field_names1, 0, strlen($sql_field_names1) - 2);
      $sql_field_values2 = substr($sql_field_values2, 0, strlen($sql_field_values2) - 2);
      $sql_field_names2 = substr($sql_field_names2, 0, strlen($sql_field_names2) - 2);
      $sql_field_values3 = substr($sql_field_values3, 0, strlen($sql_field_values3) - 2);
      $sql_field_names3 = substr($sql_field_names3, 0, strlen($sql_field_names3) - 2);

      $display_sql_field_values1 = substr($display_sql_field_values1, 0, strlen($display_sql_field_values1) - 2);
      $display_sql_field_values2 = substr($display_sql_field_values2, 0, strlen($display_sql_field_values2) - 2);
      $display_sql_field_values3 = substr($display_sql_field_values3, 0, strlen($display_sql_field_values3) - 2);

      try {
        // Execute SQL
        $sql = 'INSERT INTO [' . DB_NAME . '].[dbo].[gilbarconmi_lpg] (' . $sql_field_names1 . ') VALUES(' . $sql_field_values1 . ')';
        $display_sql = 'INSERT INTO [' . DB_NAME . '].[dbo].[gilbarconmi_lpg] (' . $sql_field_names1 . ') VALUES(' . $display_sql_field_values1 . ')';
        $stmt = $this->conn->prepare( $sql );
        $this->conn->beginTransaction();
        $result = $stmt->execute( $temp_array1 );
        if ( ! $result) {
          $this->conn->rollback();
          $this->errors[] = '1:' . $display_sql . ';'. $this->conn->errorInfo();
          $this->freeQueryResult($stmt);
          return array($success, $add_new);
          exit(0);
        }

        // Get last insert ID
        $last_Id = $this->conn->lastInsertId('[' . DB_NAME . '].[dbo].[gilbarconmi_lpg]');
        if ( ! $last_Id) {
          $this->conn->rollback();
          $this->errors[] = '2:' . $display_sql . ';'. $this->conn->errorInfo();
          $this->freeQueryResult($stmt);
          return array($success, $add_new);
          exit(0);
        }

        // Update for RefID
        $sql = 'UPDATE [' . DB_NAME . '].[dbo].[gilbarconmi_lpg] SET RefID = :RefID WHERE ID = :ID';
        $stmt = $this->conn->prepare($sql);
        $stmt->bindValue(':RefID', $last_Id);
        $stmt->bindValue(':ID', $last_Id);
        $result = $stmt->execute();
        if ( ! $result) {
          $this->conn->rollback();
          $this->errors[] = '3:' . $sql . ';'. $this->conn->errorInfo();
          $this->freeQueryResult($stmt);
          return array($success, $add_new);
          exit(0);
        }

        // Saving signature image file
        // Not to use image file as per requirement
        ////$this->saveSignatureImage($last_Id);
        $sql = 'INSERT INTO [' . DB_NAME . '].[dbo].[gilbarconmi_lpg2] (parent_id, ' . $sql_field_names2 . ') VALUES(' . $last_Id . ', ' . $sql_field_values2 . ')';
        $display_sql = 'INSERT INTO [' . DB_NAME . '].[dbo].[gilbarconmi_lpg2] (parent_id, ' . $sql_field_names2 . ') VALUES(' . $last_Id . ', ' . $display_sql_field_values2 . ')';
        $stmt = $this->conn->prepare( $sql );
        $result = $stmt->execute( $temp_array2 );
        if ( ! $result) {
          $this->conn->rollback();
          $this->errors[] = '4:' . $display_sql . ';'. $this->conn->errorInfo();
          $this->freeQueryResult($stmt);
          return array($success, $add_new);
          exit(0);
        }

        // Get last insert ID
        $last_Id2 = $this->conn->lastInsertId('[' . DB_NAME . '].[dbo].[gilbarconmi_lpg2]');
        if ( ! $last_Id2) {
          $this->conn->rollback();
          $this->errors[] = '5:' . $display_sql . ';'. $this->conn->errorInfo();
          $this->freeQueryResult($stmt);
          return array($success, $add_new);
          exit(0);
        }

        $sql = 'INSERT INTO [' . DB_NAME . '].[dbo].[gilbarconmi_lpg3] (parent_id, ' . $sql_field_names3 . ') VALUES(' . $last_Id . ', ' . $sql_field_values3 . ')';
        $display_sql = 'INSERT INTO [' . DB_NAME . '].[dbo].[gilbarconmi_lpg3] (parent_id, ' . $sql_field_names3 . ') VALUES(' . $last_Id . ', ' . $display_sql_field_values3 . ')';
        $stmt = $this->conn->prepare( $sql );
        $result = $stmt->execute( $temp_array3 );
        if ( ! $result) {
          $this->conn->rollback();
          $this->errors[] = '6:' . $display_sql . ';'. $this->conn->errorInfo();
          $this->freeQueryResult($stmt);
          return array($success, $add_new);
          exit(0);
        }

        // Get last insert ID
        $last_Id3 = $this->conn->lastInsertId('[' . DB_NAME . '].[dbo].[gilbarconmi_lpg3]');
        if ( ! $last_Id3) {
          $this->conn->rollback();
          $this->errors[] = '7:' . $display_sql . ';'. $this->conn->errorInfo();
          $this->freeQueryResult($stmt);
          return array($success, $add_new);
          exit(0);
        }

        $this->conn->commit();

        $success = $last_Id;

      } catch (PDOException $e) {
        $this->conn->rollback();
        $this->errors[] = '8:' . $e->getMessage() . '; ' . $display_sql
                      . ';display_sql_field_values1: ' . $display_sql_field_values1
                      . ';display_sql_field_values2: ' . $display_sql_field_values2
                      . ';display_sql_field_values3: ' . $display_sql_field_values3;
      }

    } else {
      // Update

      $sql_fields1 = '';
      $sql_fields2 = '';
      $sql_fields3 = '';
      foreach ($this->fields as $field => $value) {
        // Field name
        $array_field_name = explode('_', $field);
        if (count($array_field_name) > 1) {
          if (strtoupper($array_field_name[0]) == 'QA' && count($array_field_name) == 2) {
            if (is_numeric($array_field_name[1])) {
              $qa_seq = (int)$array_field_name[1];
              if ($qa_seq > 0 && $qa_seq <= 100) {
                $sql_fields1 .= $field . " = '" . $value ."', ";
                $temp_array1[':' . $field] = $value;
              } else if ($qa_seq > 100 && $qa_seq <= 200) {
                $sql_fields2 .= $field . " = '" . $value ."', ";
                $temp_array2[':' . $field] = $value;
              } else if ($qa_seq > 200 && $qa_seq <= 300) {
                $sql_fields3 .= $field . " = '" . $value ."', ";
                $temp_array3[':' . $field] = $value;
              }
            } else {
              $sql_fields1 .= $field . " = '" . $value ."', ";
              $temp_array1[':' . $field] = $value;
            }
          } else {
            $sql_fields1 .= $field . " = '" . $value ."', ";
            $temp_array1[':' . $field] = $value;
          }
        } else {
          $sql_fields1 .= $field . " = '" . $value ."', ";
          $temp_array1[':' . $field] = $value;
        }
      }

      $sql_fields1 = substr($sql_fields1, 0, strlen($sql_fields1) - 2);
      $sql_fields2 = substr($sql_fields2, 0, strlen($sql_fields2) - 2);
      $sql_fields3 = substr($sql_fields3, 0, strlen($sql_fields3) - 2);

      try {
        //****** Important Note: Use standard SQL on PDO because the PDO cannot update created_date field with a default GETDATE() for MSSQL setting. ******
        $sql = 'UPDATE [' . DB_NAME . '].[dbo].[gilbarconmi_lpg] SET ' . $sql_fields1 . ', created_date = GETDATE() WHERE id = ' . $edit_id;
        $stmt = $this->conn->prepare( $sql );
        $this->conn->beginTransaction();
        $result = $stmt->execute();
        if ( ! $result) {
          $this->conn->rollback();
          $this->errors[] = '1a:' . $sql . ';'. $this->conn->errorInfo();
          $this->freeQueryResult($stmt);
          return array($success, $add_new);
          exit(0);
        }

        $sql = 'UPDATE [' . DB_NAME . '].[dbo].[gilbarconmi_lpg2] SET ' . $sql_fields2 . ' WHERE parent_id = ' . $edit_id;
        $stmt = $this->conn->prepare( $sql );
        $result = $stmt->execute();
        if ( ! $result) {
          $this->conn->rollback();
          $this->errors[] = '2a:' . $sql . ';'. $this->conn->errorInfo();
          $this->freeQueryResult($stmt);
          return array($success, $add_new);
          exit(0);
        }

        $sql = 'UPDATE [' . DB_NAME . '].[dbo].[gilbarconmi_lpg3] SET ' . $sql_fields3 . ' WHERE parent_id = ' . $edit_id;
        $stmt = $this->conn->prepare( $sql );
        $result = $stmt->execute();
        if ( ! $result) {
          $this->conn->rollback();
          $this->errors[] = '3a:' . $sql . ';'. $this->conn->errorInfo();
          $this->freeQueryResult($stmt);
          return array($success, $add_new);
          exit(0);
        }

        $this->conn->commit();

        $success = $edit_id;

      } catch (PDOException $e) {
        $this->conn->rollback();
        $this->errors[] = '4a;'. $e->getMessage() . '; ' . $sql . '; sql_fields1: ' . $sql_fields1
                           . '; sql_fields2: ' . $sql_fields2
                           . '; sql_fields3: ' . $sql_fields3;

      }
    }

    $this->freeQueryResult($stmt);

    return array($success, $add_new);

  }

  /**
   * saveData
   * @access  public
   * @desc  Save Gilbarco NMI LPG form captures.
   */
  public function saveData($data) {

    $this->fromPost($data);
    return $this->save();
  }

  /**
   * load
   * @access  public
   * @desc  Load from database.
   */
  public function load($id, $act_no) {

    $return = FALSE;
    $stmt = $this->conn->prepare(
      'SELECT * FROM [' . DB_NAME . '].[dbo].[gilbarconmi_lpg] '
        . 'LEFT JOIN [' . DB_NAME . '].[dbo].[gilbarconmi_lpg2] ON [' . DB_NAME . '].[dbo].[gilbarconmi_lpg].id = [' . DB_NAME . '].[dbo].[gilbarconmi_lpg2].parent_id '
        . 'LEFT JOIN [' . DB_NAME . '].[dbo].[gilbarconmi_lpg3] ON [' . DB_NAME . '].[dbo].[gilbarconmi_lpg].id = [' . DB_NAME . '].[dbo].[gilbarconmi_lpg3].parent_id '
        . 'WHERE [' . DB_NAME . '].[dbo].[gilbarconmi_lpg].id = :id AND QA_002 = :act_no'
      );
    $stmt->bindValue(':id', $id, PDO::PARAM_STR);
    $stmt->bindValue(':act_no', $act_no, PDO::PARAM_STR);
    $stmt->execute();
    if ($stmt->rowCount()) {
      $this->fields = $stmt->fetch(PDO::FETCH_ASSOC);
      $return = TRUE;
    }
    $this->freeQueryResult($stmt);
    return $return;
  }

  /**
   * makeUpPDFFileName
   * @access  public
   * @desc  make up a Gilbarco NMI LPG's PDF filename.
   */
  public function makeUpPDFFileName($act_no) {

    $file_extention = '.pdf';
    // IMPORTANT NOTE: File name does not allow underscore character for Activity Number
    $filename = 'gnmi_lpg_' . substr(strtolower(preg_replace("/[^A-Za-z0-9]/", '', $act_no)), 0, 25);

    $new_num = -1;
    $stmt = $this->conn->prepare("SELECT PDFFilename FROM [" . DB_NAME . "].[dbo].[gilbarconmi_lpg] WHERE PDFFilename LIKE :PDFFilename ORDER BY PDFFilename");
    $stmt->bindValue(':PDFFilename', $filename . '%', PDO::PARAM_STR);
    $stmt->execute();
    $result = $stmt->fetchAll();

    $length = count($result);
    $counter = 0;
    while ($counter < $length) {
      $arr = explode('.', $result[$counter]['PDFFilename']);
      $arr1 = explode('_', $arr[0]);
      $temp_filename = $arr1[0] . '_' . $arr1[1] . '_' . $arr1[2];
      if (strcasecmp($temp_filename, $filename) == 0) {
        if (isset($arr1[3]) && is_numeric($arr1[3])) {
          $temp_new_num = (int)$arr1[3];
          if ($new_num < $temp_new_num) {
            $new_num = $temp_new_num;
          }
        } else {
          $new_num = 0;
        }
      }
      ++$counter;
    }

    if ($new_num > -1) {
      $filename .= '_' . ($new_num + 1);
    }

    return $filename . $file_extention;
  }

  /**
   * outputDataBackupFlatfile
   * @access  public
   * @desc  Output data backup flatfile.
   *        One of methods to store submitte result before insert record onto data table is timestamp the file.
   */
  public function outputDataBackupFlatfile($data) {

    $output_backup_file = TRUE;
    $filepath = __DIR__ . '/../outputs/backup/' . date('Ymd') . '/';
    if ( ! file_exists($filepath)) {
      if ( ! mkdir($filepath, 0777, TRUE)) {
        $output_backup_file = FALSE;
      }
    }
    if ($output_backup_file) {
      $fw = fopen($filepath.date('His') . '_' . substr(strtolower(preg_replace("/[^A-Za-z0-9_]/", '', $data['QA_002'])), 0, 25) . '.txt', 'w');
      foreach($data as $field => $value) {
        fwrite($fw, $field . ': ' . $value . PHP_EOL);
      }
      // Close file
      fclose($fw);
    }

  }

  /**
   * generatePDF
   * @access  public
   * @desc  Generate PDF.
   */
  public function generatePDF($id, $act_no, $filename, $content, $add_new) {

    /*
    require_once('phpLibs/html2pdf-4.5.1/_class/tcpdfConfig.php');
    require_once('phpLibs/html2pdf-4.5.1/_class/locale.class.php');
    require_once('phpLibs/html2pdf-4.5.1/vendor/tecnickcom/tcpdf/tcpdf.php');
    require_once('phpLibs/html2pdf-4.5.1/_class/myPdf.class.php');
    require_once('phpLibs/html2pdf-4.5.1/_class/exception.class.php');
    require_once('phpLibs/html2pdf-4.5.1/_class/parsingCss.class.php');
    require_once('phpLibs/html2pdf-4.5.1/_class/parsingHtml.class.php');
    require_once('phpLibs/html2pdf-4.5.1/html2pdf.class.php');
    */
    require_once(dirname(__FILE__) .'/../phpLibs/html2pdf-4.5.1/vendor/autoload.php');

    $html2pdf = new HTML2PDF('P', 'A4', 'en', true, 'UTF-8', array(10, 12, 10, 12));
    $html2pdf->pdf->SetDisplayMode('fullpage');
    $html2pdf->writeHTML($content);
    $html2pdf->Output($filename);
    $file_path = dirname(__FILE__) . '/../outputs/pdf/' . $filename;
    $html2pdf->Output($file_path, 'F');

    // Save file content into database and remove files
    // Step 1: using the file system for writing the new zip file with maximum compression
    $fileAsString = file_get_contents($file_path);
    $zp = gzopen($file_path . '.gz', "w9");
    gzwrite($zp, $fileAsString);
    gzclose($zp);
    // Step 2: read the file and store onto a database table
    $zipfile = base64_encode(file_get_contents($file_path . '.gz'));
    if ($add_new) {
      $stmt = $this->conn->prepare('UPDATE [' . DB_NAME . '].[dbo].[gilbarconmi_lpg] SET PDFFilename = :PDFFilename, PDFContent = :PDFContent WHERE id = :id AND QA_002 = :act_no');
      $stmt->bindValue(':PDFFilename', $filename);
    } else {
      $stmt = $this->conn->prepare('UPDATE [' . DB_NAME . '].[dbo].[gilbarconmi_lpg] SET PDFContent = :PDFContent WHERE id = :id AND QA_002 = :act_no');
    }
    $stmt->bindValue(':PDFContent', $zipfile);
    $stmt->bindValue(':act_no', $act_no);
    $stmt->bindValue(':id', $id);
    $stmt->execute();
    // Remove the files from file system
    unlink($file_path);
    unlink($file_path . '.gz');
  }

  /**
   * showPDF
   * @access  public
   * @param actno
   * @desc  show PDF content.
   */
  public function showPDF($id, $actno) {

    //Step 1: get the zip content from DB table
    $stmt = $this->conn->prepare('SELECT PDFFilename, PDFContent FROM [' . DB_NAME . '].[dbo].[gilbarconmi_lpg] WHERE id = :id AND QA_002 = :act_no');
    $stmt->bindValue(':id', $id);
    $stmt->bindValue(':act_no', $actno);
    $stmt->execute();
    $this->fields = $stmt->fetch(PDO::FETCH_ASSOC);
    $this->freeQueryResult($stmt);

    if ( ! empty($this->fields['PDFFilename'])) {
      //Step 2: write to the file system as a zip file
      $zipfilename = $this->fields['PDFFilename'] . '.gz';
      $zipname = dirname(__FILE__) . '/../outputs/pdf/' . $zipfilename;
      $outputfile = dirname(__FILE__) . '/../outputs/pdf/' . $this->fields['PDFFilename'];
      file_put_contents($zipname, base64_decode($this->fields['PDFContent']));

      $buffer_size = 4096; // read 4kb at a time
      $out_file = fopen($outputfile, 'wb');
      $zp = gzopen($zipname, "rb");

      while ( ! gzeof($zp)) { //for more than 1 files in the zip file
        fwrite($out_file, gzread($zp, $buffer_size));
        //$zipcontent=gzpassthru($zp);
      }
      fclose($out_file);
      gzclose($zp);

      header("Content-type:application/pdf");
      //header("Content-type:image/png");
      echo file_get_contents($outputfile);

      // Remove the files from file system
      unlink($zipname);
      unlink($outputfile);
    } else {
      echo 'Record does not exist.';
    }
  }
}

/* End of file class.gilbarconmi_lpg.php */
/* Location: ./classes/class.gilbarconmi_lpg.php */