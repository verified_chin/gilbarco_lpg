<?php
/*
 * Gilbarco NMI LPG Form's Excel sheet class
 * Copyright (c) 2016 Verified Pty Ltd
 * All rights reserved.
 */

/** Include PHPExcel */
require_once __DIR__ . '/../phpLibs/phpExcel1.8/Classes/PHPExcel.php';

class Gilbarconmi_lpg_excel_sheet {

  // Private fields
  private $id;
  private $act_no;
  private $add_new;
  private $file_path;
  private $filename;
  private $rows = array();
  private $objPHPExcel;
  private $conn;
  private $errors = array();

  // Class constructor
  public function __construct() {

    $this->connectedDatabase();
    $this->objPHPExcel = new PHPExcel();

  }

  // Class destructor
  public function __destruct() {

    $this->objPHPExcel = null;
    // Close database connnection
    $this->closeDatabaseConnection();

  }

  /**
   * getDBConnectionStringParam
   * @access  private
   * @param Connection String's parameter name
   * @desc  Get the database connection string parameter
   */
  private function getDBConnectionStringParam($arg) {

    global $dsn;

    $return = '';
    $array = explode(';', $dsn);
    foreach ($array as $index => $value) {
      $array_inner = explode('=', $value);
      if (count($array_inner) > 1) {
        if (strcasecmp($array_inner[0], $arg) === 0) {
          $return = $array_inner[1];
        }
      }
    }

    if (strcasecmp($arg, 'database') === 0) {
      $return = trim($return);
      if (substr($return, 0, 1) == '[') {
        $return = substr($return, 1, strlen($return) - 2);
      }
    }
    return $return;
  }

  /**
   * connectedDatabase
   * @access  private
   * @desc  Connect Gilbarco NMI Non LPG's database.
   */
  private function connectedDatabase() {

    $success = FALSE;

    // Database host
    $db_host = $this->getDBConnectionStringParam('server');
    // Database name
    $db_name = $this->getDBConnectionStringParam('database');
    // Database user ID
    $db_user = $this->getDBConnectionStringParam('uid');
    // Database user password
    $db_pass = $this->getDBConnectionStringParam('pwd');

    try {
      //$this->conn = new PDO('sqlsrv:Server=' . SERVER_NAME . ';Database=' . DB_NAME, DB_USER, DB_PASSWORD);
      //$this->conn = new PDO('dblib:host=' . $db_host . ';dbname=' . $db_name, $db_user, $db_pass);
      if (IS_WINDOWS_SERVER === true) {
        $this->conn = new PDO('sqlsrv:Server=' . $db_host . ';Database=' . $db_name, $db_user, $db_pass);
      } else {
        $this->conn = new PDO('dblib:host=' . $db_host . ';dbname=' . $db_name, $db_user, $db_pass);
      }
      $success = TRUE;
    } catch (PDOException $e) {
      $this->errors[] = date("l jS \of F Y h:i:s A") . ' Connection failed: ' . $e->getMessage();
    }

    return $success;
  }

  /**
   * close_connect_database
   * @access  private
   * @desc  Close Gilbarco database's connection.
   */
  private function closeDatabaseConnection() {

    $this->conn = null;

  }

  /**
   * freeQueryResult
   * @access  private
   * @desc  Connect Gilbarco NMI LPG's database.
   */
  private function freeQueryResult($stmt) {

    $stmt = null;

  }

  /**
   * loadData
   * @access  private
   * @desc  Load all data from database into class array variable.
   */
  private function loadData() {

    $sql = 'SELECT TOP 1 * FROM [' . DB_NAME . '].[dbo].[gilbarconmi_lpg] '
        . 'LEFT JOIN [' . DB_NAME . '].[dbo].[gilbarconmi_lpg2] ON [' . DB_NAME . '].[dbo].[gilbarconmi_lpg].id = [' . DB_NAME . '].[dbo].[gilbarconmi_lpg2].parent_id '
        . 'LEFT JOIN [' . DB_NAME . '].[dbo].[gilbarconmi_lpg3] ON [' . DB_NAME . '].[dbo].[gilbarconmi_lpg].id = [' . DB_NAME . '].[dbo].[gilbarconmi_lpg3].parent_id '
        . 'WHERE [' . DB_NAME . '].[dbo].[gilbarconmi_lpg].id = :id AND QA_002 = :act_no';
    $stmt = $this->conn->prepare($sql);
    $stmt->bindValue(':id', $this->id, PDO::PARAM_STR);
    $stmt->bindValue(':act_no', $this->act_no, PDO::PARAM_STR);
    $stmt->execute();
    while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
      $this->rows[] = $row;
    }
    $this->freeQueryResult($stmt);

  }

  /**
   * printCustomErrorLog
   * @access  private
   * @desc  Print custom error log file.
   */
  private function printCustomErrorLog() {

    $h = fopen(__DIR__ . '\..\errorLogs\excel\errorexcellog'. date('Y-m-d') . '_' . substr(strtolower(preg_replace("/[^A-Za-z0-9_]/", '', $this->act_no)), 0, 25) . '.txt', 'a'); // Open a file
    fwrite($h, PHP_EOL);
    fwrite($h, print_r($this->errors, TRUE) . PHP_EOL); // Write output
    fwrite($h, 'Occurred on: ' . date('Y-m-d H:i:s') . PHP_EOL);
    fclose($h); // Close file

  }

  /**
   * setProperties
   * @access  private
   * @desc  Set property data to Excel sheet.
   */
  private function setProperties() {

    $this->objPHPExcel->getProperties()->setCreator("Chin Lim")
                   ->setLastModifiedBy("Chin Lim")
                   ->setTitle("Gilbarco NMI LPG")
                   ->setSubject("Gilbarco NMI LPG Document")
                   ->setDescription("Gilbarco NMI LPG Document for PHPExcel, generated using PHP classes.")
                   ->setKeywords("office PHPExcel php")
                   ->setCategory("Gilbarco NMI LPG result file");

  }

  /**
   * setSheetSettings
   * @access  private
   * @desc  Set the active Excel sheet's settings.
   */
  private function setSheetSettings($sheet_index) {

    // Set active sheet index to the first sheet, so Excel opens this as the first sheet
    $this->objPHPExcel->setActiveSheetIndex($sheet_index);

    $this->objPHPExcel->getActiveSheet()->setTitle('Form 6A-Verification GVR');


  }

  /**
   * setSheetHeader
   * @access  private
   * @desc  Set the active sheet header.
   */
  private function setSheetHeader() {

    $this->objPHPExcel->getActiveSheet()
                ->mergeCells('B1:J1')
                ->mergeCells('A2:J2')
                ->mergeCells('A3:J3')
                ->mergeCells('A4:J4')
                ->mergeCells('A5:J5')
                ->mergeCells('A6:J6')
                ->mergeCells('A7:J7')
                ->mergeCells('K1:AB7');

    $this->objPHPExcel->getActiveSheet()->getStyle('A2:J2')->getBorders()->getTop()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
    $this->objPHPExcel->getActiveSheet()->getStyle('A2:A6')->getBorders()->getLeft()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
    $this->objPHPExcel->getActiveSheet()->getStyle('J2:J6')->getBorders()->getRight()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
    $this->objPHPExcel->getActiveSheet()->getStyle('A6:J6')->getBorders()->getBottom()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);

    $this->objPHPExcel->getActiveSheet()->getRowDimension('6')->setRowHeight(35);
    $this->objPHPExcel->getActiveSheet()->getRowDimension('7')->setRowHeight(50);
    $this->objPHPExcel->getActiveSheet()->getRowDimension('8')->setRowHeight(80);
    $this->objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(20);
    $this->objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(20);
    $this->objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(20);
    $this->objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(20);
    $this->objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(20);
    $this->objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(20);
    $this->objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(20);
    $this->objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(20);
    $this->objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(20);
    $this->objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(20);
    $this->objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(20);
    $this->objPHPExcel->getActiveSheet()->getColumnDimension('L')->setWidth(20);
    $this->objPHPExcel->getActiveSheet()->getColumnDimension('M')->setWidth(20);
    $this->objPHPExcel->getActiveSheet()->getColumnDimension('N')->setWidth(20);
    $this->objPHPExcel->getActiveSheet()->getColumnDimension('O')->setWidth(20);
    $this->objPHPExcel->getActiveSheet()->getColumnDimension('P')->setWidth(20);
    $this->objPHPExcel->getActiveSheet()->getColumnDimension('Q')->setWidth(20);
    $this->objPHPExcel->getActiveSheet()->getColumnDimension('R')->setWidth(20);
    $this->objPHPExcel->getActiveSheet()->getColumnDimension('S')->setWidth(20);
    $this->objPHPExcel->getActiveSheet()->getColumnDimension('T')->setWidth(20);
    $this->objPHPExcel->getActiveSheet()->getColumnDimension('U')->setWidth(20);
    $this->objPHPExcel->getActiveSheet()->getColumnDimension('V')->setWidth(20);
    $this->objPHPExcel->getActiveSheet()->getColumnDimension('W')->setWidth(20);
    $this->objPHPExcel->getActiveSheet()->getColumnDimension('X')->setWidth(20);
    $this->objPHPExcel->getActiveSheet()->getColumnDimension('Y')->setWidth(20);
    $this->objPHPExcel->getActiveSheet()->getColumnDimension('Z')->setWidth(20);
    $this->objPHPExcel->getActiveSheet()->getColumnDimension('AA')->setWidth(20);
    $this->objPHPExcel->getActiveSheet()->getColumnDimension('AB')->setWidth(40);

    $logo_image = dirname(__FILE__) . '/../images/gvr-logo.png';
    // Add a drawing to the worksheetecho date('H:i:s') . " Add a drawing to the worksheet\n";
    $objDrawing = new PHPExcel_Worksheet_Drawing();
    $objDrawing->setName('GVR Logo');
    $objDrawing->setDescription('GVR Logo');
    $objDrawing->setPath($logo_image);
    $objDrawing->setHeight(90);
    $objDrawing->setCoordinates('A2');
    $objDrawing->setWorksheet($this->objPHPExcel->getActiveSheet());
    $objDrawing->setOffsetY(5);

    $this->objPHPExcel->getActiveSheet()
                ->setCellValue('A1', 'FORM 6A')
                ->setCellValue('A2', 'GILBARCO AUSTRALIA PTY LTD')
                ->setCellValue('A3', 'ACN   000 020 799')
                ->setCellValue('A5', 'NATIONAL TRADE MEASUREMENT');

    $objRichText = new PHPExcel_RichText();
    $objRichText->createText('NOTIFICATION OF INSTRUMENT VERIFICATION OR NON VERIFICATION FORM');
    $objFontStyle = $objRichText->createTextRun("\nNote: *denotes mandatory fields and data must be entered");
    $objFontStyle->getFont()->setItalic(true);
    $objFontStyle->getFont()->setBold(true);
    $this->objPHPExcel->getActiveSheet()->getCell('A6')->setValue($objRichText);
    $this->objPHPExcel->getActiveSheet()->getStyle('A6')->getAlignment()->setWrapText(true);

    $this->objPHPExcel->getActiveSheet()->getStyle('A1')->getFont()->setSize(14);
    $this->objPHPExcel->getActiveSheet()->getStyle('A2')->getFont()->setSize(20);
    $this->objPHPExcel->getActiveSheet()->getStyle('A3')->getFont()->setSize(11);
    $this->objPHPExcel->getActiveSheet()->getStyle('A5')->getFont()->setSize(14);
    $this->objPHPExcel->getActiveSheet()->getStyle('A6')->getFont()->setSize(14);

    $style = array(
        'alignment' => array(
            'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
        )
    );

    $this->objPHPExcel->getActiveSheet()->getStyle('A2')->applyFromArray($style);
    $this->objPHPExcel->getActiveSheet()->getStyle('A3')->applyFromArray($style);
    $this->objPHPExcel->getActiveSheet()->getStyle('A5')->applyFromArray($style);
    $this->objPHPExcel->getActiveSheet()->getStyle('A6')->applyFromArray($style);

    $this->objPHPExcel->getActiveSheet()->getStyle('A1')->getFont()->setBold(TRUE);
    $this->objPHPExcel->getActiveSheet()->getStyle('A2')->getFont()->setBold(TRUE);
    $this->objPHPExcel->getActiveSheet()->getStyle('A3')->getFont()->setBold(TRUE);
    $this->objPHPExcel->getActiveSheet()->getStyle('A5')->getFont()->setBold(TRUE);
    $this->objPHPExcel->getActiveSheet()->getStyle('A6')->getFont()->setBold(TRUE);

    $this->objPHPExcel->getActiveSheet()
                ->setCellValue('A8', 'Instrument owners Business Name*')
                ->setCellValue('B8', 'Owners street number*')
                ->setCellValue('C8', 'Owners street name*')
                ->setCellValue('D8', 'Owners Suburb/Locality*')
                ->setCellValue('E8', 'Owners Postcode*')
                ->setCellValue('F8', 'Owners ABN Number')
                ->setCellValue('G8', 'Owners business type* (select from drop down box)')
                ->setCellValue('H8', 'Street Number where instrument or weighbridge is located*')
                ->setCellValue('I8', 'Street Name where instrument or weighbridge is located*')
                ->setCellValue('J8', 'Suburb/locality where instrument or weighbridge is located*')
                ->setCellValue('K8', 'Postcode where instrument or weighbridge is located')
                ->setCellValue('L8', 'Is this a Weighbridge?* (Y for yes or N for no)')
                ->setCellValue('M8', 'Public Weighbridge No* (if a public weighbridge)')
                ->setCellValue('N8', 'GPS Coordinates if address not applicable (eg a mine site)')
                ->setCellValue('O8', 'Servicing Licensee Number*')
                ->setCellValue('P8', 'Licensee Name*')
                ->setCellValue('Q8', 'Licensees ABN Number*')
                ->setCellValue('R8', 'Verifiers Registration No.*')
                ->setCellValue('S8', 'Name of Verifier* ')
                ->setCellValue('T8', 'Verifiers Mark*')
                ->setCellValue('U8', 'Date of verification*')
                ->setCellValue('V8', 'Instrument Make*')
                ->setCellValue('W8', 'Instrument Model*')
                ->setCellValue('X8', 'Instrument Serial Number*')
                ->setCellValue('Y8', 'Instrument NMI Pattern Approval Number*')
                ->setCellValue('Z8', 'Instrument Capacity or Flow rate*(eg 60t or 30 l/m)')
                ->setCellValue('AA8', 'Instrument Performance Code* (drop down box)')
                ->setCellValue('AB8', '*If Verification Code is "H", please provide reasons-eg non payment by owner, parts not available, replaced with new instrument etc.');

    $this->objPHPExcel->getActiveSheet()->getStyle('A8:AB8')->getFont()->setBold(TRUE);

    $this->objPHPExcel->getActiveSheet()->getStyle('A8:AB8')->getAlignment()->setWrapText(true);
    $this->objPHPExcel->getActiveSheet()->getStyle('A8:AB8')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_TOP);

    $array_column_background = array(
                'fill' => array(
                    'type' => PHPExcel_Style_Fill::FILL_SOLID,
                    'color' => array('rgb' => 'ffffff')
                )
              );
    $this->objPHPExcel->getActiveSheet()->getStyle('A2:J6')->applyFromArray($array_column_background);
    $array_column_background = array(
                'fill' => array(
                    'type' => PHPExcel_Style_Fill::FILL_SOLID,
                    'color' => array('rgb' => 'e5e3e3')
                )
              );
    $this->objPHPExcel->getActiveSheet()->getStyle('A8:AB8')->applyFromArray($array_column_background);

    $this->objPHPExcel->getActiveSheet()->getStyle('A8:AB8')->getBorders()->getTop()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
    $this->objPHPExcel->getActiveSheet()->getStyle('A8:AB8')->getBorders()->getLeft()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
    $this->objPHPExcel->getActiveSheet()->getStyle('A8:AB8')->getBorders()->getRight()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
    $this->objPHPExcel->getActiveSheet()->getStyle('A8:AB8')->getBorders()->getBottom()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);

    $this->objPHPExcel->getActiveSheet()->getStyle('A8')->getBorders()->getRight()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
    $this->objPHPExcel->getActiveSheet()->getStyle('B8')->getBorders()->getRight()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
    $this->objPHPExcel->getActiveSheet()->getStyle('C8')->getBorders()->getRight()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
    $this->objPHPExcel->getActiveSheet()->getStyle('D8')->getBorders()->getRight()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
    $this->objPHPExcel->getActiveSheet()->getStyle('E8')->getBorders()->getRight()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
    $this->objPHPExcel->getActiveSheet()->getStyle('F8')->getBorders()->getRight()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
    $this->objPHPExcel->getActiveSheet()->getStyle('G8')->getBorders()->getRight()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
    $this->objPHPExcel->getActiveSheet()->getStyle('H8')->getBorders()->getRight()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
    $this->objPHPExcel->getActiveSheet()->getStyle('I8')->getBorders()->getRight()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
    $this->objPHPExcel->getActiveSheet()->getStyle('J8')->getBorders()->getRight()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
    $this->objPHPExcel->getActiveSheet()->getStyle('K8')->getBorders()->getRight()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
    $this->objPHPExcel->getActiveSheet()->getStyle('L8')->getBorders()->getRight()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
    $this->objPHPExcel->getActiveSheet()->getStyle('M8')->getBorders()->getRight()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
    $this->objPHPExcel->getActiveSheet()->getStyle('N8')->getBorders()->getRight()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
    $this->objPHPExcel->getActiveSheet()->getStyle('O8')->getBorders()->getRight()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
    $this->objPHPExcel->getActiveSheet()->getStyle('P8')->getBorders()->getRight()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
    $this->objPHPExcel->getActiveSheet()->getStyle('Q8')->getBorders()->getRight()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
    $this->objPHPExcel->getActiveSheet()->getStyle('R8')->getBorders()->getRight()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
    $this->objPHPExcel->getActiveSheet()->getStyle('S8')->getBorders()->getRight()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
    $this->objPHPExcel->getActiveSheet()->getStyle('T8')->getBorders()->getRight()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
    $this->objPHPExcel->getActiveSheet()->getStyle('U8')->getBorders()->getRight()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
    $this->objPHPExcel->getActiveSheet()->getStyle('V8')->getBorders()->getRight()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
    $this->objPHPExcel->getActiveSheet()->getStyle('W8')->getBorders()->getRight()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
    $this->objPHPExcel->getActiveSheet()->getStyle('X8')->getBorders()->getRight()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
    $this->objPHPExcel->getActiveSheet()->getStyle('Y8')->getBorders()->getRight()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
    $this->objPHPExcel->getActiveSheet()->getStyle('Z8')->getBorders()->getRight()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
    $this->objPHPExcel->getActiveSheet()->getStyle('AA8')->getBorders()->getRight()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);

    // Freeze the pane
    //$this->objPHPExcel->getActiveSheet()->freezePane('K9');
  }

  /**
   * collectDataIntoArray
   * @access  private
   * @desc  Collect data in preparation for rendering.
   */
  private function collectDataIntoArray($row) {

    $i = 0;
    $array_fields = array();
    /* Commented out as a mistaken brief earlier, Chin 20/05/2019
    if ($row['QA_052'] == 'with-switch') {
      $array_row_field_names = array(
                                  array('field_name_1' => 'QA_090'),
                                  array('field_name_1' => 'QA_091'),
                                  array('field_name_1' => 'QA_092'),
                                  array('field_name_1' => 'QA_093'),
                                  array('field_name_1' => 'QA_094'),
                                  array('field_name_1' => 'QA_095')
                                );
    } else {
      $array_row_field_names = array(
                                  array('field_name_1' => 'QA_196'),
                                  array('field_name_1' => 'QA_197'),
                                  array('field_name_1' => 'QA_198'),
                                  array('field_name_1' => 'QA_199'),
                                  array('field_name_1' => 'QA_200'),
                                  array('field_name_1' => 'QA_201'),
                                  array('field_name_1' => 'QA_202')
                                );
    }


    foreach ($array_row_field_names as $array_field_names) {
      $found = FALSE;
      foreach ($array_field_names as $field_name) {
        if ( ! empty($row[$field_name])) {
          $found = TRUE;
        }
      }
      if ($found) {
    */
        $array_fields[$i]['instrument_owners_business'] = $row['QA_009'];
        $array_fields[$i]['verifiers_registration_number'] = $row['QA_005'];
        $array_fields[$i]['verifiers_registration_name'] = $row['QA_004'];
        $array_fields[$i]['verifiers_mark'] = $row['QA_008'];
        $array_fields[$i]['date_of_verification'] = $row['QA_001'];
        $array_fields[$i]['instrument_nmi_pattern_approval_number'] = $row['QA_019'];
        $array_fields[$i]['make'] = $row['QA_015'];
        $array_fields[$i]['model'] = $row['QA_016'];
        $array_fields[$i]['serial_number'] = $row['QA_018'];
        //$array_fields[$i]['flow_rate'] = $row[$array_field_names['field_name_1']];
        $array_fields[$i]['flow_rate'] = $row['QA_022'];
        $array_qa_280 = explode('~', $row['QA_280']);
        if (count($array_qa_280) > 6) {
          $array_fields[$i]['instrument_street_name'] = $array_qa_280[0];
          $array_fields[$i]['instrument_suburb'] = $array_qa_280[1];
          $array_fields[$i]['instrument_postcode'] = $array_qa_280[2];
          $array_fields[$i]['owners_street_name'] = $array_qa_280[3];
          $array_fields[$i]['owners_suburb'] = $array_qa_280[4];
          $array_fields[$i]['owners_postcode'] = $array_qa_280[5];
          $array_fields[$i]['owners_abn'] = $array_qa_280[6];
        } else {
          $array_fields[$i]['instrument_street_name'] = '';
          $array_fields[$i]['instrument_suburb'] = '';
          $array_fields[$i]['instrument_postcode'] = '';
          $array_fields[$i]['owners_street_name'] = '';
          $array_fields[$i]['owners_suburb'] = '';
          $array_fields[$i]['owners_postcode'] = '';
          $array_fields[$i]['owners_abn'] = '';
        }
        ++$i;
/*
      }
    }
*/
    return $array_fields;

  }

  /**
   * populateData
   * @access  private
   * @desc  Populate data to Excel sheet.
   */
  private function populateData() {


    $this->loadData();

    if (count($this->rows) > 0) {

      // Initialise
      $sheet_index = 0;
      $owners_business_type = 'FUEL';
      $is_weighbridge = 'N';
      $public_weighbridge_no = 'N';
      $gps_co_ordinates  = 'N/A';
      $licenses_no = 'SL0484';
      $licensee_name = 'GILBARCO AUSTRALIA PTY LTD';
      $licensee_abn = ' 930000020799'; // ** Need a openning space character to make the spreasheet display number.

      foreach ($this->rows as $row) {

        if ($sheet_index > 0) {
          $objWorkSheet = $this->objPHPExcel->createSheet($sheet_index);
        } else {
          $this->setProperties();
        }

        $this->setSheetSettings($sheet_index);
        $this->setSheetHeader();
        $array_fields = $this->collectDataIntoArray($row);

        $k = 9;
        $keys = array_keys($array_fields);
        $array_fields_length = count($keys);
        for ($j = 0; $j < $array_fields_length; ++$j) {
          $this->objPHPExcel->getActiveSheet()
                  ->setCellValue('A' . $k, $array_fields[$keys[$j]]['instrument_owners_business'])
                  ->setCellValue('C' . $k, $array_fields[$keys[$j]]['owners_street_name'])
                  ->setCellValue('D' . $k, $array_fields[$keys[$j]]['owners_suburb'])
                  ->setCellValue('E' . $k, $array_fields[$keys[$j]]['owners_postcode'])
                  ->setCellValue('F' . $k, $array_fields[$keys[$j]]['owners_abn'])
                  ->setCellValue('G' . $k, $owners_business_type)
                  ->setCellValue('I' . $k, $array_fields[$keys[$j]]['instrument_street_name'])
                  ->setCellValue('J' . $k, $array_fields[$keys[$j]]['instrument_suburb'])
                  ->setCellValue('K' . $k, $array_fields[$keys[$j]]['instrument_postcode'])
                  ->setCellValue('L' . $k, $is_weighbridge)
                  ->setCellValue('M' . $k, $public_weighbridge_no)
                  ->setCellValue('N' . $k, $gps_co_ordinates)
                  ->setCellValue('O' . $k, $licenses_no)
                  ->setCellValue('P' . $k, $licensee_name)
                  ->setCellValue('Q' . $k, $licensee_abn)
                  ->setCellValue('R' . $k, $array_fields[$keys[$j]]['verifiers_registration_number'])
                  ->setCellValue('S' . $k, $array_fields[$keys[$j]]['verifiers_registration_name'])
                  ->setCellValue('T' . $k, $array_fields[$keys[$j]]['verifiers_mark'])
                  ->setCellValue('U' . $k, $array_fields[$keys[$j]]['date_of_verification'])
                  ->setCellValue('V' . $k, $array_fields[$keys[$j]]['make'])
                  ->setCellValue('W' . $k, $array_fields[$keys[$j]]['model'])
                  ->setCellValue('X' . $k, $array_fields[$keys[$j]]['serial_number'])
                  ->setCellValue('Y' . $k, $array_fields[$keys[$j]]['instrument_nmi_pattern_approval_number'])
                  ->setCellValue('Z' . $k, $array_fields[$keys[$j]]['flow_rate']);
          ++$k;
        }
        ++$sheet_index;
      }
    }

  }

  /**
   * makeUpExcelSheetFileName
   * @access  public
   * @desc  make up a Gilbarco NMI LPG's Excel sheet filename.
   */
  public function makeUpExcelSheetFileName($act_no) {

    $file_extention = '.xlsx';
    // IMPORTANT NOTE: File name does not allow underscore character for Activity Number
    $filename = 'gnmi_lpg_' . substr(strtolower(preg_replace("/[^A-Za-z0-9]/", '', $act_no)), 0, 25);

    $new_num = -1;
    $stmt = $this->conn->prepare("SELECT ExcelSheetFilename FROM [" . DB_NAME . "].[dbo].[gilbarconmi_lpg] WHERE ExcelSheetFilename LIKE :ExcelSheetFilename ORDER BY ExcelSheetFilename");
    $stmt->bindValue(':ExcelSheetFilename', $filename . '%', PDO::PARAM_STR);
    $stmt->execute();
    $result = $stmt->fetchAll();

    $length = count($result);
    $counter = 0;
    while ($counter < $length) {
      $arr = explode('.', $result[$counter]['ExcelSheetFilename']);
      $arr1 = explode('_', $arr[0]);
      $temp_filename = $arr1[0] . '_' . $arr1[1] . '_' . $arr1[2];
      if (strcasecmp($temp_filename, $filename) == 0) {
        if (isset($arr1[3]) && is_numeric($arr1[3])) {
          $temp_new_num = (int)$arr1[3];
          if ($new_num < $temp_new_num) {
            $new_num = $temp_new_num;
          }
        } else {
          $new_num = 0;
        }
      }
      ++$counter;
    }

    if ($new_num > -1) {
      $filename .= '_' . ($new_num + 1);
    }

    return $filename . $file_extention;
  }

  /**
   * setExcelSheetFilename
   * @access  public
   * @desc  set Gilbarco NMI LPG's Excel sheet filename.
   */
  public function setExcelSheetFilename($filename) {
    $this->filename = $filename;
  }

  /**
   * saveExcelSheet
   * @access  public
   * @desc  generate Gilbarco NMI LPG's Excel sheet.
   */
  public function saveExcelSheet() {

    if (empty($this->filename)) {
      $filename = $this->makeUpExcelSheetFileName($this->act_no);
    } else {
      $filename = $this->filename;
    }
    $this->file_path = dirname(__FILE__) . '\..\outputs\excel\\' . $filename;
    $objWriter = PHPExcel_IOFactory::createWriter($this->objPHPExcel, 'Excel2007');
    $objWriter->save($this->file_path);

    // Save file content into database and remove files
    // Step 1: using the file system for writing the new zip file with maximum compression
    $fileAsString = file_get_contents($this->file_path);
    $zp = gzopen($this->file_path . '.gz', "w9");
    gzwrite($zp, $fileAsString);
    gzclose($zp);
    // Step 2: read the file and store onto a database table
    $zipfile = base64_encode(file_get_contents($this->file_path . '.gz'));
    if ($this->add_new) {
      $stmt = $this->conn->prepare('UPDATE [' . DB_NAME . '].[dbo].[gilbarconmi_lpg] SET ExcelSheetFilename = :ExcelSheetFilename, ExcelSheetContent = :ExcelSheetContent WHERE id = :id AND QA_002 = :act_no');
      $stmt->bindValue(':ExcelSheetFilename', $filename);
    } else {
      $stmt = $this->conn->prepare('UPDATE [' . DB_NAME . '].[dbo].[gilbarconmi_lpg] SET ExcelSheetContent = :ExcelSheetContent WHERE id = :id AND QA_002 = :act_no');
    }
    $stmt->bindValue(':ExcelSheetContent', $zipfile);
    $stmt->bindValue(':id', $this->id);
    $stmt->bindValue(':act_no', $this->act_no);
    $stmt->execute();

    // Remove the files from file system
    unlink($this->file_path);
    unlink($this->file_path . '.gz');
  }

  /**
   * generateExcelSheet
   * @access  public
   * @desc  generate Gilbarco NMI LPG's PDF.
   */
  public function generateExcelSheet($id, $act_no, $add_new) {

    $this->id = $id;
    $this->act_no = $act_no;
    $this->add_new = $add_new;
    $this->populateData();
    if (count($this->errors) > 0) {
      $this->printCustomErrorLog();
      echo 'There is an error occured. Please check the Excel error log.';
    } else {
      $this->saveExcelSheet();
      echo 'File was successfully saved ' . $this->file_path;
    }

  }

  /**
   * showExcelSheet
   * @access  public
   * @param actno
   * @desc  show Excel Sheet.
   */
  public function showExcelSheet($id, $actno) {

    //Step 1: get the zip content from DB table
    $stmt = $this->conn->prepare('SELECT ExcelSheetFilename, ExcelSheetContent FROM [' . DB_NAME . '].[dbo].[gilbarconmi_lpg] WHERE id = :id AND QA_002 = :act_no');
    $stmt->bindValue(':id', $id);
    $stmt->bindValue(':act_no', $actno);
    $stmt->execute();
    $this->fields = $stmt->fetch(PDO::FETCH_ASSOC);
    $this->freeQueryResult($stmt);

    if ( ! empty($this->fields['ExcelSheetFilename'])) {
      //Step 2: write to the file system as a zip file
      $zipfilename = $this->fields['ExcelSheetFilename'] . '.gz';
      $zipname = dirname(__FILE__) . '/../outputs/excel/' . $zipfilename;
      $outputfile = dirname(__FILE__) . '/../outputs/excel/' . $this->fields['ExcelSheetFilename'];
      file_put_contents($zipname, base64_decode($this->fields['ExcelSheetContent']));

      $buffer_size = 4096; // read 4kb at a time
      $out_file = fopen($outputfile, 'wb');
      $zp = gzopen($zipname, "rb");

      while ( ! gzeof($zp)) { //for more than 1 files in the zip file
        fwrite($out_file, gzread($zp, $buffer_size));
        //$zipcontent=gzpassthru($zp);
      }
      fclose($out_file);
      gzclose($zp);


      header('Content-Disposition: attachment; filename="' . $this->fields['ExcelSheetFilename'] . '"');
      header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
      header('Content-Length: ' . filesize(dirname(__FILE__) . '/../outputs/excel/' . $this->fields['ExcelSheetFilename']));
      header('Content-Transfer-Encoding: binary');
      header('Cache-Control: must-revalidate');
      header('Pragma: public');

      //header("Content-type:image/png");
      echo file_get_contents($outputfile);

      // Remove the files from file system
      unlink($zipname);
      unlink($outputfile);
    } else {
      echo 'Record does not exists.';
    }
  }

}

/* End of file class.gilbarconmi_lpg_excel_sheet.php */
/* Location: ./classes/class.gilbarconmi_lpg_excel_sheet.php */