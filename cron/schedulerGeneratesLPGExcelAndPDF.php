
<?php
/*
 * Gilbarco NMI LPG Form
 * @desc A PHP script to be triggered by the scheduler for generating LPG's Excelsheets and PDFs.
 * @author Chin Lim
 * @date 01/08/2019
 * Copyright (c) 2019 Verified Pty Ltd
 * All rights reserved.
 */

/** Include database config */
require_once(__DIR__ . '/../config/config.php');
require_once(__DIR__ . '/../config/db.config.php');
/** Include classes */
require_once(__DIR__ . '/../classes/class.gilbarconmi_lpg.php');
require_once(__DIR__ . '/../classes/class.gilbarconmi_lpg_excel_sheet.php');

/**
 * getDBConnectionStringParam
 * @param Connection String's parameter name
 * @desc  Get the database connection string parameter
 */
function getDBConnectionStringParam($arg) {

  global $dsn;

  $return = '';
  $array = explode(';', $dsn);
  foreach ($array as $index => $value) {
    $array_inner = explode('=', $value);
    if (count($array_inner) > 1) {
      if (strcasecmp($array_inner[0], $arg) === 0) {
        $return = $array_inner[1];
      }
    }
  }

  if (strcasecmp($arg, 'database') === 0) {
    $return = trim($return);
    if (substr($return, 0, 1) == '[') {
      $return = substr($return, 1, strlen($return) - 2);
    }
  }
  return $return;
}

// Allow infinitive server response time
$return_ini = ini_set('max_execution_time', 0);
if ( ! $return_ini) {
  echo 'INI setting fails.';
}

try {

  // Database host
  $db_host = getDBConnectionStringParam('server');
  // Database name
  $db_name = getDBConnectionStringParam('database');
  // Database user ID
  $db_user = getDBConnectionStringParam('uid');
  // Database user password
  $db_pass = getDBConnectionStringParam('pwd');

  $conn = new PDO('dblib:host=' . $db_host . ';dbname=' . $db_name, $db_user, $db_pass);
} catch (PDOException $e) {
  die('Connection failed: ' . $e->getMessage());
}


// Set object variable
$form = new Gilbarconmi_lpg();
$cls_excel_sheet = new Gilbarconmi_lpg_excel_sheet;
$array_records = array();
$stmt = $conn->prepare(
  'SELECT id, QA_002, ExcelSheetFilename, PDFFilename FROM [' . DB_NAME . '].[dbo].[gilbarconmi_lpg] '
    . "WHERE DATEDIFF(DAY, created_date, GETDATE()) <= 3 AND (ISNULL(CAST(ExcelSheetContent AS VARCHAR), '') = '' OR ISNULL(CAST(PDFContent AS VARCHAR), '') = '') "
      . "AND (ISNULL(isprocess, '') = '' OR DATEDIFF(DAY, isprocess, GETDATE()) >= 1) "
    . 'ORDER BY created_date'
  );
//$stmt->bindValue(':id', $start_id, PDO::PARAM_STR);
$stmt->execute();
if ($stmt->rowCount()) {
  $array_records = $stmt->fetchAll();
}
$stmt = null;

try {
  // Loop and generate Excelsheet and PDF for each record.
  $length = count($array_records);
  $index = 0;
  echo 'Total records: ' . $length . '<br/>';
  while ($index < $length) {

    $stmt = $conn->prepare(
      'SELECT id, QA_002, ExcelSheetFilename, PDFFilename FROM [' . DB_NAME . '].[dbo].[gilbarconmi_lpg] '
        . "WHERE (ISNULL(CAST(ExcelSheetContent AS VARCHAR), '') = '' OR ISNULL(CAST(PDFContent AS VARCHAR), '') = '') "
          . "AND (ISNULL(isprocess, '') = '' OR DATEDIFF(DAY, isprocess, GETDATE()) >= 1) "
          . 'AND id = :id'
      );
    $stmt->bindValue(':id', $array_records[$index]['id'], PDO::PARAM_INT);
    $stmt->execute();
    if ($stmt->rowCount()) {
      $array_records2 = $stmt->fetchAll();

      echo ($index + 1). '. Processing ID: ' . $array_records[$index]['id'] . ', act no. ' . $array_records[$index]['QA_002'] . '<br/>';

      // Determine to overwrite Excelsheet and PDF filename column.
      $add_new = '1';
      if ( ! empty($array_records2[0]['ExcelSheetFilename']) && ! empty($array_records2[0]['PDFFilename'])) {
        $add_new = '0';
      }

      echo '"' . PHP_EXE . '" -f "' . WEBSERVER_PATH . 'cron/generateLPGExcelAndPDF.php" ' . $array_records[$index]['QA_002'] . ' ' . $array_records[$index]['id'] . ' ' . $add_new . '<br/>';
      // Command to generate Excelsheet and PDF
      exec('"' . PHP_EXE . '" -f "' . WEBSERVER_PATH . 'cron/generateLPGExcelAndPDF.php" ' . $array_records[$index]['QA_002'] . ' ' . $array_records[$index]['id'] . ' ' . $add_new);

      if ($index % 5 == 0) {
        sleep(25);
      }
    }
    $stmt = null;
    ++$index;
  }
} catch (Exception $e) {
    echo 'Caught exception: ',  $e->getMessage(), "\n";
}

// Close the database connection
$conn = null;

/* End of file schedulerGeneratesNLPGExcelAndPDF.php */
/* Location: ./schedulerGeneratesNLPGExcelAndPDF.php */