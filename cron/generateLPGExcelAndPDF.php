<?php
/*
 * Gilbarco NMI LPG Form
 * @desc To generate a LPG's Excelsheet and a PDF base on the given parameters.
 * @author Chin Lim
 * @date 01/08/2019
 * Copyright (c) 2019 Verified Pty Ltd
 * All rights reserved.
 */
/*
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
*/
/** Include database config */
require_once(__DIR__ . '/../config/config.php');
require_once(__DIR__ . '/../config/db.config.php');
/** Include classes */
require_once(__DIR__ . '/../classes/class.gilbarconmi_lpg.php');
require_once(__DIR__ . '/../classes/class.gilbarconmi_lpg_excel_sheet.php');

// qa_002 or activity no.
@$qa_002 = $_GET['qa_002'];
if ( ! isset($qa_002)) {
  if (strlen(@$argv[1])) {
    @$qa_002 = $argv[1];
  } else {
    echo 'qa_002 (Activity No.) is missing...';
    die;
  }
}
// Confirmation no.
@$id = $_GET['id'];
if ( ! isset($id)) {
  if (strlen(@$argv[2])) {
    @$id = $argv[2];
  } else {
    echo 'id (Confirmation No.) is missing...';
    die;
  }
}
// Create new Excelsheet and PDF with overiwrite both filename fields in DB table? If so the DB table's Excelsheet and PDF filename columns will be populated.
@$add_new = $_GET['addnew'];
if ( ! isset($add_new)) {
  if (strlen(@$argv[3])) {
    @$add_new = $argv[3];
  } else {
    echo 'addnew (To determine either populate or not populate PDF filename and Excelsheet filename DB table columns) is missing...';
    die;
  }
}

// Decide whether https or http?
/* Commented out as no longer require third party CutyCapt, Chin 31/07/2019
$ssl      = ( ! empty( $_SERVER['HTTPS'] ) && $_SERVER['HTTPS'] == 'on' );
$sp       = strtolower( $_SERVER['SERVER_PROTOCOL'] );
$protocol = substr( $sp, 0, strpos( $sp, '/' ) ) . ( ( $ssl ) ? 's' : '' );

// Get script name's path and then recreate a new return url path
$array_script_name = explode('/', $_SERVER['SCRIPT_NAME']);
if (sizeof($array_script_name) > 1) {
  $script_name_path = '';
  for ($i = 0; $i < sizeof($array_script_name) - 1; ++$i) {
    if ($i > 0) {
      $script_name_path .= '/';
    }
    $script_name_path .= $array_script_name[$i];
  }
  $excel_sheet_url = $protocol . '://' . $_SERVER['HTTP_HOST'] . $script_name_path . '/generate_excel_sheet.php?actno=' . urlencode($qa_002);
  $pdf_url = $protocol . '://' . $_SERVER['HTTP_HOST'] . $script_name_path . '/html2pdf_lpg.php?actno=' . urlencode($qa_002);
} else {
  $excel_sheet_url = $protocol . '://' . $_SERVER['HTTP_HOST'] . '/generate_excel_sheet.php?actno=' . urlencode($qa_002);
  $pdf_url = $protocol . '://' . $_SERVER['HTTP_HOST'] . '/html2pdf_lpg.php?actno=' . urlencode($qa_002);
}

$excel_sheet_url = str_replace('s://' . DOMAIN_NAME, '://localhost', strtolower($excel_sheet_url));
$pdf_url = str_replace('s://' . DOMAIN_NAME, '://localhost', strtolower($pdf_url));
$excel_sheet_url = str_replace('://' . DOMAIN_NAME, '://localhost', strtolower($excel_sheet_url));
$pdf_url = str_replace('://' . DOMAIN_NAME, '://localhost', strtolower($pdf_url));

$excel_sheet_url = str_replace('s://prtl.verified.com.au', '://localhost', strtolower($excel_sheet_url));
$pdf_url = str_replace('s://prtl.verified.com.au', '://localhost', strtolower($pdf_url));
$excel_sheet_url = str_replace('://prtl.verified.com.au', '://localhost', strtolower($excel_sheet_url));
$pdf_url = str_replace('://prtl.verified.com.au', '://localhost', strtolower($pdf_url));

$excel_sheet_url = str_replace('s://melb.verified.com.au', '://localhost', strtolower($excel_sheet_url));
$pdf_url = str_replace('s://melb.verified.com.au', '://localhost', strtolower($pdf_url));
$excel_sheet_url = str_replace('://melb.verified.com.au', '://localhost', strtolower($excel_sheet_url));
$pdf_url = str_replace('://melb.verified.com.au', '://localhost', strtolower($pdf_url));

$excel_sheet_url = str_replace('s://nsw.verified.com.au', '://localhost', strtolower($excel_sheet_url));
$pdf_url = str_replace('s://nsw.verified.com.au', '://localhost', strtolower($pdf_url));
$excel_sheet_url = str_replace('://nsw.verified.com.au', '://localhost', strtolower($excel_sheet_url));
$pdf_url = str_replace('://nsw.verified.com.au', '://localhost', strtolower($pdf_url));

$excel_sheet_url = str_replace('s://www.verified.com.au', '://localhost', strtolower($excel_sheet_url));
$pdf_url = str_replace('s://www.verified.com.au', '://localhost', strtolower($pdf_url));
$excel_sheet_url = str_replace('://www.verified.com.au', '://localhost', strtolower($excel_sheet_url));
$pdf_url = str_replace('://www.verified.com.au', '://localhost', strtolower($pdf_url));

$excel_sheet_url = str_replace('s://verified.com.au', '://localhost', strtolower($excel_sheet_url));
$pdf_url = str_replace('s://verified.com.au', '://localhost', strtolower($pdf_url));
$excel_sheet_url = str_replace('://verified.com.au', '://localhost', strtolower($excel_sheet_url));
$pdf_url = str_replace('://verified.com.au', '://localhost', strtolower($pdf_url));
*/

// Set LPG object
$form = new Gilbarconmi_lpg();
// Set Excelsheet object
$cls_excel_sheet = new Gilbarconmi_lpg_excel_sheet;
$excel_filename =  $cls_excel_sheet->makeUpExcelSheetFileName($qa_002);
$pdf_filename = $form->makeUpPDFFileName($qa_002);

try {
  // Generate Excel Sheet
  /* Commented out as no longer require third party CutyCapt, Chin 31/07/2019
  $excel_sheet_url .= '&filename=' . urlencode($excel_filename) . '&id=' . $id . '&addnew=' . $add_new;
  $x_excel = 'c:\SiteShoter\CutyCapt.exe --url="' . $excel_sheet_url . '" ';
  $x_excel .= '--min-width=440 --delay=12500 --out=' . dirname(__FILE__) . '\outputs\excel\excelsheet_dummy_screenshot.png';
  //exec($xpdf.' > /dev/null 2>/dev/null &');
  pclose(popen("start " . $x_excel , "r"));
  */
  echo '"' . PHP_EXE . '" -f "' . WEBSERVER_PATH . 'generate_excel_sheet.php" ' . $id . ' ' . $qa_002 . ' ' . $excel_filename . ' ' . $add_new . '<br/>';
  exec('"' . PHP_EXE . '" -f "' . WEBSERVER_PATH . 'generate_excel_sheet.php" ' . $id . ' ' . $qa_002 . ' ' . $excel_filename . ' ' . $add_new);

  // Generate PDF
  /* Commented out as no longer require third party CutyCapt, Chin 31/07/2019
  $pdf_url .= '&filename=' . urlencode($pdf_filename) . '&id=' . $id . '&addnew=' . $add_new;
  $x_pdf = 'c:\SiteShoter\CutyCapt.exe --url="' . $pdf_url . '" ';
  $x_pdf .= '--min-width=440 --delay=26500 --out=' . dirname(__FILE__) . '\outputs\pdf\pdf_dummy_screenshot.png';
  //exec($xpdf.' > /dev/null 2>/dev/null &');
  pclose(popen("start " . $x_pdf , "r"));
  */
  echo '"' . PHP_EXE . '" -f "' . WEBSERVER_PATH . 'html2pdf_lpg.php" ' .  $id . ' ' . $qa_002 . ' ' . $pdf_filename . ' ' . $add_new . '<br/>';
  exec('"' . PHP_EXE . '" -f "' . WEBSERVER_PATH . 'html2pdf_lpg.php" ' .  $id . ' ' . $qa_002 . ' ' . $pdf_filename . ' ' . $add_new);

} catch (Exception $e) {
    echo 'Caught exception: ',  $e->getMessage(), "\n";
}
/* End of file generateNLPGExcelAndPDF.php */
/* Location: ./generateNLPGExcelAndPDF.php */