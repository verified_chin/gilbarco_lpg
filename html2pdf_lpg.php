<?php
/*
 * Gilbarco NMI LPG Form
 * @desc To generating LPG's PDF report.
 * @author Chin Lim
 * @date 2016
 * Copyright (c) 2016 Verified Pty Ltd
 * All rights reserved.
 */
/*
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
*/
/** Include database config */
require_once('config/config.php');
require_once('config/db.config.php');
/** Include custom classes */
require_once('classes/class.gilbarconmi_lpg.php');

// Get query string
// Confirmation No.
@$id = $_GET['id'];
if ( ! isset($id)) {
  if (strlen(@$argv[1])) {
    @$id = $argv[1];
  } else {
    echo 'id (Confirmation No.) is missing...';
    die;
  }
}
// Activity No.
@$act_no = $_GET['actno'];
if ( ! isset($act_no)) {
  if (strlen(@$argv[2])) {
    @$act_no = $argv[2];
  } else {
    echo 'actno (Activity No.) is missing...';
    die;
  }
}
// PDF filename
@$filename = $_GET['filename'];
if ( ! isset($filename)) {
  if (strlen(@$argv[3])) {
    @$filename = $argv[3];
  } else {
    echo 'filename is missing...';
    die;
  }
}
// Wether to overwrite PDF filename field?
@$add_new = $_GET['addnew'];
if ( ! isset($add_new)) {
  if (strlen(@$argv[4])) {
    @$add_new = $argv[4];
  } else {
    echo 'addnew (Whether to overwrite PDF filename field) is missing...';
    die;
  }
}

if (trim($id) != '' && trim($act_no) != '' && trim($filename) != '' && trim($add_new) != '') {

  // Create and initialise variables
  for ($i = 1; $i <= 279; ++$i) {
    ${'qa_' . str_pad($i, 3, '0', STR_PAD_LEFT)} = '';
  }

  $form = new Gilbarconmi_lpg();
  if ($form->load($id, $act_no)) {
    for ($i = 1; $i <= 279; ++$i) {
      ${'qa_' . str_pad($i, 3, '0', STR_PAD_LEFT)} = $form->filterXSS('QA_' . str_pad($i, 3, '0', STR_PAD_LEFT));
    }
  }
  ob_start();
?>
  <style>
    /* General */
    img {
      width: 100%;
    }
    label {
      padding-top: 7px;
      padding-left: 10px;
      padding-right: 10px;
      height: 28px;
    }
    th {
      background-color: #eee;
      veritical-align: top;
    }
    th.header {
      font-weight: bold;
      font-size: 18px;
    }
    th, td {
      font-family: Arial , sans-serif;
      font-size: 12px;
      padding: 5px;
    }
    .label {
      font-weight: bold;
    }
    label.left-unit {
      float: left;
      margin-right: 10px;
      font-weight: bold;
    }
    label.unit {
      float: right;
      margin-left: 5px;
      font-weight: bold;
    }
    .figure {
      text-align: right;
    }
    .page_number {
      width: 100px;
      float: right;
      text-align: right;
      position: absolute;
      right: 0;
      bottom: 0;
    }
    .important-note {
      font-weight: bold;
    }
    /* Logo */
    #gilbarco-logo,
    #verified-logo {
      width: 250px;
    }
    .small-logo #gilbarco-logo,
    .small-logo #verified-logo {
      width: 120px;
    }
    #verified-logo {
      margin-left: 100px;
    }

    /* Section General */
    /* Section Site & Instrument Details */
    .tbl-general-details,
    #tbl-site-and-instrument-details,
    #tbl-reference-standards-of-measurement,
    #tbl-test-results-header,
    #tbl-note,
    #tbl-verifer-signature {
      margin-top: 10px;
      border-collapse : collapse;
    }
    #tbl-general-characteristics,
    .tbl-test-reports,
    #tbl-test-report-header-top,
    #tbl-test-report-header,
    #table-totaliser {
      border-collapse : collapse;
    }
    #tbl-general-characteristics,
    #tbl-test-report-header-top {
      margin-top: 20px;
    }
    .tbl-general-details th,
    .tbl-general-details td,
    #tbl-site-and-instrument-details th,
    #tbl-site-and-instrument-details td,
    #tbl-reference-standards-of-measurement th,
    #tbl-reference-standards-of-measurement td,
    #tbl-general-characteristics th,
    #tbl-general-characteristics td,
    #tbl-test-results-header th,
    #tbl-test-results-header td,
    .tbl-test-reports th,
    .tbl-test-reports td,
    #tbl-note th,
    #tbl-note td,
    #tbl-verifer-signature th,
    #tbl-verifer-signature td,
    #tbl-test-report-header-top th,
    #tbl-test-report-header-top td,
    #tbl-test-report-header th,
    #tbl-test-report-header td,
    .tbl-test-reports th,
    .tbl-test-reports td {
      border: 1px solid #ccc;
    }
    #tbl-test-report-header-top th,
    #tbl-test-report-header-top td,
    .tbl-test-report-header-last-row td {
      border-bottom: 0;
    }
    #tbl-test-report-header-top th.header {
      border-bottom: 1px solid #ccc;
    }
    #tbl-test-report-header-top th,
    #tbl-test-report-header-top td,
    #tbl-test-report-header th,
    #tbl-test-report-header td,
    .tbl-test-reports th,
    .tbl-test-reports td {
      font-size: 12px;
      padding: 3px;
    }
    #tbl-test-report-header-top td {
      text-align: right;
    }
    .tbl-test-report-header-last-row td {
      font-size: 12px;
      padding: 2px;
    }
    .tbl-test-reports td td {
      border: 0;
      padding: 0;
    }
    #table-totaliser td {
      border: 0;
    }
    #verifer-signature {
      width: 480px;
      height: 140px;
    }
  </style>
  <page orientation="p" backtop="0" backbottom="0" backleft="0" backright="0">
    <div id="logo">
      <img id="gilbarco-logo" src="<?php echo __DIR__;?>/images/gvr-logo.png"/>
      <img id="verified-logo" src="<?php echo __DIR__;?>/images/verified-logo.png"/>
    </div>
    <table class="tbl-general-details">
    <tr>
      <th class="header" colspan="4">
        Calibration Check/Certification Report for LPG Dispensers
      </th>
    </tr>
    <tr>
      <th style="width: 18%">
        Date of test
      </th>
      <td style="width: 32%">
        <?php print $qa_001;?>
      </td>
      <th style="width: 18%">
        GVR Job number
      </th>
      <td  style="width: 32%">
        <?php print $qa_279;?>
      </td>
    </tr>
    <tr>
      <th>
        GVR Site number
      </th>
      <td>
        <?php print $qa_003;?>
      </td>
      <th>
        Employee name
      </th>
      <td>
        <?php print $qa_004;?>
      </td>
    </tr>
    <tr>
      <th>
        Verifier number
      </th>
      <td>
        <?php print $qa_005;?>
      </td>
      <th>
        Type of test
      </th>
      <td>
        <label>
        <?php if (stristr(strtolower($qa_006), 'yes')):?>
          <img src="<?php echo __DIR__;?>/images/utilities/checkbox_checked.jpg" style="width: 15px;"/>
        <?php else:?>
          <img src="<?php echo __DIR__;?>/images/utilities/checkbox.jpg" style="width: 12px;"/>
        <?php endif;?>
        <b>Verification</b></label><br/>
        <label>
        <?php if (stristr(strtolower($qa_007), 'yes')):?>
          <img src="<?php echo __DIR__;?>/images/utilities/checkbox_checked.jpg" style="width: 15px;"/>
        <?php else:?>
          <img src="<?php echo __DIR__;?>/images/utilities/checkbox.jpg" style="width: 12px;"/>
        <?php endif;?>
        <b>In-service Inspection</b></label>
      </td>
    </tr>
    <tr>
      <th>
        Verification Mark
      </th>
      <td colspan="3">
        <?php print $qa_008;?>
      </td>
    </tr>
    <tr>
      <th colspan="4" class="header">
        Site and Instrument Details
      </th>
    </tr>
    <tr>
      <th style="width: 18%">
        Name of owner/user
      </th>
      <td style="width: 32%">
        <?php print $qa_009;?>
      </td>
      <th style="width: 18%">
        Address of <br/>owner/user
      </th>
      <td style="width: 32%">
        <?php print $qa_010;?>
      </td>
    </tr>
    <tr>
      <th>
        Name of contact <br/>person on premises
      </th>
      <td>
        <?php print $qa_011;?>
      </td>
      <th>
        Trading name
      </th>
      <td>
        <?php print $qa_012;?>
      </td>
    </tr>
    <tr>
      <th>
        Address of <br/>instrument location
      </th>
      <td>
        <?php print $qa_013;?>
      </td>
      <th>
        Description of <br/>instrument
      </th>
      <td>
        <?php print $qa_014;?>
      </td>
    </tr>
    <tr>
      <th>
        Manufacturer
      </th>
      <td>
        <?php print $qa_015;?>
      </td>
      <th>
        Model
      </th>
      <td>
        <?php print $qa_016;?>
      </td>
    </tr>
    <tr>
      <th>
        Dispenser number(s)
      </th>
      <td>
        <?php print $qa_017;?>
      </td>
      <th>
        Dispenser serial <br/>number
      </th>
      <td>
        <?php print $qa_018;?>
      </td>
    </tr>
    <tr>
      <th>
        Certificate(s) of <br/>Approval number
      </th>
      <td>
        <?php print $qa_019;?>
      </td>
      <th>
        LPG density range<br/> dispenser approved<br/> to deliver
      </th>
      <td>
        <?php print $qa_020;?>
      </td>
    </tr>
    <tr>
      <th>
        Minimum flowrate
      </th>
      <td>
        <?php print $qa_021;?>
      </td>
      <th>
        Maximum flowrate
      </th>
      <td>
        <?php print $qa_022;?>
      </td>
    </tr>
    <tr>
      <th colspan="4" class="header">
        Details of the Reference Standards of Measurement (clause 2)
      </th>
    </tr>
    <tr>
      <th style="width: 18%">
        Make
      </th>
      <td style="width: 32%">
        <?php print $qa_023;?>
      </td>
      <th style="width: 18%">
        Model
      </th>
      <td style="width: 32%">
        <?php print $qa_024;?>
      </td>
    </tr>
    <tr>
      <th>
        Serial number
      </th>
      <td>
        <?php print $qa_025;?>
      </td>
      <th>
        Flowrate<br/> range/weight
      </th>
      <td>
        <?php print $qa_026;?>
      </td>
    </tr>
    <tr>
      <th>
        Regulation 13/37<br/> certificate no.
      </th>
      <td>
        <?php print $qa_027;?>
      </td>
      <th>
        Certificate<br/> expiry date
      </th>
      <td>
        <?php print $qa_028;?>
      </td>
    </tr>
    </table>

    <div class="page_number">
      [[page_cu]]/[[page_nb]]
    </div>

  </page>


  <page orientation="p">
    <div id="logo">
      <img id="gilbarco-logo" src="<?php echo __DIR__;?>/images/gvr-logo.png"/>
      <img id="verified-logo" src="<?php echo __DIR__;?>/images/verified-logo.png"/>
    </div>
    <!-- Section Details of the General Characteristics (clause 3.3)//-->
    <?php
      $array_general_characteristics_rows = array(
        array('Does the instrument comply with its Certificate(s) of Approval?', 'QA_029', 'Is the instrument being used in an appropriate manner?', 'QA_030'),
        array('Are all mandatory descriptive markings clearly and permanently marked on the data plate?', 'QA_031','Is the data plate fixed on the dispenser?', 'QA_032'),
        array('Is the dispenser complete?', 'QA_033','Is the dispenser clean?', 'QA_034'),
        array('Is the dispenser operational?', 'QA_035','Is the operation of the dispenser free of any apparent obstructions?', 'QA_036'),
        array('Is the dispenser firmly fixed on its foundations?', 'QA_037', 'Are all external panels secure?', 'QA_038'),
        array('Are the cover windows broken?', 'QA_039', 'Does the operator (and where applicable, the customer) have a clear and unobstructed view of the indicating device and the entire measuring process?', 'QA_040'),
        array('Do the indications of volume, unit price and total price correspond with the selected hose?', 'QA_041', 'Are all indications clearly visible under all conditions day and night?', 'QA_042'),
        array('Are the hoses in reasonable condition, e.g. they are not badly chafed, split, or worn through to the fabric?', 'QA_043', 'Are there any leaks?', 'QA_044'),
        array('For self-service systems: do the dispenser number(s) correspond with the console?', 'QA_045', 'Checking facility for indicating devices (clause 4.1)', 'QA_046'),
        array('Zero setting (clause 4.2)', 'QA_047', 'Price computing (clause 4.3)', 'QA_048'),
        array('Interlock (clause 4.4)', 'QA_049', 'Density and temperature settings (clause 4.5)', 'QA_050'),
        array('Pre-set indication (clause 4.6)', 'QA_051', '', ''),
        )
    ?>
    <table class="tbl-general-details">
    <tr>
      <th style="width: 18%">
        Date of test
      </th>
      <td style="width: 32%">
        <?php print $qa_001;?>
      </td>
      <th style="width: 18%">
        GVR Job number
      </th>
      <td  style="width: 32%">
        <?php print $qa_279;?>
      </td>
    </tr>
    <tr>
      <th>
        GVR Site number
      </th>
      <td>
        <?php print $qa_003;?>
      </td>
      <th>
        Employee name
      </th>
      <td>
        <?php print $qa_004;?>
      </td>
    </tr>
    <tr>
      <th>
        Verifier number
      </th>
      <td>
        <?php print $qa_005;?>
      </td>
      <th>
      </th>
      <td>
      </td>
    </tr>
    </table>
    <table id="tbl-general-characteristics">
    <tr>
      <th colspan="4" class="header">
        General Characteristics (clause 3.3)
      </th>
    </tr>
    <?php
    foreach ($array_general_characteristics_rows as $array_general_characteristics_row):?>
      <tr>
        <td style="width: 42%" class="label">
          <?php print $array_general_characteristics_row[0];?>
        </td>
        <td style="width: 8%">
          <?php print ${strtolower($array_general_characteristics_row[1])};?>
        </td>
        <td style="width: 42%" class="label">
          <?php print $array_general_characteristics_row[2];?>
        </td>
        <td style="width: 8%">
          <?php if ( ! empty($array_general_characteristics_row[3])):?>
            <?php print ${strtolower($array_general_characteristics_row[3])};?>
          <?php endif;?>
        </td>
      </tr>
    <?php endforeach;?>
    </table>
    <div class="page_number">
      [[page_cu]]/[[page_nb]]
    </div>
  </page>

  <page orientation="l">
    <div id="logo" class="small-logo">
      <img id="gilbarco-logo" src="<?php echo __DIR__;?>/images/gvr-logo.png"/>
      <img id="verified-logo" src="<?php echo __DIR__;?>/images/verified-logo.png"/>
    </div>
    <table class="tbl-general-details">
    <tr>
      <th style="width: 18%">
        Date of test
      </th>
      <td style="width: 32%">
        <?php print $qa_001;?>
      </td>
      <th style="width: 18%">
        GVR Job number
      </th>
      <td  style="width: 32%">
        <?php print $qa_279;?>
      </td>
    </tr>
    <tr>
      <th>
        GVR Site number
      </th>
      <td>
        <?php print $qa_003;?>
      </td>
      <th>
        Employee name
      </th>
      <td>
        <?php print $qa_004;?>
      </td>
    </tr>
    <tr>
      <th>
        Verifier number
      </th>
      <td>
        <?php print $qa_005;?>
      </td>
      <th>
      </th>
      <td>
      </td>
    </tr>
    </table>

    <table id="tbl-test-report-header-top" style="width: 947px;">
    <tr>
      <th colspan="6" class="header">
        <?php if (empty($qa_052) || $qa_052 == 'with-switch'):?>
          Test Report 1-1 for LPG Dispensers <u>with</u> V<sub>FD</sub>/V<sub>FD15</sub> Switch which are Tested Volumetrically using Master Meter
        <?php else:?>
          Test Report 1-2 for LPG Dispensers <u>without</u> V<sub>FD</sub>/V<sub>FD15</sub> Switch which are Tested Volumetrically using Master Meter
        <?php endif;?>
      </th>
    </tr>
    <tr>
      <th style="width: 20%">
        Master meter serial number
      </th>
      <td style="width: 15%">
        <?php print $qa_053;?>
      </td>
      <th style="width: 22%">
        Density displayed by dispenser
      </th>
      <td style="width: 13%">
        <?php print $qa_054;?><label class="unit">kg/L</label>
      </td>
      <th style="width: 28%">
        Temperature displayed by dispenser (T<sub>FDI</sub>)
      </th>
      <td style="width: 7%">
        <?php print $qa_055;?><label class="unit"><sup>o</sup>C</label>
      </td>
    </tr>
    </table>
    <table id="tbl-test-report-header" style="width: 994px;">
    <tr>
      <th colspan="9" class="header" style="width: 100%;">
        Hydrometer readings
      </th>
    </tr>
    <tr>
      <td  style="width: 25%;">
        <label>Observed P<sub>e</sub> = </label><?php print $qa_056;?><label class="unit">kPa</label>
      </td>
      <td  style="width: 25%;">
        <label>Observed temp = </label><?php print $qa_061;?><label class="unit"><sup>o</sup>C</label>
      </td>
      <td  style="width: 25%;">
        <label>Observed density = </label><?php print $qa_062;?><label class="unit">kg/L</label>
      </td>
      <td  rowspan="2" style="width: 25%;">
        <label>Density at 15 <sup>o</sup>C</label><br/><br/><label>D<sub>15</sub> = </label><?php print $qa_063;?><label class="unit">kg/L</label>
      </td>
    </tr>
    <tr>
      <td  style="width: 25%;">
        <label>Corrected P<sub>e</sub> = </label>
        <?php print $qa_057;?>
        <label class="unit">kPa</label>
      </td>
      <td  style="width: 25%;">
        <label>Corrected temp = </label>
        <?php print $qa_064;?>
        <label class="unit"><sup>o</sup>C</label>
      </td>
      <td  style="width: 25%;">
        <label>Corrected density = </label>
        <?php print $qa_065;?>
        <label class="unit">kg/L</label>
      </td>
    </tr>
    <tr class="tbl-test-report-header-last-row">
      <td colspan="9">
        <table id="table-totaliser" style="width: 800px;">
        <tr>
          <td style="width: 20%">
            Totaliser at the end =
          </td>
          <td style="width: 13%">
              <?php print $qa_058;?>
              <label class="unit">L</label>
          </td>
          <td style="width: 21%">
            Totaliser at the start =
          </td>
          <td style="width: 13%">
              <?php print $qa_060;?>
              <label class="unit">L</label>
          </td>
          <td style="width: 20%">
            Total volume used =
          </td>
          <td style="width: 13%">
              <?php print $qa_059;?>
              <label class="unit">L</label>
          </td>
        </tr>
        </table>
      </td>
    </tr>
    </table>

    <?php if (empty($qa_052) || $qa_052 == 'with-switch'):?>
      <table class="tbl-test-reports">
      <tr>
        <th style="width: 25%"></th>
        <th style="width: 10%">Delivery 1<br/>Q<sub>max</sub></th>
        <th style="width: 10%">Delivery 2<br/>Q<sub>max</sub></th>
        <th style="width: 10%">Delivery 3<br/>Q<sub>max</sub></th>
        <th style="width: 10%">Spare</th>
        <th style="width: 10%">Compensated with V<sub>FD</sub>/V<sub>FD15</sub>switch</th>
        <th style="width: 10%">Delivery 1 Q<sub>min</sub></th>
        <th style="width: 10%">Spare</th>
      </tr>
      <tr>
        <th>T<sub>MM</sub></th>
        <td class="figure"><?php print $qa_066;?><label class="unit"><sup>o</sup>C</label></td>
        <td class="figure"><?php print $qa_067;?><label class="unit"><sup>o</sup>C</label></td>
        <td class="figure"><?php print $qa_068;?><label class="unit"><sup>o</sup>C</label></td>
        <td class="figure"><?php print $qa_069;?><label class="unit"><sup>o</sup>C</label></td>
        <th></th>
        <td class="figure"><?php print $qa_070;?><label class="unit"><sup>o</sup>C</label></td>
        <td class="figure"><?php print $qa_071;?><label class="unit"><sup>o</sup>C</label></td>
      </tr>
      <tr>
        <th>P<sub>MM</sub></th>
        <td class="figure"><?php print $qa_072;?><label class="unit">kPa</label></td>
        <td class="figure"><?php print $qa_073;?><label class="unit">kPa</label></td>
        <td class="figure"><?php print $qa_074;?><label class="unit">kPa</label></td>
        <td class="figure"><?php print $qa_075;?><label class="unit">kPa</label></td>
        <th></th>
        <td class="figure"><?php print $qa_076;?><label class="unit">kPa</label></td>
        <td class="figure"><?php print $qa_077;?><label class="unit">kPa</label></td>
      </tr>
      <tr>
        <th>D<sub>P</sub></th>
        <td class="figure"><?php print $qa_078;?><label class="unit">kPa</label></td>
        <td class="figure"><?php print $qa_079;?><label class="unit">kPa</label></td>
        <td class="figure"><?php print $qa_080;?><label class="unit">kPa</label></td>
        <td class="figure"><?php print $qa_081;?><label class="unit">kPa</label></td>
        <th></th>
        <td class="figure"><?php print $qa_082;?><label class="unit">kPa</label></td>
        <td class="figure"><?php print $qa_083;?><label class="unit">kPa</label></td>
      </tr>
      <tr>
        <th>V<sub>MM</sub></th>
        <td class="figure"><?php print $qa_084;?><label class="unit">L</label></td>
        <td class="figure"><?php print $qa_085;?><label class="unit">L</label></td>
        <td class="figure"><?php print $qa_086;?><label class="unit">L</label></td>
        <td class="figure"><?php print $qa_087;?><label class="unit">L</label></td>
        <th></th>
        <td class="figure"><?php print $qa_088;?><label class="unit">L</label></td>
        <td class="figure"><?php print $qa_089;?><label class="unit">L</label></td>
      </tr>
      <tr>
        <th>Maximum achievable flow rate</th>
        <td class="figure"><?php print $qa_090;?><label class="unit">L/min</label></td>
        <td class="figure"><?php print $qa_091;?><label class="unit">L/min</label></td>
        <td class="figure"><?php print $qa_092;?><label class="unit">L/min</label></td>
        <td class="figure"><?php print $qa_093;?><label class="unit">L/min</label></td>
        <th></th>
        <td class="figure"><?php print $qa_094;?><label class="unit">L/min</label></td>
        <td class="figure"><?php print $qa_095;?><label class="unit">L/min</label></td>
      </tr>
      <tr>
        <th>MF<sub>MM</sub></th>
        <td class="figure"><?php print $qa_096;?></td>
        <td class="figure"><?php print $qa_097;?></td>
        <td class="figure"><?php print $qa_098;?></td>
        <td class="figure"><?php print $qa_099;?></td>
        <th></th>
        <td class="figure"><?php print $qa_100;?></td>
        <td class="figure"><?php print $qa_101;?></td>
      </tr>
      <tr>
        <th>C<sub>tiMM</sub>   (using density at 15<sup>o</sup>C, T<sub>MM</sub>)</th>
        <td class="figure"><?php print $qa_102;?></td>
        <td class="figure"><?php print $qa_103;?></td>
        <td class="figure"><?php print $qa_104;?></td>
        <td class="figure"><?php print $qa_105;?></td>
        <th></th>
        <td class="figure"><?php print $qa_106;?></td>
        <td class="figure"><?php print $qa_107;?></td>
      </tr>
      <tr>
        <th>C<sub>piMM</sub>    (using P<sub>e</sub> density at 15<sup>o</sup>C, T<sub>MM</sub>, P<sub>MM</sub>)</th>
        <td class="figure"><?php print $qa_108;?></td>
        <td class="figure"><?php print $qa_109;?></td>
        <td class="figure"><?php print $qa_110;?></td>
        <td class="figure"><?php print $qa_111;?></td>
        <th></th>
        <td class="figure"><?php print $qa_112;?></td>
        <td class="figure"><?php print $qa_113;?></td>
      </tr>
      <tr>
        <th>V<sub>REF</sub><span class="important-note">***</span>    (V<sub>MM</sub> * C<sub>tiMM</sub> * C<sub>piMM</sub> * MF<sub>MM</sub>)</th>
        <td class="figure"><?php print $qa_114;?><label class="unit">L</label></td>
        <td class="figure"><?php print $qa_115;?><label class="unit">L</label></td>
        <td class="figure"><?php print $qa_116;?><label class="unit">L</label></td>
        <td class="figure"><?php print $qa_117;?><label class="unit">L</label></td>
        <th></th>
        <td class="figure"><?php print $qa_118;?><label class="unit">L</label></td>
        <td class="figure"><?php print $qa_119;?><label class="unit">L</label></td>
      </tr>
      <tr>
        <th>T<sub>FD</sub></th>
        <td class="figure"><?php print $qa_120;?><label class="unit"><sup>o</sup>C</label></td>
        <td class="figure"><?php print $qa_121;?><label class="unit"><sup>o</sup>C</label></td>
        <td class="figure"><?php print $qa_122;?><label class="unit"><sup>o</sup>C</label></td>
        <td class="figure"><?php print $qa_123;?><label class="unit"><sup>o</sup>C</label></td>
        <td class="figure"><?php print $qa_124;?><label class="unit"><sup>o</sup>C</label></td>
        <td class="figure"><?php print $qa_125;?><label class="unit"><sup>o</sup>C</label></td>
        <td class="figure"><?php print $qa_126;?><label class="unit"><sup>o</sup>C</label></td>
      </tr>
      <tr>
        <th>T<sub>FD</sub> - T<sub>FDI</sub></th>
        <td class="figure"><?php print $qa_127;?><label class="unit"><sup>o</sup>C</label></td>
        <td class="figure"><?php print $qa_128;?><label class="unit"><sup>o</sup>C</label></td>
        <td class="figure"><?php print $qa_129;?><label class="unit"><sup>o</sup>C</label></td>
        <td class="figure"><?php print $qa_130;?><label class="unit"><sup>o</sup>C</label></td>
        <td class="figure"><?php print $qa_131;?><label class="unit"><sup>o</sup>C</label></td>
        <td class="figure"><?php print $qa_132;?><label class="unit"><sup>o</sup>C</label></td>
        <td class="figure"><?php print $qa_133;?><label class="unit"><sup>o</sup>C</label></td>
      </tr>
      <tr>
        <th>V<sub>FD</sub></th>
        <td class="figure"><?php print $qa_134;?><label class="unit">L</label></td>
        <td class="figure"><?php print $qa_135;?><label class="unit">L</label></td>
        <td class="figure"><?php print $qa_136;?><label class="unit">L</label></td>
        <td class="figure"><?php print $qa_137;?><label class="unit">L</label></td>
        <td class="figure"><?php print $qa_138;?><label class="unit">L</label></td>
        <td class="figure"><?php print $qa_139;?><label class="unit">L</label></td>
        <td class="figure"><?php print $qa_140;?><label class="unit">L</label></td>
      </tr>
      <tr>
        <th>V<sub>FD15</sub></th>
        <th></th>
        <th></th>
        <th></th>
        <th></th>
        <td class="figure"><?php print $qa_141;?><label class="unit">L</label></td>
        <th></th>
        <th></th>
      </tr>
      <tr>
        <th>C<sub>tiFD</sub> = (using density at 15<sup>o</sup>C, T<sub>FD</sub>)</th>
        <td class="figure"><?php print $qa_142;?></td>
        <td class="figure"><?php print $qa_143;?></td>
        <td class="figure"><?php print $qa_144;?></td>
        <td class="figure"><?php print $qa_145;?></td>
        <td class="figure"><?php print $qa_146;?></td>
        <td class="figure"><?php print $qa_147;?></td>
        <td class="figure"><?php print $qa_148;?></td>
      </tr>
      <tr>
        <th>V<sub>FD, C</sub> = (V<sub>FD</sub> * C<sub>tiFD</sub>)</th>
        <td class="figure"><?php print $qa_149;?><label class="unit">L</label></td>
        <td class="figure"><?php print $qa_150;?><label class="unit">L</label></td>
        <td class="figure"><?php print $qa_151;?><label class="unit">L</label></td>
        <td class="figure"><?php print $qa_152;?><label class="unit">L</label></td>
        <td class="figure"><?php print $qa_153;?><label class="unit">L</label></td>
        <td class="figure"><?php print $qa_154;?><label class="unit">L</label></td>
        <td class="figure"><?php print $qa_155;?><label class="unit">L</label></td>
      </tr>
      <tr>
        <th>E<sub>FD</sub> = [(V<sub>FD,C</sub> - V<sub>REF</sub>) / V<sub>REF</sub> * 100]</th>
        <td class="figure"><?php print $qa_156;?><label class="unit">%</label></td>
        <td class="figure"><?php print $qa_157;?><label class="unit">%</label></td>
        <td class="figure"><?php print $qa_158;?><label class="unit">%</label></td>
        <td class="figure"><?php print $qa_159;?><label class="unit">%</label></td>
        <th></th>
        <td class="figure"><?php print $qa_160;?><label class="unit">%</label></td>
        <td class="figure"><?php print $qa_161;?><label class="unit">%</label></td>
      </tr>
      <tr>
        <th>E<sub>C</sub>[(V<sub>FD15</sub> - V<sub>FD,C</sub>) / V<sub>FD,C</sub> * 100]</th>
        <th></th>
        <th></th>
        <th></th>
        <th></th>
        <td class="figure"><?php print $qa_162;?><label class="unit">%</label></td>
        <th></th>
        <th></th>
      </tr>
      </table>
      <span class="important-note">***</span>The correction factor MF<sub>MM</sub> is the meter factor obtained from a traceable measurement report.
    <?php else:?>
      <table class="tbl-test-reports">
      <tr>
        <th rowspan="2" style="width: 25%"></th>
        <th rowspan="2" style="width: 10%">Delivery 1<br/>Q<sub>max</sub></th>
        <th rowspan="2" style="width: 10%">Delivery 2<br/>Q<sub>max</sub></th>
        <th rowspan="2" style="width: 10%">Delivery 3<br/>Q<sub>max</sub></th>
        <th colspan="3" style="width: 30%">Compensated no V<sub>FD</sub>/V<sub>FD15</sub>switch</th>
        <th rowspan="2" style="width: 10%">Delivery 1 Q<sub>min</sub></th>
      </tr>
      <tr>
        <th style="width: 10%">Delivery 1 Q<sub>max</sub></th>
        <th style="width: 10%">Delivery 2 Q<sub>max</sub></th>
        <th style="width: 10%">Delivery 3 Q<sub>max</sub></th>
      </tr>
      <tr>
        <th>T<sub>MM</sub></th>
        <td class="figure"><?php print $qa_168;?><label class="unit"><sup>o</sup>C</label></td>
        <td class="figure"><?php print $qa_169;?><label class="unit"><sup>o</sup>C</label></td>
        <td class="figure"><?php print $qa_170;?><label class="unit"><sup>o</sup>C</label></td>
        <td class="figure"><?php print $qa_171;?><label class="unit"><sup>o</sup>C</label></td>
        <td class="figure"><?php print $qa_172;?><label class="unit"><sup>o</sup>C</label></td>
        <td class="figure"><?php print $qa_173;?><label class="unit"><sup>o</sup>C</label></td>
        <td class="figure"><?php print $qa_174;?><label class="unit"><sup>o</sup>C</label></td>
      </tr>
      <tr>
        <th>P<sub>MM</sub></th>
        <td class="figure"><?php print $qa_175;?><label class="unit">kPa</label></td>
        <td class="figure"><?php print $qa_176;?><label class="unit">kPa</label></td>
        <td class="figure"><?php print $qa_177;?><label class="unit">kPa</label></td>
        <td class="figure"><?php print $qa_178;?><label class="unit">kPa</label></td>
        <td class="figure"><?php print $qa_179;?><label class="unit">kPa</label></td>
        <td class="figure"><?php print $qa_180;?><label class="unit">kPa</label></td>
        <td class="figure"><?php print $qa_181;?><label class="unit">kPa</label></td>
      </tr>
      <tr>
        <th>D<sub>P</sub></th>
        <td class="figure"><?php print $qa_182;?><label class="unit">kPa</label></td>
        <td class="figure"><?php print $qa_183;?><label class="unit">kPa</label></td>
        <td class="figure"><?php print $qa_184;?><label class="unit">kPa</label></td>
        <td class="figure"><?php print $qa_185;?><label class="unit">kPa</label></td>
        <td class="figure"><?php print $qa_186;?><label class="unit">kPa</label></td>
        <td class="figure"><?php print $qa_187;?><label class="unit">kPa</label></td>
        <td class="figure"><?php print $qa_188;?><label class="unit">kPa</label></td>
      </tr>
      <tr>
        <th>V<sub>MM</sub></th>
        <td class="figure"><?php print $qa_189;?><label class="unit">L</label></td>
        <td class="figure"><?php print $qa_190;?><label class="unit">L</label></td>
        <td class="figure"><?php print $qa_191;?><label class="unit">L</label></td>
        <td class="figure"><?php print $qa_192;?><label class="unit">L</label></td>
        <td class="figure"><?php print $qa_193;?><label class="unit">L</label></td>
        <td class="figure"><?php print $qa_194;?><label class="unit">L</label></td>
        <td class="figure"><?php print $qa_195;?><label class="unit">L</label></td>
      </tr>
      <tr>
        <th>Maximum achievable flow rate</th>
        <td class="figure"><?php print $qa_196;?><label class="unit">L/min</label></td>
        <td class="figure"><?php print $qa_197;?><label class="unit">L/min</label></td>
        <td class="figure"><?php print $qa_198;?><label class="unit">L/min</label></td>
        <td class="figure"><?php print $qa_199;?><label class="unit">L/min</label></td>
        <td class="figure"><?php print $qa_200;?><label class="unit">L/min</label></td>
        <td class="figure"><?php print $qa_201;?><label class="unit">L/min</label></td>
        <td class="figure"><?php print $qa_202;?><label class="unit">L/min</label></td>
      </tr>
      <tr>
        <th>MF<sub>MM</sub></th>
        <td class="figure"><?php print $qa_203;?></td>
        <td class="figure"><?php print $qa_204;?></td>
        <td class="figure"><?php print $qa_205;?></td>
        <td class="figure"><?php print $qa_206;?></td>
        <td class="figure"><?php print $qa_207;?></td>
        <td class="figure"><?php print $qa_208;?></td>
        <td class="figure"><?php print $qa_209;?></td>
      </tr>
      <tr>
        <th>C<sub>tiMM</sub>   (using density at 15<sup>o</sup>C, T<sub>MM</sub>)</th>
        <td class="figure"><?php print $qa_210;?></td>
        <td class="figure"><?php print $qa_211;?></td>
        <td class="figure"><?php print $qa_212;?></td>
        <td class="figure"><?php print $qa_213;?></td>
        <td class="figure"><?php print $qa_214;?></td>
        <td class="figure"><?php print $qa_215;?></td>
        <td class="figure"><?php print $qa_216;?></td>
      </tr>
      <tr>
        <th>C<sub>piMM</sub>    (using P<sub>e</sub> density at 15<sup>o</sup>C, T<sub>MM</sub>, P<sub>MM</sub>)</th>
        <td class="figure"><?php print $qa_217;?></td>
        <td class="figure"><?php print $qa_218;?></td>
        <td class="figure"><?php print $qa_219;?></td>
        <td class="figure"><?php print $qa_220;?></td>
        <td class="figure"><?php print $qa_221;?></td>
        <td class="figure"><?php print $qa_222;?></td>
        <td class="figure"><?php print $qa_223;?></td>
      </tr>
      <tr>
        <th>V<sub>REF</sub>    (V<sub>MM</sub> * C<sub>tiMM</sub> * C<sub>piMM</sub> * MF<sub>MM</sub>)</th>
        <td class="figure"><?php print $qa_224;?><label class="unit">L</label></td>
        <td class="figure"><?php print $qa_225;?><label class="unit">L</label></td>
        <td class="figure"><?php print $qa_226;?><label class="unit">L</label></td>
        <td class="figure"><?php print $qa_227;?><label class="unit">L</label></td>
        <td class="figure"><?php print $qa_228;?><label class="unit">L</label></td>
        <td class="figure"><?php print $qa_229;?><label class="unit">L</label></td>
        <td class="figure"><?php print $qa_230;?><label class="unit">L</label></td>
      </tr>
      <tr>
        <th>T<sub>FD</sub></th>
        <td class="figure"><?php print $qa_231;?><label class="unit"><sup>o</sup>C</label></td>
        <td class="figure"><?php print $qa_232;?><label class="unit"><sup>o</sup>C</label></td>
        <td class="figure"><?php print $qa_233;?><label class="unit"><sup>o</sup>C</label></td>
        <td class="figure"><?php print $qa_234;?><label class="unit"><sup>o</sup>C</label></td>
        <td class="figure"><?php print $qa_235;?><label class="unit"><sup>o</sup>C</label></td>
        <td class="figure"><?php print $qa_236;?><label class="unit"><sup>o</sup>C</label></td>
        <td class="figure"><?php print $qa_237;?><label class="unit"><sup>o</sup>C</label></td>
      </tr>
      <tr>
        <th>T<sub>FD</sub> - T<sub>FDI</sub></th>
        <td class="figure"><?php print $qa_238;?><label class="unit"><sup>o</sup>C</label></td>
        <td class="figure"><?php print $qa_239;?><label class="unit"><sup>o</sup>C</label></td>
        <td class="figure"><?php print $qa_240;?><label class="unit"><sup>o</sup>C</label></td>
        <td class="figure"><?php print $qa_241;?><label class="unit"><sup>o</sup>C</label></td>
        <td class="figure"><?php print $qa_242;?><label class="unit"><sup>o</sup>C</label></td>
        <td class="figure"><?php print $qa_243;?><label class="unit"><sup>o</sup>C</label></td>
        <td class="figure"><?php print $qa_244;?><label class="unit"><sup>o</sup>C</label></td>
      </tr>
      <tr>
        <th>V<sub>FD</sub></th>
        <td class="figure"><?php print $qa_245;?><label class="unit">L</label></td>
        <td class="figure"><?php print $qa_246;?><label class="unit">L</label></td>
        <td class="figure"><?php print $qa_247;?><label class="unit">L</label></td>
        <th></th>
        <th></th>
        <th></th>
        <td class="figure"><?php print $qa_248;?><label class="unit">L</label></td>
      </tr>
      <tr>
        <th>V<sub>FD15</sub></th>
        <th></th>
        <th></th>
        <th></th>
        <td class="figure"><?php print $qa_249;?><label class="unit">L</label></td>
        <td class="figure"><?php print $qa_250;?><label class="unit">L</label></td>
        <td class="figure"><?php print $qa_251;?><label class="unit">L</label></td>
        <th></th>
      </tr>
      <tr>
        <th>C<sub>tiFD</sub> = (using density at 15<sup>o</sup>C, T<sub>FD</sub>)</th>
        <td class="figure"><?php print $qa_252;?></td>
        <td class="figure"><?php print $qa_253;?></td>
        <td class="figure"><?php print $qa_254;?></td>
        <td class="figure"><?php print $qa_255;?></td>
        <td class="figure"><?php print $qa_256;?></td>
        <td class="figure"><?php print $qa_257;?></td>
        <td class="figure"><?php print $qa_258;?></td>
      </tr>
      <tr>
        <th>V<sub>FD, C</sub> = (V<sub>FD</sub> * C<sub>tiFD</sub>)</th>
        <td class="figure"><?php print $qa_259;?><label class="unit">L</label></td>
        <td class="figure"><?php print $qa_260;?><label class="unit">L</label></td>
        <td class="figure"><?php print $qa_261;?><label class="unit">L</label></td>
        <td class="figure"><?php print $qa_262;?><label class="unit">L</label></td>
        <td class="figure"><?php print $qa_263;?><label class="unit">L</label></td>
        <td class="figure"><?php print $qa_264;?><label class="unit">L</label></td>
        <td class="figure"><?php print $qa_265;?><label class="unit">L</label></td>
      </tr>
      <tr>
        <th>E<sub>FD</sub> = [(V<sub>FD,C</sub> - V<sub>REF</sub>) / V<sub>REF</sub> * 100]</th>
        <td class="figure"><?php print $qa_266;?><label class="unit">%</label></td>
        <td class="figure"><?php print $qa_267;?><label class="unit">%</label></td>
        <td class="figure"><?php print $qa_268;?><label class="unit">%</label></td>
        <td class="figure"><?php print $qa_269;?><label class="unit">%</label></td>
        <td class="figure"><?php print $qa_270;?><label class="unit">%</label></td>
        <td class="figure"><?php print $qa_271;?><label class="unit">%</label></td>
        <td class="figure"><?php print $qa_272;?><label class="unit">%</label></td>
      </tr>
      <tr>
        <th></th>
        <td colspan="3">E<sub>AV</sub> = <?php print $qa_273;?><label class="unit">%</label></td>
        <td colspan="3">E<sub>AV,C</sub> = <?php print $qa_274;?><label class="unit">%</label></td>
        <th></th>
      </tr>
      <tr>
        <th>E<sub>C</sub>[(E<sub>AV,C</sub> - E<sub>AV</sub>) / V<sub>REF</sub>* 100]</th>
        <th colspan="3"></th>
        <td colspan="3">E<sub>C</sub> = <?php print $qa_275;?><label class="unit">%</label></td>
        <th></th>
      </tr>
      </table>
      <span class="important-note">***</span>The correction factor MF<sub>MM</sub> is the meter factor obtained from a traceable measurement report. <br/>
      ***For compensated delivery the conversion factor C<sub>tIFD</sub> must be set to unity.
    <?php endif;?>
    <div class="page_number">
      [[page_cu]]/[[page_nb]]
    </div>
  </page>

<?php

  try {

    $content = ob_get_clean();
    $form->generatePDF($id, $act_no, $filename, $content, $add_new);
  }
  catch(HTML2PDF_exception $e) {
    echo $e;
    exit;
  }

} else {
  print 'Please enter a query strings "actno", "filename" and "id" to load PDF';
}
/* End of file html2pdf_lpg.php */
/* Location: ./html2pdf_lpg.php */