<?php
/*
 * Gilbarco NMI Non LPG Form Postback
 * Copyright (c) 2016 Verified Pty Ltd
 * All rights reserved.
 */

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
/** Include database config */
require_once('../../config/config.php');
require_once('../../config/db.config.php');
/** Include classes */
require_once('../../classes/class.gilbarconmi_lpg_internal.php');
require_once('../../classes/class.gilbarconmi_lpg_excel_sheet.php');
/** Include Php library */
//require_once('phpLibs/lzwCompression/lzwc.php');


  $return_ini = ini_set('max_execution_time', 0);
  if ( ! $return_ini) {
    echo 'INI setting fails.';
  }

  $start_date = $_GET['startdate'];
  $end_date = $_GET['enddate'];

  $form = new Gilbarconmi_lpg();
  $cls_excel_sheet = new Gilbarconmi_lpg_excel_sheet;

  // Validate input date format?
  if ( ! $form->validateDate($start_date)) {
    die('Invalid start date. Date must be in Y-m-d, eg. 2019-05-27');
  }
  if ( ! $form->validateDate($end_date)) {
    die('Invalid end date. Date must be in Y-m-d, eg. 2019-05-27');
  }

  // Validate date range?
  $start_time = strtotime($start_date);
  $end_time = strtotime($end_date);
  if ($start_time > $end_time) {
    die('Invalid date range. START DATE must be the same or earlier then END DATE.');
  }

  try {
    $conn = new PDO('sqlsrv:Server=' . SERVER_NAME . ';Database=' . DB_NAME, DB_USER, DB_PASSWORD);
  } catch (PDOException $e) {
    die('Connection failed: ' . $e->getMessage());
  }

  $array_records = array();
  $stmt = $conn->prepare(
    'SELECT * FROM [' . DB_NAME . '].[dbo].[gilbarconmi_lpg] '
      . "WHERE [" . DB_NAME . "].[dbo].[gilbarconmi_lpg].created_date BETWEEN CONVERT(DATETIME, '" . $start_date . " 00:00:00') AND CONVERT(DATETIME, '" . $end_date . " 23:59:59') "
      . "ORDER BY created_date"
    );
  //$stmt->bindValue(':id', $start_id, PDO::PARAM_STR);
  $stmt->execute();
  if ($stmt->rowCount()) {
    $array_records = $stmt->fetchAll();
  }
  $stmt = null;
  $conn = null;

  // Decide whether https or http?
  $ssl      = ( ! empty( $_SERVER['HTTPS'] ) && $_SERVER['HTTPS'] == 'on' );
  $sp       = strtolower( $_SERVER['SERVER_PROTOCOL'] );
  $protocol = substr( $sp, 0, strpos( $sp, '/' ) ) . ( ( $ssl ) ? 's' : '' );

  // Get script name's path and then recreate a new return url path
  $array_script_name = explode('/', $_SERVER['SCRIPT_NAME']);

  $length = count($array_records);
  $index = 0;
  echo 'Total records: ' . $length . '<br/>';
  while ($index < $length) {
  //foreach ($array_records as index => $record) {
    echo $index . '. Processing ID: ' . $array_records[$index]['id'] . ', act no. ' . $array_records[$index]['QA_002'] . '<br/>';
    if (sizeof($array_script_name) > 1) {
      $script_name_path = '';
      for ($i = 0; $i < 2; ++$i) {
        if ($i > 0) {
          $script_name_path .= '/';
        }
        $script_name_path .= $array_script_name[$i];
      }
      $excel_sheet_url = $protocol . '://' . $_SERVER['HTTP_HOST'] . $script_name_path . '/generate_excel_sheet.php?actno=' . urlencode($array_records[$index]['QA_002']);
      $pdf_url = $protocol . '://' . $_SERVER['HTTP_HOST'] . $script_name_path . '/html2pdf_lpg.php?actno=' . urlencode($array_records[$index]['QA_002']);
    } else {
      $excel_sheet_url = $protocol . '://' . $_SERVER['HTTP_HOST'] . '/generate_excel_sheet.php?actno=' . urlencode($array_records[$index]['QA_002']);
      $pdf_url = $protocol . '://' . $_SERVER['HTTP_HOST'] . '/html2pdf_lpg.php?actno=' . urlencode($array_records[$index]['QA_002']);
    }

    $excel_sheet_url = str_replace('s://' . DOMAIN_NAME,'://localhost', strtolower($excel_sheet_url));
    $pdf_url = str_replace('s://' . DOMAIN_NAME,'://localhost', strtolower($pdf_url));
    $excel_sheet_url = str_replace('://' . DOMAIN_NAME,'://localhost', strtolower($excel_sheet_url));
    $pdf_url = str_replace('://' . DOMAIN_NAME,'://localhost', strtolower($pdf_url));

    $excel_sheet_url = str_replace('s://prtl.verified.com.au','://localhost', strtolower($excel_sheet_url));
    $pdf_url = str_replace('s://prtl.verified.com.au','://localhost', strtolower($pdf_url));
    $excel_sheet_url = str_replace('://prtl.verified.com.au','://localhost', strtolower($excel_sheet_url));
    $pdf_url = str_replace('://prtl.verified.com.au','://localhost', strtolower($pdf_url));

    $excel_sheet_url = str_replace('s://melb.verified.com.au','://localhost', strtolower($excel_sheet_url));
    $pdf_url = str_replace('s://melb.verified.com.au','://localhost', strtolower($pdf_url));
    $excel_sheet_url = str_replace('://melb.verified.com.au','://localhost', strtolower($excel_sheet_url));
    $pdf_url = str_replace('://melb.verified.com.au','://localhost', strtolower($pdf_url));

    $excel_sheet_url = str_replace('s://nsw.verified.com.au','://localhost', strtolower($excel_sheet_url));
    $pdf_url = str_replace('s://nsw.verified.com.au','://localhost', strtolower($pdf_url));
    $excel_sheet_url = str_replace('://nsw.verified.com.au','://localhost', strtolower($excel_sheet_url));
    $pdf_url = str_replace('://nsw.verified.com.au','://localhost', strtolower($pdf_url));

    $excel_sheet_url = str_replace('s://www.verified.com.au','://localhost', strtolower($excel_sheet_url));
    $pdf_url = str_replace('s://www.verified.com.au','://localhost', strtolower($pdf_url));
    $excel_sheet_url = str_replace('://www.verified.com.au','://localhost', strtolower($excel_sheet_url));
    $pdf_url = str_replace('://www.verified.com.au','://localhost', strtolower($pdf_url));

    $excel_sheet_url = str_replace('s://verified.com.au','://localhost', strtolower($excel_sheet_url));
    $pdf_url = str_replace('s://verified.com.au','://localhost', strtolower($pdf_url));
    $excel_sheet_url = str_replace('://verified.com.au','://localhost', strtolower($excel_sheet_url));
    $pdf_url = str_replace('://verified.com.au','://localhost', strtolower($pdf_url));


    $excel_filename =  $cls_excel_sheet->makeUpExcelSheetFileName($array_records[$index]['QA_002']);
    $pdf_filename = $form->makeUpPDFFileName($array_records[$index]['QA_002']);

    $excel_sheet_url .= '&filename=' . urlencode($excel_filename) . '&id=' . $array_records[$index]['id'] . '&addnew=1';
    $pdf_url .= '&filename=' . urlencode($pdf_filename) . '&id=' . $array_records[$index]['id'] . '&addnew=1';

echo $excel_sheet_url.'<br/>';
echo $pdf_url . '<br/>';

    // Generate Excel Sheet
    $x_excel = 'c:\SiteShoter\CutyCapt.exe --url="' . $excel_sheet_url . '" ';
    $x_excel .= '--min-width=440 --delay=12500 --out=' . dirname(__FILE__) . '\..\..\outputs\excel\excelsheet_dummy_screenshot.png';
    //exec($xpdf.' > /dev/null 2>/dev/null &');
    pclose(popen("start " . $x_excel , "r"));

    // Generate PDF
    $x_pdf = 'c:\SiteShoter\CutyCapt.exe --url="' . $pdf_url . '" ';
    $x_pdf .= '--min-width=440 --delay=26500 --out=' . dirname(__FILE__) . '\..\..\outputs\pdf\pdf_dummy_screenshot.png';
    //exec($xpdf.' > /dev/null 2>/dev/null &');
    pclose(popen("start " . $x_pdf , "r"));

    if ($index % 5 == 0) {
      sleep(25);
    }
    ++$index;
  }


/* End of file emulate_pdf_excelsheet.php */
/* Location: ./emulate_pdf_excelsheet.php */