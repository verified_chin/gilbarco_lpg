<?php
/*
 * Gilbarco NMI LPG Form Bulk Postback
 * Copyright (c) 2016 Verified Pty Ltd
 * All rights reserved.
 */
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

/** Include database config */
require_once('../../config/config.php');
require_once('../../config/db.config.php');
/** Include classes */
require_once('../../classes/class.gilbarconmi_lpg_internal.php');
require_once('../../classes/class.gilbarconmi_lpg_excel_sheet.php');
/** Include Php library */
//require_once('phpLibs/lzwCompression/lzwc.php');

$return_ini = ini_set('max_execution_time', 0);
if ( ! $return_ini) {
  die('Custom INI setting for max execute time fails.');
}

if ( ! isset($_GET['inputfile']) || (isset($_GET['inputfile']) && empty($_GET['inputfile']))) {
  die('Invalid input file.');
}

// Constants
define('BACKUP_INSTRUCTION_UPLOAD_PATH', getcwd() . '/../../uploads/bulkbackup/');
define('BACKUP_INPUT_PATH', getcwd() . '/../../outputs/backup/');
define('ERROR_LOG_PATH', getcwd() . '/../../errorLogs/');

// Check source files from input file if exist?
$success = TRUE;
$array_fail_files = array();
$fhouter = fopen(BACKUP_INSTRUCTION_UPLOAD_PATH . $_GET['inputfile'], 'r');
while ($line = fgets($fhouter)) {
  $backup_filename = str_replace(chr(13), '', str_replace(chr(10), '', str_replace(chr(13) . chr(10), '', $line)));
  $filepath = BACKUP_INPUT_PATH . $backup_filename;
  if ( ! file_exists($filepath)) {
    // Log failure not found source file
    $array_fail_files[] = $filepath;
    $success = FALSE;
  }
}
if ( ! $success) {
  // Stop process and output error log and error message to the screen
  $output_log_file = TRUE;
  $filepath = ERROR_LOG_PATH . date('Ymd') . '/';
  if ( ! file_exists($filepath)) {
    if ( ! mkdir($filepath, 0777, TRUE)) {
      $output_log_file = FALSE;
    }
  }
  if ( ! $output_log_file) {
    $filepath = ERROR_LOG_PATH . '/';
  }

  $fp = fopen($filepath . date('Y_m_d') . 'bulk_backup_error_log.txt', 'a+');
  fwrite($fp, 'Occurred on: ' . date('Y-m-d H:i:s') . PHP_EOL);
  fwrite($fp, 'No files have been imported. File(s) not found as below:' . PHP_EOL);
  foreach ($array_fail_files as $index => $file) {
    fwrite($fp, $file);
  }
  fwrite($fp, PHP_EOL . PHP_EOL);
  fclose($fp);
  die('<p style="color:red;font-weight:bold">Error occurred. <ul>Process has been stopped. No files have been imported.</u> Please check error log ' . $filepath . date('Y_m_d') . 'bulk_backup_error_log.txt.</p>');
}

echo '<p>Each backup file will take at least 25 seconds for generating the Excel and PDF reports. Processing now..<p>';

$form = new Gilbarconmi_lpg();
$flog = fopen(BACKUP_INSTRUCTION_UPLOAD_PATH . 'processedlog.txt', 'a+');
fwrite($flog, 'Processing on: ' . date('Y-m-d H:i:s') . PHP_EOL);
fclose($flog);

$success = TRUE;
$return_screen_data = array();
$return_data = array();
$fhouter = fopen(BACKUP_INSTRUCTION_UPLOAD_PATH . $_GET['inputfile'], 'r');
while ($line = fgets($fhouter)) {
  $backup_filename = str_replace(chr(13), '', str_replace(chr(10), '', str_replace(chr(13) . chr(10), '', $line)));
  $filepath = BACKUP_INPUT_PATH . $backup_filename;
  if (file_exists($filepath)) {

    $array_raw = array();

    // Initialise array
    for ($count = 1; $count <= 280; $count++) {
      $array_raw['QA_' . str_pad($count, 3, '0', STR_PAD_LEFT)] = '';
    }
    $array_raw['IncrementNumber'] = '';
    $array_raw['client_submitted_date'] = '';
    $array_raw['GPSlocation'] = '';

    // Open backup file for processing
    $fh = fopen(BACKUP_INPUT_PATH . $backup_filename, 'r');
    while ($line = fgets($fh)) {
      $array = explode(': ', $line);
      $array[1] = str_replace(chr(13), '', str_replace(chr(10), '', $array[1]));
      $array[0] = str_replace('~', '', str_replace(':', '', $array[0]));
      if ($array[0] == 'QA_280') {
        $temp_array = explode('~', $array[1]);
        if (count($temp_array) < 7) {
          for ($i = 0; $length = 7 - count($temp_array), $i < $length; $i++) {
            $array[1] .= '~';
          }
        }
      }
      $array_raw[trim($array[0])] = $array[1];
    }
    fclose($fh);



    // Output data backup flatfile.
	  $form->fields = array();
    //$form->outputDataBackupFlatfile($array_raw);

    // Decide whether https or http?
    $ssl      = ( ! empty( $_SERVER['HTTPS'] ) && $_SERVER['HTTPS'] == 'on' );
    $sp       = strtolower( $_SERVER['SERVER_PROTOCOL'] );
    $protocol = substr( $sp, 0, strpos( $sp, '/' ) ) . ( ( $ssl ) ? 's' : '' );

    // Get script name's path and then recreate a new return url path
    $array_script_name = explode('/', $_SERVER['SCRIPT_NAME']);
    if (sizeof($array_script_name) > 1) {
      $script_name_path = '';
      for ($i = 0; $i < 2; ++$i) {
        if ($i > 0) {
          $script_name_path .= '/';
        }
        $script_name_path .= $array_script_name[$i];
      }
      $excel_sheet_url = $protocol . '://' . $_SERVER['HTTP_HOST'] . $script_name_path . '/generate_excel_sheet.php?actno=' . urlencode($array_raw['QA_002']);
      $pdf_url = $protocol . '://' . $_SERVER['HTTP_HOST'] . $script_name_path . '/html2pdf_lpg.php?actno=' . urlencode($array_raw['QA_002']);
    } else {
      $excel_sheet_url = $protocol . '://' . $_SERVER['HTTP_HOST'] . '/generate_excel_sheet.php?actno=' . urlencode($array_raw['QA_002']);
      $pdf_url = $protocol . '://' . $_SERVER['HTTP_HOST'] . '/html2pdf_lpg.php?actno=' . urlencode($array_raw['QA_002']);
    }

    $excel_sheet_url = str_replace('s://' . DOMAIN_NAME,'://localhost', strtolower($excel_sheet_url));
    $pdf_url = str_replace('s://' . DOMAIN_NAME,'://localhost', strtolower($pdf_url));
    $excel_sheet_url = str_replace('://' . DOMAIN_NAME,'://localhost', strtolower($excel_sheet_url));
    $pdf_url = str_replace('://' . DOMAIN_NAME,'://localhost', strtolower($pdf_url));

    $excel_sheet_url = str_replace('s://prtl.verified.com.au','://localhost', strtolower($excel_sheet_url));
    $pdf_url = str_replace('s://prtl.verified.com.au','://localhost', strtolower($pdf_url));
    $excel_sheet_url = str_replace('://prtl.verified.com.au','://localhost', strtolower($excel_sheet_url));
    $pdf_url = str_replace('://prtl.verified.com.au','://localhost', strtolower($pdf_url));

    $excel_sheet_url = str_replace('s://melb.verified.com.au','://localhost', strtolower($excel_sheet_url));
    $pdf_url = str_replace('s://melb.verified.com.au','://localhost', strtolower($pdf_url));
    $excel_sheet_url = str_replace('://melb.verified.com.au','://localhost', strtolower($excel_sheet_url));
    $pdf_url = str_replace('://melb.verified.com.au','://localhost', strtolower($pdf_url));

    $excel_sheet_url = str_replace('s://nsw.verified.com.au','://localhost', strtolower($excel_sheet_url));
    $pdf_url = str_replace('s://nsw.verified.com.au','://localhost', strtolower($pdf_url));
    $excel_sheet_url = str_replace('://nsw.verified.com.au','://localhost', strtolower($excel_sheet_url));
    $pdf_url = str_replace('://nsw.verified.com.au','://localhost', strtolower($pdf_url));

    $excel_sheet_url = str_replace('s://www.verified.com.au','://localhost', strtolower($excel_sheet_url));
    $pdf_url = str_replace('s://www.verified.com.au','://localhost', strtolower($pdf_url));
    $excel_sheet_url = str_replace('://www.verified.com.au','://localhost', strtolower($excel_sheet_url));
    $pdf_url = str_replace('://www.verified.com.au','://localhost', strtolower($pdf_url));

    $excel_sheet_url = str_replace('s://verified.com.au','://localhost', strtolower($excel_sheet_url));
    $pdf_url = str_replace('s://verified.com.au','://localhost', strtolower($pdf_url));
    $excel_sheet_url = str_replace('://verified.com.au','://localhost', strtolower($excel_sheet_url));
    $pdf_url = str_replace('://verified.com.au','://localhost', strtolower($pdf_url));

    // Save form fields
    $array_return = $form->saveData($array_raw);
    $return_id = $array_return[0];
    $add_new = $array_return[1];
    if ($return_id) {

      $cls_excel_sheet = new Gilbarconmi_lpg_excel_sheet();
      $excel_filename =  $cls_excel_sheet->makeUpExcelSheetFileName($array_raw['QA_002']);
      $pdf_filename = $form->makeUpPDFFileName($array_raw['QA_002']);

      $excel_sheet_url .= '&filename=' . urlencode($excel_filename) . '&id=' . $return_id . '&addnew=' . $add_new;
      $pdf_url .= '&filename=' . urlencode($pdf_filename) . '&id=' . $return_id . '&addnew=' . $add_new;
echo $pdf_url .'<br/>';
      // Generate Excel Sheet
      $x_excel = 'c:\SiteShoter\CutyCapt.exe --url="' . $excel_sheet_url . '" ';
      $x_excel .= '--min-width=440 --delay=12500 --out=' . dirname(__FILE__) . '\..\..\outputs\excel\excelsheet_dummy_screenshot.png';
      //exec($xpdf.' > /dev/null 2>/dev/null &');
      pclose(popen("start " . $x_excel, "r"));

      // Generate PDF
      $x_pdf = 'c:\SiteShoter\CutyCapt.exe --url="' . $pdf_url . '" ';
      $x_pdf .= '--min-width=440 --delay=26500 --out=' . dirname(__FILE__) . '\..\..\outputs\pdf\pdf_dummy_screenshot.png';
      //exec($xpdf.' > /dev/null 2>/dev/null &');
      pclose(popen("start " . $x_pdf, "r"));

      $return_id = str_pad($return_id, 7, '0', STR_PAD_LEFT);
      $return_screen_data[] = '<span style="color: green; font-weight: bold;">Success ' . $backup_filename . ', ' . $array_raw['QA_002'] . ', return ID: ' . $return_id .'</span><br/>';
      $return_data[] = 'Success ' . $backup_filename . ', ' . $array_raw['QA_002'] . ', return ID: ' . $return_id;

      // Sleep for 25 seconds allowing external calls to generating excelsheet and PDF
      sleep(25);
    } else {
      $return_screen_data[] = '<span style="color: red; font-weight: bold;">Fail ' . $backup_filename . ', ' . $array_raw['QA_002'] .'</span><br/>';
      $return_data[] = 'Fail ' . $backup_filename . ', ' . $array_raw['QA_002'];
      $success = FALSE;
    }
  	$flog = fopen(BACKUP_INSTRUCTION_UPLOAD_PATH . 'processedlog.txt', 'a+');
  	fwrite($flog, BACKUP_INPUT_PATH . $backup_filename . PHP_EOL);
  	fclose($flog);
  }

}
fclose($fhouter);

if ( ! $success) {
  // Record failure(s) to error log and display error message to the screen
  $output_log_file = TRUE;
  $filepath = ERROR_LOG_PATH . date('Ymd') . '/';
  if ( ! file_exists($filepath)) {
    if ( ! mkdir($filepath, 0777, TRUE)) {
      $output_log_file = FALSE;
    }
  }
  if ( ! $output_log_file) {
    $filepath = ERROR_LOG_PATH . '/';
  }

  $fp = fopen($filepath . date('Y_m_d') . 'bulk_backup_error_log.txt', 'a+');
  fwrite($fp, 'Occurred on: ' . date('Y-m-d H:i:s') . PHP_EOL);
  fwrite($fp, 'Success files have been successfully imported however not those failed files.' . PHP_EOL);
  foreach ($return_data as $index => $line) {
    fwrite($fp, $line);
  }
  fwrite($fp, PHP_EOL . PHP_EOL);
  fclose($fp);
  echo '<p style="color:red;font-weight:bold">Error occurred. <u>Success files are imported but failed files are being rejected.</u> Please check error log ' . $filepath . date('Y_m_d') . 'bulk_backup_error_log.txt.</p>';
  foreach ($return_screen_data as $index => $line) {
    echo $line;
  }
} else {
  echo '<p style="color:green;font-weight:bold">Successfully complete. All backup are successfully imported.</p>';
}

/* End of file postback_bulk_internal.php */
/* Location: ./postback_bulk_internal.php */