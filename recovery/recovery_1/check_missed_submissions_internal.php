<?php
/*
 * Gilbarco NMI Non LPG Form Bulk Miss Checking
 * Copyright (c) 2019 Verified Pty Ltd
 * All rights reserved.
 */

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

/** Include database config */
require_once('../../config/db.config.php');
/** Include classes */
require_once('../../classes/class.gilbarconmi_lpg_internal.php');


$return_ini = ini_set('max_execution_time', 0);
if ( ! $return_ini) {
  die('Custom INI setting for max execute time fails.');
}

if ( ! isset($_GET['startdate']) || (isset($_GET['startdate']) && empty($_GET['startdate']))) {
  die('Expect "startdate" on querystring.');
}
if ( ! isset($_GET['enddate']) || (isset($_GET['enddate']) && empty($_GET['enddate']))) {
  die('Expect "enddate" on querystring.');
}

// Set class object
$form = new Gilbarconmi_lpg();

$start_date = $_GET['startdate'];
$end_date = $_GET['enddate'];
// Validate input date format?
if ( ! $form->validateDate($start_date)) {
  die('Invalid start date. Date must be in Y-m-d, eg. 2019-05-27');
}
if ( ! $form->validateDate($end_date)) {
  die('Invalid end date. Date must be in Y-m-d, eg. 2019-05-27');
}

// Validate date range?
$start_time = strtotime($start_date);
$end_time = strtotime($end_date);
if ($start_time > $end_time) {
  die('Invalid date range. START DATE must be the same or earlier then END DATE.');
}

// Constants
define('BACKUP_INSTRUCTION_UPLOAD_PATH', getcwd() . '/../../uploads/bulkbackup/');
define('BACKUP_INPUT_PATH', getcwd() . '/../../outputs/backup/');
define('ERROR_LOG_PATH', getcwd() . '/../../errorLogs/');

// Unlink existing output file
if (file_exists(BACKUP_INSTRUCTION_UPLOAD_PATH . 'bulk_validate_missed_submissions.txt')) {
  unlink(BACKUP_INSTRUCTION_UPLOAD_PATH . 'bulk_validate_missed_submissions.txt');
}

$next_date = $start_date;
$next_time = $start_time;
// Loop through each day until the end date.
while ($next_time <= $end_time) {

  // Initialise array
  $array_raw = array();
  for ($count = 1; $count <= 280; $count++) {
    $array_raw['QA_' . str_pad($count, 3, '0', STR_PAD_LEFT)] = '';
  }
  $array_raw['IncrementNumber'] = '';
  $array_raw['client_submitted_date'] = '';
  $array_raw['GPSlocation'] = '';

  // Check if folder or file exists?
  $path = BACKUP_INPUT_PATH .str_replace('-', '', $next_date);
  foreach(glob($path .'/*.*') as $file) {
    echo '<br><br>' . $file.'<br>';
    // Get input file name
    $array_filename = explode('/', $file);
    $input_filename = end($array_filename);
    // Find file's extension
    $array_input_filename_ext = explode('.', $input_filename);
    $input_filename_ext = '';
    $length = count($array_input_filename_ext);
    if ($length > 1) {
      $input_filename_ext = strtolower(end($array_input_filename_ext));
    }

    // Valid backup log's fiel extention?
    if (strtolower($input_filename_ext) === 'txt') {

      // Open backup log file and read content and populate array.
      $fh = fopen($path . '/' . $input_filename,'r');
      while ($line = fgets($fh)) {
        $array = explode(': ', $line);
        if (count($array) > 1) {
          $array[1] = str_replace(chr(13), '', str_replace(chr(10), '', $array[1]));
          $array[0] = str_replace('~', '', str_replace(':', '', $array[0]));
          if ($array[0] == 'QA_280') {
            $temp_array = explode('~', $array[1]);
            if (count($temp_array) < 7) {
              for ($i = 0; $length = 7 - count($temp_array), $i < $length; $i++) {
                $array[1] .= '~';
              }
            }
          }
          $array_raw[trim($array[0])] = $array[1];
          //echo $array[0] . ' => ' . $array_raw[$array[0]] . '<br/>';
        }
      }
      fclose($fh);

      // Validate if submission successful?
      if ( ! $form->validateSubmissionSuccessful($array_raw)) {
        $fp = fopen(BACKUP_INSTRUCTION_UPLOAD_PATH . 'bulk_validate_missed_submissions.txt', 'a+');
        fwrite($fp, str_replace('-', '', $next_date) . '/' . $input_filename);
        fwrite($fp, PHP_EOL);
        fclose($fp);

        // Output a csv file for openning in Excelsheet in easy comparison of logs.
        $print_header = FALSE;
        if ( ! file_exists(BACKUP_INSTRUCTION_UPLOAD_PATH . 'bulk_validate_missed_submissions.csv')) {
          $print_header = TRUE;
        }
        $fp2 = fopen(BACKUP_INSTRUCTION_UPLOAD_PATH . 'bulk_validate_missed_submissions.csv', 'a+');
        if ($print_header) {
          $s_csv = '';
          foreach ($array_raw as $csv_index => $csv_value) {
            if ( ! empty($s_csv)) {
              $s_csv .= ',';
            }
            $s_csv .= '"' . $csv_index . '"';
          }
          fwrite($fp2, $s_csv);
          fwrite($fp2, PHP_EOL);
        }
        $s_csv = '';
        foreach ($array_raw as $csv_index => $csv_value) {
          if ( ! empty($s_csv)) {
            $s_csv .= ',';
          }
          $s_csv .= '"' . $csv_value . '"';
        }
        fwrite($fp2, $s_csv);
        fwrite($fp2, PHP_EOL);
        fclose($fp2);
      }

    }
  }

  // Get next day's date
  $next_date = date('Y-m-d', strtotime($next_date .' +1 day'));
  $next_time = strtotime($next_date);
}


/* End of file check_missed_submissions_internal.php */
/* Location: ./check_missed_submissions_internal.php */