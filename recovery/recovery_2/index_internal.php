<?php
/*
 * Gilbarco NMI LPG Form
 * Copyright (c) 2016 Verified Pty Ltd
 * All rights reserved.
 */

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

/** Include database config */
require_once('../../config/db.config.php');
/** Include classes */
require_once('../../classes/class.gilbarconmi_lpg.php');



// Initialise variables
$gvr_site_number = '';
$job_no = '';
$employee_name = '';
$verified_number = '';
$name_of_owner = '';
$address_of_owner = '';
$trading_name = '';
$address_of_instrument_location = '';
$make = '';
$model = '';
$serial_number = '';
$flowrate_range = '';
$regulation_cert_no = '';
$cert_expiry_date = '';
$cert_expiry_date_day = '';
$cert_expiry_date_month = '';
$cert_expiry_date_year = '';
$qa_280 = '';
$array_raw = array();

// Initialise array
for ($count = 1; $count <= 280; $count++) {
  $array_raw['QA_' . str_pad($count, 3, '0', STR_PAD_LEFT)] = '';
}
$array_raw['IncrementNumber'] = '';

if (isset($_GET['rawfile']) && ! empty($_GET['rawfile'])) {
//echo __DIR__ . '/outputs/backup/' . $_GET['rawfile'];
  $fh = fopen(getcwd() . '/outputs/backup/' . $_GET['rawfile'],'r');
  while ($line = fgets($fh)) {
    $array = explode(': ', $line);
    $array[1] = str_replace(chr(13), '', str_replace(chr(10), '', $array[1]));
//echo $line.'->' . $array[0].';;;;;'.$array[1].'<br/>';
    $array[0] = str_replace('~', '', str_replace(':', '', $array[0]));
    if ($array[0] == 'QA_280') {
      $temp_array = explode('~', $array[1]);
      if (count($temp_array) < 7) {
        for ($i = 0; $length = 7 - count($temp_array), $i < $length; $i++) {
          $array[1] .= '~';
        }
      }
//      echo $array[1];
    }
    $array_raw[$array[0]] = $array[1];
//echo $array[0] . ' => ' . $array_raw[$array[0]] . '<br/>';
  }
  fclose($fh);
} else {
  die('Invalid raw file.');
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
<title>Calibration Check/Certification Report for LPG Dispensers</title>
  <meta charset="UTF-8" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <meta name="apple-mobile-web-app-capable" content="yes">
  <!--<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" >//-->
  <meta name="viewport" content="user-scalable=no, width=device-width, initial-scale=1, maximum-scale=1">
  <meta http-equiv="Pragma" content="no-cache" />
  <meta http-equiv="CACHE-CONTROL" content="NO-CACHE" />

  <!-- Latest compiled and minified CSS -->
  <link rel="stylesheet" href="bootstrap/bootstrap-3.3.6-dist/css/bootstrap.min.css">
  <link rel="stylesheet" href="js/libraries/jquery-ui-bootstrap-jquery-ui-bootstrap-71f2e47/css/custom-theme/jquery-ui-1.10.0.custom.css" rel="stylesheet">

  <style>
    /* General */
    body {
      font-family: "Lucida Grande", "Segoe UI", Ubuntu, Cantarell, Arial , sans-serif;
      margin-left: 10px;
      margin-right: 10px;
    }
    .container {
      width: 1000px;
    }
    .clear-float {
      clear: both;
    }
    h1, h2, h3, h4, h5 {
      font-weight: 700;
    }
    img {
      width: 100%;
    }
    h3 a, h4 a {
      width: 100%;
      display: block;
      padding-right: 30px;
    }
    h3 a:hover {
      text-decoration: none;
    }
    label {
      padding-top: 7px;
      padding-left: 10px;
      padding-right: 10px;
      height: 28px;
    }
    .date-picker {
      background-color: #fff !important;
    }

    /* Pop up dialogue box */
    #list-group-fra {
      border-bottom-left-radius: 4px;
      border-top-left-radius: 4px;
      border-bottom-right-radius: 4px;
      border-top-right-radius: 4px;
      border: 1px solid #ccc;
      height: 260px;
      overflow-y: scroll;
      -webkit-overflow-scrolling: touch;
    }
    #list-group-fra .list-group-item {
      border-bottom-left-radius: 0 !important;
      border-top-left-radius: 0 !important;
      border-bottom-right-radius: 0 !important;
      border-top-right-radius: 0 !important;
    }
    .loadjob-buttons {
      margin-top: 10px;
      margin-bottom: 10px;
    }
    #dialog-error-msg {
      font-weight: bold;
      color: red;
      margin-bottom: 10px;
      border: 1px solid red;
      background-color: #FDF1FC;
      padding: 5px;
      display: none;
    }

    #form-head .panel {
      margin-top: 20px;
    }
    .panel-heading {
      position: relative;
    }
    .panel-heading a:after {
      font-family: 'Glyphicons Halflings';
      content: '\e114';
      position: absolute;
      right: 15px;
      top: 50%;
      margin-top: -8px;
      color: grey;
    }
    .panel-heading a.collapsed:after {
      content: '\e080';
    }

    .no-transition {
      -webkit-transition: height 0.001s;
      -moz-transition: height 0.001s;
      -ms-transition: height 0.001s;
      -o-transition: height 0.001s;
      transition: height 0.001s;
    }
    .form-group.manual {
      position: relative;
    }
    .fieldset {
      position: relative;
    }
    .form-group.last, .panel.last {
      margin-bottom: 0;
    }

    .col-forms {
      padding-left: 5px;
      padding-right: 5px;
    }
    .row-forms{
      margin-right: -5px;
      margin-left: -5px;
    }

    div.form-control {
      border-bottom-left-radius: 4px !important;
      border-top-left-radius: 4px !important;
    }

    .row-forms .panel-heading h3, .row-forms .panel-heading h4 {
      font-size: 14px;
    }

    #form-head .input-group-addon.left,
    #form-body .input-group-addon.left {
      background: none;
      font-weight: bold;
      padding-left: 0;
      border: 0;
    }

    /* Header section */
    #test-option span span {
      min-width: 200px;
      width: 200px;
      text-align: center;
      border-radius: 5px;
    }
    #inspect-items {
      padding-top: 20px;
    }

    /* Site and Instrument Details section */
    #test-details label {
      white-space: nowrap;
      height: 28px;
    }
    #test-details .col-sm-4 {
      width: 39%;
    }
    #test-details .col-sm-8 {
      width: 61%;
    }
    #test-details .col-sm-4-1 {
      width: 31%;
    }
    #test-details .col-sm-8-1 {
      width: 69%;
    }
    #test-details .first {
      width: 20%;
    }
    #test-details .second {
      width: 30%;
    }

    /* Details of the Reference Standards of Measurement (clause 2) section */
    #reference-standards [contenteditable="true"] {
      width: 100%;
    }
    #reference-standards .col-sm-8 {
      width: 70%;
    }
    #reference-standards .col-sm-4 {
      width: 30%;
    }

    /* General characteristics section */
    #general-characteristics-questions .row {
      padding-bottom: 3px;
      margin-left: 2px;
      margin-right: 2px;
    }
    #general-characteristics-questions label {
      margin-left: 0;
      padding-left: 0;
    }
    #general-characteristics-questions .col-sm-6 {
      width: 48%;
      padding-bottom: 3px;
    }
    #general-characteristics-questions  .left-most-col {
      margin-right: 2%;
    }
    #general-characteristics .col-sm-4-custom {
      width: 55%;
      float: left;
    }
    #general-characteristics .col-sm-2-custom {
      width: 45%;
      text-align: right;
      float: left;
    }

    /* Test reports section */
    .figure-label {
      padding-right: 0;
    }
    .unit-label {
      padding-left: 0;
    }
    #host-test-result label {

      float: left;
    }
    #test-report-type label {
      width: 550px;
      height: 83px;
      margin-bottom: 10px;
    }
    #test-report-type label:hover,
    #test-report-type label:hover span.highlight {
      color: #fff;
      background-color: #098109;
    }
    #test-report-type span.highlight,
    #test-report-type label:hover span.highlight {
      color: #f47d18;
      font-weight: bold;
    }
    #col-sm-header-3 label {
      white-space: nowrap;
    }

    #fuel .scroll-x { overflow-x: auto; -webkit-overflow-scrolling: touch; }
    .tiles {
      overflow-y: hidden;
    }
    #test-reports-menu {
      position: absolute;
      float: left;
      z-index: 100;
    }
    #test-reports-inputs {
      position: relative;
      float: left;
      margin-left: 238px;
    }
    #table-test-reports-menu {
      min-width: 220px;
      width: 240px !important;
      background-color: #eee;
      font-size: 11px;
      display: none;
    }
    #table-test-reports-menu label {
      font-size: 11px !important;
      padding-left: 0;
      padding-right: 0;
    }

    .unit {
      float: left;
      font-weight: bold;
      padding-top: 7px;
      padding-left: 5px;
    }
    #col-sm-header-1 [contenteditable="true"],
    #col-sm-header-2 [contenteditable="true"],
    #col-sm-header-2 .imask,
    #col-sm-header-3 [contenteditable="true"],
    #col-sm-header-3 .imask {
      width: 160px !important;
      margin-left: 10px;
    }
    #master-meter-serial-number {
      width: 160px;
      margin-left: 10px;
      float: left;
    }
    #table-scroll {
      width: 100%;
      overflow-x: scroll;
      border: 1px solid red;
    }
    .empty-grid-column,
    .header-column {
      background-color: #eee;
    }
    #table-test-reports-menu {
      border-top-left-radius: 4px;
      border-bottom-left-radius: 4px;
    }
    #table-test-report-with-switch,
    #table-test-report-without-switch {
      width: 1000px;
      border-bottom-right-radius: 4px;
      border-top-right-radius: 4px;
      display: none;
    }
    #table-test-reports-menu [contenteditable="true"],
    #table-test-report-with-switch [contenteditable="true"],
    #table-test-report-without-switch [contenteditable="true"],
    #table-test-reports-menu .imask,
    #table-test-report-with-switch .imask,
    #table-test-report-without-switch .imask  {
      width: 75px !important;
    }
    #table-test-report-with-switch,
    #table-test-report-with-switch td,
    #table-test-report-without-switch ,
    #table-test-report-without-switch td,
    #table-test-reports-menu,
    #table-test-reports-menu td {
      border: 1px solid #ccc;
    }
    #table-test-reports-menu td {
      padding: 5px;
    }
    #table-test-report-with-switch td,
    #table-test-report-without-switch td {
      padding: 5px;
    }
    #table-test-reports-menu label,
    #table-test-report-with-switch label,
    #table-test-report-without-switch label {
      padding-bottom: 5px;
      height: 25px;
    }

    #table-test-report-without-switch .column-4-5-6 {
      width: 40%;
    }
    #table-test-report-header {
      display: none;
      width: 100%;
      margin-bottom: 15px;
    }
    #table-test-report-header .allow-padding-top {
      padding-top: 10px;
    }
    #table-test-report-header .column-1 {
      background-color: #ccc;
    }
    #table-test-report-header .column-inner-4,
    #table-test-report-header .column-inner-5,
    #table-test-report-header .column-inner-6,
    #table-test-report-with-switch .column-inner-4,
    #table-test-report-with-switch .column-inner-5,
    #table-test-report-with-switch .column-inner-6,
    #table-test-report-without-switch .column-inner-4,
    #table-test-report-without-switch .column-inner-5,
    #table-test-report-without-switch .column-inner-6 {
      width: 14%;
    }
    #table-test-report-header .column-inner-7,
    #table-test-report-header .column-inner-8,
    #table-test-report-header .column-inner-9,
    #table-test-report-header .column-inner-10 {

    }
    #table-test-report-with-switch .column-inner-7,
    #table-test-report-with-switch .column-inner-8,
    #table-test-report-with-switch .column-inner-9,
    #table-test-report-with-switch .column-inner-10 {
      width: 14%;
    }
    #table-test-report-without-switch .column-inner-inner-7,
    #table-test-report-without-switch .column-inner-inner-8,
    #table-test-report-without-switch .column-inner-inner-9 {
      width: 14%;
    }
    #table-test-report-without-switch .column-inner-10 {
      width: 13%;
    }
    #table-test-report-header .column-2-3 [contenteditable="true"],
    #table-test-report-header .column-7-8 [contenteditable="true"],
    #table-test-report-header .column-inner-2-3 [contenteditable="true"],
    #table-test-report-header .column-2-3 .imask,
    #table-test-report-header .column-7-8 .imask,
    #table-test-report-header .column-inner-2-3 .imask {
      clear: left;
      margin-left: 10px;
    }
    .row-2-lines {
      height: 80px;
    }
    .row-4-lines {
      height: 160px;
    }
    .column-inner-7 {
      width: 10%;
    }
    .column-inner-1,
    .column-inner-2-3 {
      vertical-align: top;
    }
    .column-inner-1 [contenteditable="true"],
    .column-inner-4 [contenteditable="true"],
    .column-inner-5 [contenteditable="true"],
    .column-inner-6 [contenteditable="true"],
    .column-inner-1 .imask,
    .column-inner-4 .imask,
    .column-inner-5 .imask,
    .column-inner-6 .imask {
      margin-left: 10px;
    }
    .column-2-3 [contenteditable="true"],
    .column-inner-2-3 [contenteditable="true"],
    .column-7-8 [contenteditable="true"],
    .column-2-3 .imask,
    .column-inner-2-3 .imask,
    .column-7-8 .imask {
      margin-left: 10px;
    }
    #table-test-report-header [contenteditable="true"],
    #table-test-report-header .imask {
      width: 80px;
    }
    #table-test-report-header td {
      vertical-align: top;
      border: 1px solid #ccc;
    }
    .totaliser-column .column {
      width: 33%;
      float: left;
      padding: 15px;
    }
    .sum label {
      float: right !important;
    }
    .thick-bar {
      border-top: 20px solid #999;
    }
    .blank-column {
      height: 0;
      padding: 0 !important;
    }
/*
    #master-meter-serial-number {
      width: 160px;
      margin-left: 10px;
      clear: both;
    }
*/
    /* Verifer signature section */
    #form-body-bottom label {
      float: left;
    }
    #div-verifier-number {
      width: 400px;
    }
    #div-row-verifier-number {
      width: 0;
      clear: both;
    }
    #div-verifier-date {
      width: 250px;
    }
    #verifier_number {
      width: 260px;
      float: left;
    }
    .signature-pad {
      margin-top: 20px;
      margin-bottom: 20px;
      width: 620px;
    }
    #verifer-sign {
      width: 500px;
      height: 160px;
      float: left;
    }
    #btn-clear-signature {
      float: right;
      margin-right: 15px;
    }

    .form-control[readonly] {
      background-color: #fff !important;
    }

    .date-picker {
      border-bottom-left-radius: 4px !important;
      border-top-left-radius: 4px !important;
      border-bottom-right-radius: 4px !important;
      border-top-right-radius: 4px !important;
      width: 150px !important;
    }

    /* Remove spin buttons on number input boxes */
    input::-webkit-outer-spin-button,
    input::-webkit-inner-spin-button {
      -webkit-appearance: none;
      margin: 0;
    }

    /* Custom contentEditable */
    [contenteditable="true"],
    .imask {
      float: left;
    }
    input,
    contenteditable="true"] {
      border-top-left-radius: 4px;
      border-bottom-left-radius: 4px;
      border-bottom-right-radius: 4px;
      border-top-right-radius: 4px;
    }
    div[data-ph]:not(:focus):not([data-div-placeholder-content]):before {
      content: attr(data-ph);
      display: block; /* For Firefox */
      color: #ccc;
    }
    [contenteditable="true"].single-line {
      white-space: nowrap;
      overflow: hidden;
      word-break: break-all;
    }
    [contenteditable="true"].single-line br {
      display: none;
    }
    [contenteditable="true"].single-line * {
      display: inline;
      white-space: nowrap;
    }

    .opt-control input[type="radio"] {
      display:none;
    }
    .opt-control .clsyes {
      display:inline-block;
      background-color:#999;
      padding:4px 11px;
      font-family:Arial;
      font-size:16px;
      margin: 5px 0px 0px 10px;
      width:50px;
      color:#fff;
    }
    .opt-control .clsno {
      display:inline-block;
      background-color:#999;
      padding:4px 11px;
      font-family:Arial;
      font-size:16px;
      margin: 5px 0px 0px 10px;
      width:50px;
      color:#fff;
    }
    .opt-control .clsna {
      display:inline-block;
      background-color:#999;
      padding:4px 11px;
      font-family:Arial;
      font-size:16px;
      margin: 5px 0px 0px 10px;
      width:50px;
      color:#fff;
    }
    .QA_test-type-verification + .clsyes{
      width: 110px;
    }
    .QA_test-type-inspection + .clsyes {
      width: 175px;
    }
    .opt-control input[type="radio"]:checked + .clsyes {
      background-color:#098109;
      color:#fff;
    }
    .opt-control input[type="radio"]:checked + .clsno {
      background-color:#f47d18;
      color:#fff;
    }
    .opt-control input[type="radio"]:checked + .clsna {
      background-color:#1010c6;
      color:#fff;
    }
    .QA_test-type-verification + .clsyes{
      width: 110px;
    }
    .QA_test-type-inspection + .clsyes {
      width: 175px;
    }
    #test-report-type input[type="radio"]:checked + .clsno {
      background-color: #098109 !important;
      color:#fff;
    }




    .opt-control2 input[type="radio"] {
      display:none;
    }
    .opt-control2 span {
      background-color:#999;
      display:inline-block;
      padding:4px 11px;
      font-family:Arial;
      font-size:14px;
      margin: 3px 0px 0px 3px;
      width:52px;
      color:#fff;
    }
    .opt-control2 .clsyesOn {
        background-color:#098109 !important;
        color:#fff;
    }
    .opt-control2 .clsnoOn {
        background-color:#f47d18 !important;
        color:#fff;
    }
    .opt-control2 .clsnaOn {
        background-color:#1010c6 !important;
        color:#fff;
    }
    .opt-control2 .clsyes,
    .opt-control2 .clsno,
    .opt-control2 .clsna {
        background-color:#999;
        color:#fff;
    }

    /* Loading animation */
    .loading {
      width: 250px;
      height: 35px;
      padding: 15px 10px;
      display: inline;
      margin-left: 50px;
      color: red;
      font-weight: bold;
      visibility: hidden;
    }
    .loading img {
      width: 25px;
      height: 25px;
    }

    /* Fields not to be used */
    #div-verifier-number {
      display: none;
    }

    .ipad-table-row-padding-fix {
      padding-bottom: 6px;
    }

    /* Validation */
    .warning {
      background-color: #FCC7C7 !important;
    }
    [contenteditable="true"].warning,
    input.warning,
    div.warning {
      border: 1px solid red;
    }
    .opt-control .warning label.btn {
      background-color: #FCC7C7 !important;
    }
    #test-report-type .warning span.highlight,
    #test-report-type .warning label:hover span.highlight {
      background-color: #FCC7C7 !important;
    }
    #test-report-type .warning {
      border: 0;
      background-color: #fff !important;
    }
    #test-report-type div.warning label {
      border: 1px solid red;
      background-color: #FCC7C7 !important;
    }
    #submit-message-wrapper {
      margin-top: 5px;
    }
    #submit-message {
      color: red;
      font-weight: bold;
      border: 1px solid red;
      padding: 5px 5px 0 5px;
      height: 35px;
      margin: 15px auto;
    }

    /* Virtual nunerical keyboard */
    #virtual-keyboard {
      width: 195px;
      height: 450px;
      background-color: #d9edf7;
      position: absolute;
      z-index: 9999;
      display: none;
      border: 1px solid #31708f;
      border-radius: 4px;
    }
    #virtual-keyboard ul {
      width: 195px;
      height: 430px;
      padding-top: 50px;
      padding-left: 5px;
      list-style: outside none none;
    }
    .new-row {
      clear: both;
    }
    .btn-smll-font-size {
      font-size: 9pt !important;
    }
    .btn-symbol {
      font-size: 18pt;
    }
    #virtual-keyboard li {
      background-color: #ccc;
      border: 2px solid white;
      border-radius: 6px;
      color: #333;
      font-size: 13pt;
      font-weight: bold;
      height: 55px;
      line-height: 55px;
      margin: 3px;
      text-align: center;
      vertical-align: middle;
      width: 55px;
      float: left;
    }
    #virtual-keyboard li.long-pad {
      width: 178px;
    }
    #pad-head {
      background-color: #004a84;
      color: white;
      font-size: 7pt;
      height: 12px;
      left: 0;
      letter-spacing: 3px;
      padding-left: 5px;
      position: relative;
      right: 0;
      top: 0;
    }
    .hide {
      display: none;
    }
    #btn-keyboard-swap {
      background-color: #004a84;
      border: 1px solid #004a84;
      border-radius: 4px;
      color: white;
      font-size: 12px;
      font-weight: bold;
      text-align: center;
      display: inline-block;
      cursor: pointer;
      text-decoration: none;
      box-shadow: 3px 3px #999;
      position: absolute;
      left: 100px;
      z-index: 9999;
      display: none;
      padding: 20px;
    }
    #slider {
      margin-top: 5px;
      margin-left: 10px;
      width: 90%;
    }

    @media (max-width: 1199px) {
      .container {
        width: 970px;
      }
      #col-sm-header-1 {
        width: 33%;
      }
      #col-sm-header-2 {
        width: 33%;
      }
      .signature-pad {
        width: 620px;
      }
      #verifer-sign {
        width: 500px;
      }
      #btn-clear-signature {
        margin-right: 0;
      }
    }

    @media (max-width: 992px) {
      .container {
        width: 720px;
      }
      #form-head .col-sm-4 [contenteditable="true"] {
        width: 70px;
      }
      #gvr-job-number {
        width: 60px;
        overflow: hidden;
      }
      #form-head .col-sm-6 [contenteditable="true"] {
        width: 200px;
      }
      #test-details .col-sm-4 {
        width: 56%;
      }
      #test-details .col-sm-8 {
        width: 44%;
      }
      #test-details .col-sm-4-1 {
        width: 43%;
      }
      #test-details .col-sm-8-1 {
        width: 57%;
      }
      #test-details .first {
        clear: left;
        width: 30%;
      }
      #test-details .second {
        width: 70%;
      }
      #test-details .second [contenteditable="true"] {
        width: 100%;
      }
      #test-details .col-sm-8 [contenteditable="true"] {
        width: 100%;
      }
      #reference-standards label {
        white-space: nowrap;
      }
      #reference-standards .col-sm-8 {
        width: 62%;
      }
      #reference-standards .col-sm-4 {
        width: 38%;
      }
      #general-characteristics .col-sm-4-custom {
        width: 78%;
      }
      #general-characteristics .col-sm-2-custom {
        width: 22%;
      }
      #host-test-result label {
        float: left;
      }
      #col-sm-header-1,
      #col-sm-header-2,
      #col-sm-header-3 {
        width: 100%;
      }
      #col-sm-header-2,
      #col-sm-header-3 {
        margin-top: 10px;
      }
      #col-sm-header-1 label,
      #col-sm-header-2 label,
      #col-sm-header-3 label {
        width: 51%;
      }
      #col-sm-header-1 .unit,
      #col-sm-header-2 .unit,
      #col-sm-header-3 .unit {
        width: 90px;
      }

      .totaliser-column .column [contenteditable="true"],
      .totaliser-column .column .imask,
      #table-test-report-header .column-9-10 [contenteditable="true"],
      #table-test-report-header .column-9-10 .imask,
      #table-test-report-header .allow-padding-top [contenteditable="true"],
      #table-test-report-header .allow-padding-top .imask,
      #table-test-report-header .column-2-3 .unit {
        margin-left: 10px;
      }
      #table-test-report-header .allow-padding-top {
        padding-top: 0;
      }
    }
  </style>
</head>
<body id="main">
  <div id="menu-bar"></div>
  <form id="frm">
    <!--tion id="header">
      <div class="container">
        <div class="row">
          <div class="headcol hidden-xs col-sm-4 col-lg-3">
            <img src="gvr-logo.png" />
          </div>
          <div class="headcol hidden-xs col-sm-4 col-sm-push-4 col-lg-3 col-lg-push-6">
            <img src="verified-logo.png" />
          </div>
          <div class="headcol visible-xs-block col-sm-12">
            <img src="mobile-logos.png" />
          </div>
        </div>
      </div>
    </section>//-->
    <section id="form-head" class="navbar navbar-default navbar-static">
      <div class="container">
        <button id="btn-keyboard-swap">Keyboard Swap</button>
        <div class="panel panel-primary" id="panel-form-head">
          <div class="panel-heading">
            <h3 class="panel-title">Calibration Check/Certification Report for LPG Dispensers</h3>
          </div>
          <div class="panel-body">
              <div class="row">
                <div class="form-group col-sm-3">
                  <div class="input-group">
                    <label for="test-date" class="input-group-addon left">Date of test</label>
                    <div class='input-group date'>
                      <input type='text' class="QA QA_001 form-control date-picker" id="test-date" name="test-date" readonly tabindex="1" value="<?php print $array_raw['QA_001'];?>"/>
                    </div>
                  </div>
                </div>
                <div class="col-sm-5">
                  &nbsp;
                </div>
                <div class="form-group col-sm-4">
                  <div class="input-group required">
                    <label for="gvr-job-number" class="input-group-addon left">GVR Job number</label>
                    <div class="QA QA_279 form-control single-line" id="gvr-job-number" contenteditable="false" tabindex="2" readonly disabled><?php print $array_raw['QA_279'];?></div>
                  </div>
                </div>
                <div class="form-group col-sm-4">
                  <div class="input-group">
                    <label for="gvr-site-number" class="input-group-addon left">GVR Site number</label>
                    <div class="QA QA_003 form-control single-line" id="gvr-site-number" contenteditable="true" tabindex="3"><?php print $array_raw['QA_003'];?></div>
                  </div>
                </div>
                <div class="form-group col-sm-4">
                  <div class="input-group">
                    <label for="employee-name" class="input-group-addon left">Employee name</label>
                    <div class="QA QA_004 form-control single-line" id="employee-name" contenteditable="true" tabindex="4"><?php print $array_raw['QA_004'];?></div>
                  </div>
                </div>
                <div class="form-group col-sm-4">
                  <div class="input-group">
                    <label for="verifier-number" class="input-group-addon left">Verifier number</label>
                    <div class="QA QA_005 form-control single-line" id="verifier-number" contenteditable="true" tabindex="5"><?php print $array_raw['QA_005'];?></div>
                  </div>
                </div>
                <div id="test-option" class="col-sm-12 opt-control">
                  <label>Type of test&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>
                  <span class="opt-control2">
                    <span class="QA QA_006 btn QA_test-type-verification-yes spsize clsyes" id="yes-test-type-verification" tabindex="6">Verification</span>
                    <span class="QA QA_007 btn QA_test-type-inspection-yes spsize clsyes" id="yes-test-type-inspection" tabindex="7">In-service Inspection</span>
                  </span>
                </div>
                <div id="inspection" style="display: none;" class="form-group col-sm-12">
                  <div id="inspect-items" class="input-group"><label for="in-service-mark" class="input-group-addon left">Please record the verification mark</label>
                  <div class="QA QA_008 single-line form-control" id="in-service-mark" contenteditable="true" tabindex="8"><?php print $array_raw['QA_008'];?></div></div>
                </div>
              </div>
          </div>
        </div>
      </div>
    </section>
    <section id="form-body" class="container">
      <div class="panel panel-info" id="panel-form-body-1">
        <div class="panel-heading">
          <h3 class="panel-title"><a data-toggle="collapse" data-target="#test-details" tabindex="9">Site and Instrument Details</a></h3>
        </div>
        <div id="test-details" class="no-transition panel-body">
          <div class="row">
            <div class="form-group col-sm-3 first">
              <label for="owner-user-name">Name of owner/user</label>
            </div>
            <div class="form-group col-sm-3 second">
              <div class="QA QA_009 form-control single-line" id="owner-user-name" contenteditable="true" tabindex="9"><?php print $array_raw['QA_009'];?></div>
            </div>
            <div class="form-group col-sm-3 first">
              <label for="owner-user-address">Address of owner/user</label>
            </div>
            <div class="form-group col-sm-3 second">
              <div class="QA QA_010 form-control single-line" id="owner-user-address" contenteditable="true" tabindex="10"><?php print $array_raw['QA_010'];?></div>
            </div>
            <div class="form-group col-sm-4 col-sm-4-1">
              <label for="contact-name">Name of contact person on premises</label>
            </div>
            <div class="form-group col-sm-8 col-sm-8-1">
              <div class="QA QA_011 form-control single-line" id="contact-name" contenteditable="true" tabindex="11"><?php print $array_raw['QA_011'];?></div>
            </div>
            <div class="form-group col-sm-4 col-sm-4-1">
              <label for="trading-name">Trading name</label>
            </div>
            <div class="form-group col-sm-8 col-sm-8-1">
              <div class="QA QA_012 form-control single-line" id="trading-name" contenteditable="true" tabindex="12"><?php print $array_raw['QA_012'];?></div>
            </div>
            <div class="form-group col-sm-4 col-sm-4-1">
              <label for="instrument-location-address">Address of instrument location</label>
            </div>
            <div class="form-group col-sm-8 col-sm-8-1">
              <div class="QA QA_013 form-control single-line" id="instrument-location-address" contenteditable="true" tabindex="13"><?php print $array_raw['QA_013'];?></div>
            </div>
            <div class="form-group col-sm-4 col-sm-4-1">
              <label for="instrument-description">Description of instrument</label>
            </div>
            <div class="form-group col-sm-8 col-sm-8-1">
              <div class="QA QA_014 form-control single-line" id="instrument-description" contenteditable="true" tabindex="14"><?php print $array_raw['QA_014'];?></div>
            </div>
            <div class="form-group col-sm-3 first">
              <label for="instrument-manufacturer">Manufacturer</label>
            </div>
            <div class="form-group col-sm-3 second">
              <div class="QA QA_015 form-control single-line" id="instrument-manufacturer" contenteditable="true" tabindex="15"><?php print $array_raw['QA_015'];?></div>
            </div>
            <div class="form-group col-sm-3 first">
              <label for="instrument-model">Model</label>
            </div>
            <div class="form-group col-sm-3 second">
              <div class="QA QA_016 form-control single-line" id="instrument-model" contenteditable="true" tabindex="16"><?php print $array_raw['QA_016'];?></div>
            </div>
            <div class="form-group col-sm-3 first">
              <label for="dispenser-numbers">Dispenser number(s)</label>
            </div>
            <div class="form-group col-sm-3 second">
              <div class="QA QA_017 form-control single-line" id="dispenser-numbers" contenteditable="true" tabindex="17"><?php print $array_raw['QA_017'];?></div>
            </div>
            <div class="form-group col-sm-3 first">
              <label for="dispenser-serial-number">Dispenser serial number</label>
            </div>
            <div class="form-group col-sm-3 second">
              <div class="QA QA_018 form-control single-line" id="dispenser-serial-number" contenteditable="true" tabindex="18"><?php print $array_raw['QA_018'];?></div>
            </div>
            <div class="form-group col-sm-4 col-sm-4-1">
              <label for="certificate-numbers">Certificate(s) of Approval number</label>
            </div>
            <div class="form-group col-sm-8 col-sm-8-1">
              <div class="QA QA_019 form-control single-line" id="certificate-numbers" contenteditable="true" tabindex="19"><?php print $array_raw['QA_019'];?></div>
            </div>
            <div class="form-group col-sm-4">
              <label for="fuel-products">LPG density range dispenser approved to deliver</label>
            </div>
            <div class="form-group col-sm-8">
              <div class="QA QA_020 form-control single-line" id="fuel-products" contenteditable="true" tabindex="20"><?php print $array_raw['QA_020'];?></div>
            </div>
            <div class="form-group col-sm-3 first">
              <label for="minimum-flowrate">Minimum flowrate</label>
            </div>
            <div class="form-group col-sm-3 second">
              <div class="QA QA_021 form-control input-sm imask single-line" id="minimum-flowrate" contenteditable="true" tabindex="21" data-ph="00"><?php print $array_raw['QA_021'];?></div>
            </div>
            <div class="form-group col-sm-3 first">
              <label for="maximum-flowrate">Maximum flowrate</label>
            </div>
            <div class="form-group col-sm-3 second">
              <div class="QA QA_022 form-control input-sm imask single-line" id="maximum-flowrate" contenteditable="true" tabindex="22" data-ph="00"><?php print $array_raw['QA_022'];?></div>
            </div>
          </div>
        </div>
      </div>

      <div class="panel panel-info" id="panel-form-body-2">
        <div class="panel-heading">
          <h3 class="panel-title"><a data-toggle="collapse" data-target="#reference-standards" tabindex="23">Details of the Reference Standards of Measurement (clause 2)</a></h3>
        </div>
        <div id="reference-standards" class="no-transition panel-body">
          <div class="row">
            <div class="col-sm-4">
              <label for="reference-standards-make">Make</label>
            </div>
            <div class="form-group col-sm-8">
              <div class="QA QA_023 form-control single-line" id="reference-standards-make" contenteditable="true" tabindex="23"><?php print $array_raw['QA_023'];?></div>
            </div>
            <div class="col-sm-4">
              <label for="reference-standards-model">Model</label>
            </div>
            <div class="form-group col-sm-8">
              <div class="QA QA_024 form-control single-line" id="reference-standards-model" contenteditable="true" tabindex="24"><?php print $array_raw['QA_024'];?></div>
            </div>
            <div class="col-sm-4">
              <label for="reference-standards-serial">Serial number</label>
            </div>
            <div class="form-group col-sm-8">
              <div class="QA QA_025 form-control single-line" id="reference-standards-serial" contenteditable="true" tabindex="25"><?php print $array_raw['QA_025'];?></div>
            </div>
            <div class="col-sm-4">
              <label for="reference-standards-volume">Flowrate range/weight</label>
            </div>
            <div class="form-group col-sm-8">
              <div class="QA QA_026 form-control single-line" id="reference-standards-volume" contenteditable="true" tabindex="26"><?php print $array_raw['QA_026'];?></div>
            </div>
            <div class="col-sm-4">
              <label for="reference-standards-regulation-13">Regulation 13/37 certificate no.</label>
            </div>
            <div class="form-group col-sm-8">
              <div class="QA QA_027 form-control single-line" id="reference-standards-regulation-13" contenteditable="true" tabindex="27"><?php print $array_raw['QA_027'];?></div>
            </div>
            <div class="col-sm-4">
              <label for="reference-standards-expiry">Certificate expiry date</label>
            </div>
            <div class="col-sm-8">
              <div class='input-group date required'>
                <input type='text' class="QA QA_028 form-control date-picker" id="reference-standards-expiry" name="reference-standards-expiry" readonly  tabindex="28" value="<?php print $array_raw['QA_028'];?>"/>
              </div>
            </div>
          </div>
        </div>
      </div>

      <div class="panel panel-info" id="panel-form-body-3">
        <div class="panel-heading">
          <h3 class="panel-title"><a data-toggle="collapse" data-target="#general-characteristics" class="collapsed" tabindex="29">General Characteristics (clause 3.3)</a></h3>
        </div>
        <div id="general-characteristics" class="no-transition panel-body collapse">
          <div id="general-characteristics-questions">

          </div>
        </div>
      </div>

      <div class="panel panel-info" id="panel-form-body-4">
        <div class="panel-heading">
          <h3 class="panel-title"><a data-toggle="collapse" data-target="#host-test-result" tabindex="52">Host Test Results</a></h3>
        </div>
        <div id="host-test-result" class="no-transition panel-body">
          <div class="row">

            <div id="test-report-type" class="opt-control">
              <div class="col-sm-12 required">
                <input type="radio" class="QA QA_052 QQ QA_test-report-for-lpg-dispensers-yes" name="test-report-for-lpg-dispensers" value="with-switch">
                <label class="QQ QL_test-report-for-lpg-dispensers-yes btn clsyes" tabindex="52">
                Test Report 1-1 for LPG Dispensers <br/><span class="highlight">with</span> <br/>V<sub>FD</sub>/V<sub>FD15</sub> Switch which are Tested Volumetrically using Master Meter
                </label>

                <input type="radio" class="QA QA_052 QQ QA_test-report-for-lpg-dispensers-no" name="test-report-for-lpg-dispensers" value="without-switch">
                <label class="QQ QL_test-report-for-lpg-dispensers-no btn clsno" tabindex="52">
                Test Report 1-2 for LPG Dispensers <br/><span class="highlight">without</span> <br/>V<sub>FD</sub>/V<sub>FD15</sub> Switch which are Tested Volumetrically using a Master Meter
                </label>
              </div>
            </div>

            <div class="col-sm-12">
              <hr class="divider" />
            </div>

            <div id="col-sm-header-1" class="col-sm-4">
              <label for="master-meter-serial-number">Master meter serial number</label>
              <div class="QA QA_053 form-control input-sm single-line" id="master-meter-serial-number" contenteditable="true" tabindex="53"><?php print $array_raw['QA_053'];?></div>
            </div>
            <div id="col-sm-header-2" class="col-sm-4">
              <label for="density-displayed-by-dispenser">Density displayed by dispenser</label>
              <div class="QA QA_054 form-control input-sm single-line imask" id="density-displayed-by-dispenser" contenteditable="true" data-ph="000" pattern="[0-9]*" tabindex="54"><?php print $array_raw['QA_054'];?></div>
              <span class="unit">kg/L</span>
            </div>
            <div id="col-sm-header-3" class="col-sm-4 tfd-tfdi">
              <label for="temperature-displayed-by-dispenser">Temperature displayed by dispenser (T<sub>FDI</sub>)</label>
              <div class="QA QA_055 form-control input-sm single-line imask" id="temperature-displayed-by-dispenser" contenteditable="true" data-ph="0.0" pattern="[0-9]*" tabindex="55"><?php print $array_raw['QA_055'];?></div>
              <span class="unit"><sup>o</sup>C</span>
            </div>

            <div class="col-sm-12">
              <hr class="divider" />
            </div>



            <div id="fuel-wrapper" class="col-sm-12 col-forms" id="panel-form-body-5">
              <div id="fuel-header">
                <table id="table-test-report-header">
                <tr>
                  <td colspan="9" class="column-1">
                    <label>Hydrometer readings</label>
                  </td>
                </tr>
                <tr>
                  <td class="row-2-lines column-2-3">
                    <label for="observed-pe">Observed P<sub>e</sub> = </label>
                    <div class="QA QA_056 form-control input-sm single-line imask" id="observed-pe" contenteditable="true" data-ph="0000" tabindex="56"><?php print $array_raw['QA_056'];?></div>
                    <span class="unit">kPa</span>
                  </td>
                  <td colspan="3" class="row-2-lines column-4-5-6 allow-padding-top">
                    <label for="observed-temperature">Observed temp = </label>
                    <div class="QA QA_061 form-control input-sm single-line imask" id="observed-temperature" contenteditable="true" data-ph="0.0" tabindex="57"><?php print $array_raw['QA_061'];?></div>
                    <span class="unit"><sup>o</sup>C</span>
                  </td>
                  <td colspan="3" class="column-7-8 row-2-lines">
                    <label for="observed-density">Observed density = </label>
                    <div class="QA QA_062  form-control input-sm single-line imask" id="observed-density" contenteditable="true" data-ph="0.0000" tabindex="58"><?php print $array_raw['QA_062'];?></div>
                    <span class="unit">kg/L</span>
                  </td>
                  <td colspan="2" rowspan="2" class="column-9-10">
                    <label for="density-at-15-celcius">Density at 15 <sup>o</sup>C</label><br/><br/>
                    <label for="density-at-15-celcius">D<sub>15</sub> = </label>
                    <div class="QA QA_063  form-control input-sm single-line imask" id="density-at-15-celcius" contenteditable="true" data-ph="0.0000" tabindex="59"><?php print $array_raw['QA_063'];?></div>
                    <span class="unit">kg/L</span>
                  </td>
                </tr>
                <tr>
                  <td class="row-2-lines column-2-3">
                    <label for="corrected-pe">Corrected P<sub>e</sub> = </label>
                    <div class="QA QA_057 form-control input-sm single-line imask" id="corrected-pe" contenteditable="true" data-ph="0000" tabindex="60"><?php print $array_raw['QA_057'];?></div>
                    <span class="unit">kPa</span>
                  </td>
                  <td colspan="3" class="row-2-lines column-4-5-6 allow-padding-top">
                    <label for="corrected-temperature">Corrected temp = </label>
                    <div class="QA QA_064 form-control input-sm single-line imask" id="corrected-temperature" contenteditable="true" data-ph="0.0" tabindex="61"><?php print $array_raw['QA_064'];?></div>
                    <span class="unit"><sup>o</sup>C</span>
                  </td>
                  <td colspan="3" class="column-7-8 row-2-lines">
                    <label for="corrected-density">Corrected density = </label>
                    <div class="QA QA_065 form-control input-sm single-line imask" id="corrected-density" contenteditable="true" data-ph="0.0000" tabindex="62"><?php print $array_raw['QA_065'];?></div>
                    <span class="unit">kg/L</span>
                  </td>
                </tr>
                <tr>
                  <td colspan="9" class="totaliser-column">
                    <div class="column">
                      <label for="totaliser-end">Totaliser at the end</label>
                      <div class="QA QA_058 form-control input-sm single-line imask" id="totaliser-end" contenteditable="true" data-ph="0000000000" tabindex="63"><?php print $array_raw['QA_058'];?></div>
                      <span class="unit">L</span>
                    </div>
                    <div class="column">
                      <label for="totaliser-start">Totaliser at the start</label>
                      <div class="QA QA_060 form-control input-sm single-line imask" id="totaliser-start" contenteditable="true" data-ph="0000000000" tabindex="64"><?php print $array_raw['QA_060'];?></div>
                      <span class="unit">L</span>
                    </div>
                    <div class="column">
                      <label for="total-vol-used">Total volume used</label>
                      <label class="QA QA_059 no-decimal" id="total-vol-used"><?php print $array_raw['QA_059'];?></label>
                      <span class="unit">L</span>
                    </div>
                  </td>
                </tr>
                </table>
              </div>
              <div id="fuel" class="scroll-x tiles" id="panel-form-body-6">
                <div id="test-reports-menu">
                  <table id="table-test-reports-menu">
                  <tr class="thick-bar">
                    <td class="row-4-lines">
                      &nbsp;
                    </td>
                  </tr>
                  <tr>
                    <td>
                      <label for="tmm-delivery-qmax-1">T<sub>MM</sub></label>
                    </td>
                  </tr>
                  <tr>
                    <td>
                      <label for="pmm-delivery-qmax-1">P<sub>MM</sub></label>
                    </td>
                  </tr>
                  <tr>
                    <td>
                      <label for="">D<sub>P</sub></label>
                    </td>
                  </tr>
                  <tr>
                    <td>
                      <label for="">V<sub>MM</sub></label>
                    </td>
                  </tr>
                  <tr>
                    <td>
                      <label for="">Maximum achievable flow rate</label>
                    </td>
                  </tr>
                  <tr>
                    <td>
                      <label for="">MF<sub>MM</sub></label>
                    </td>
                  </tr>
                  <tr>
                    <td>
                      <label for="">C<sub>tiMM</sub>   (using density at 15<sup>o</sup>C, T<sub>MM</sub>)</label>
                    </td>
                  </tr>
                  <tr class="thick-bar">
                    <td class="blank-column">
                    </td>
                  </tr>
                  <tr>
                    <td>
                      <label for="">C<sub>piMM</sub>    (using P<sub>e</sub> density at 15<sup>o</sup>C, T<sub>MM</sub>, P<sub>MM</sub>)</label>
                    </td>
                  </tr>
                  <tr>
                    <td>
                      <label for="">V<sub>REF</sub>    (V<sub>MM</sub> * C<sub>tiMM</sub> * C<sub>piMM</sub> * MF<sub>MM</sub>)</label>
                    </td>
                  </tr>
                  <tr>
                    <td>
                      <label for="">T<sub>FD</sub></label>
                    </td>
                  </tr>
                  <tr>
                    <td>
                      <label for="">T<sub>FD</sub> - T<sub>FDI</sub></label>
                    </td>
                  </tr>
                  <tr>
                    <td>
                      <label for="">V<sub>FD</sub></label>
                    </td>
                  </tr>
                  <tr>
                    <td>
                      <label for="">V<sub>FD15</sub></label>
                    </td>
                  </tr>
                  <tr>
                    <td>
                      <label for="">C<sub>tiFD</sub> = (using density at 15<sup>o</sup>C, T<sub>FD</sub>)</label>
                    </td>
                  </tr>
                  <tr>
                    <td>
                      <label for="">V<sub>FD, C</sub> = (V<sub>FD</sub> * C<sub>tiFD</sub>)</label>
                    </td>
                  </tr>
                  <tr>
                    <td>
                      <label for="">E<sub>FD</sub> = [(V<sub>FD,C</sub> - V<sub>REF</sub>) / V<sub>REF</sub> * 100]</label>
                    </td>
                  </tr>
                  </table>
                </div> <!--//test-reports-menu-->
                <div id="test-reports-inputs">
                  <table id="table-test-report-with-switch" border="1">
                  <tr class="thick-bar">
                    <td class="row-4-lines column-inner-4">
                      <label>Delivery 1</label>
                      <div class="clear-float"></div>
                      <div class="div-center"><label>Q<sub>max</sub></div>
                    </td>
                    <td class="column-inner-5">
                      <label>Delivery 2</label>
                      <div class="clear-float"></div>
                      <div class="div-center"><label>Q<sub>max</sub></div>
                    </td>
                    <td class="column-inner-6">
                      <label>Delivery 3</label>
                      <div class="clear-float"></div>
                      <div class="div-center"><label>Q<sub>max</sub></div>
                    </td>
                    <td class="column-inner-7 header-column">
                      <label>Spare</label>
                    </td>
                    <td class="column-inner-8 header-column">
                      <label>Compensated with V<sub>FD</sub>/V<sub>FD15</sub> switch</label>
                    </td>
                    <td class="column-inner-9 header-column">
                      <label>Delivery 1 Q<sub>min</sub></label>
                    </td>
                    <td class="column-inner-10 header-column">
                      <label>Spare</label>
                    </td>
                  </tr>
                  </table>

                  <table id="table-test-report-without-switch">
                  <tr class="thick-bar">
                    <td rowspan="2" class="row-4-lines column-inner-4">
                      <label>Delivery 1</label>
                      <div class="clear-float"></div>
                      <div class="div-center"><label>Q<sub>max</sub></div>
                    </td>
                    <td rowspan="2" class="row-4-lines column-inner-5">
                      <label>Delivery 2</label>
                      <div class="clear-float"></div>
                      <div class="div-center"><label>Q<sub>max</sub></div>
                    </td>
                    <td rowspan="2" class="row-4-lines column-inner-6">
                      <label>Delivery 3</label>
                      <div class="clear-float"></div>
                      <div class="div-center"><label>Q<sub>max</sub></div>
                    </td>
                    <td colspan="3" class="column-inner-7">
                      <label>Compensated no V<sub>FD</sub>/V<sub>FD15</sub> switch</label>
                    </td>
                    <td rowspan="2" class="row-2-lines column-inner-10">
                      <label>Delivery 1 Q<sub>min</sub></label>
                    </td>
                  </tr>
                  <tr>
                    <td class="column-inner-inner-7">
                      <label for="tbl2-tmm-delivery-qmax-1">Delivery 1 Q<sub>max</sub></label>
                    </td>
                    <td class="column-inner-inner-8">
                      <label for="tbl2-tmm-delivery-qmax-2">Delivery 2 Q<sub>max</sub></label>
                    </td>
                    <td class="column-inner-inner-9">
                      <label for="tbl2-tmm-delivery-qmax-3">Delivery 3 Q<sub>max</sub></label>
                    </td>
                  </tr>
                  </table>
                </div>  <!--//test-reports-inputs-->

              </div>  <!--//fuel-->
            </div> <!--//fuel-wrapper-->
          </div>
        </div>
      </div>
    </section>

    <section id="form-body-bottom" class="container">

      <div class="panel panel-info" id="panel-form-body-bottom">
        <div class="panel-heading">
          Note
        </div>
        <div class="panel-body">
          <p id="p-note">The correction factor MF<sub>MM</sub> is the meter factor obtained from a traceable measurement report.</p>
        </div>
      </div>

      <div class="panel panel-info">
        <div class="panel-heading">
          Verifer Signature
        </div>
        <div class="panel-body">
          <div class="row">
            <div class="col-sm-7 signature-pad">
              <div id="verifer-sign" class="QA QA_276 form-control" tabIndex="268"></div>
              <button type="button" id="btn-clear-signature" class="btn btn-success" tabIndex="269">Clear</button>
            </div>
            <div class="col-sm-5">
              &nbsp;
            </div>
            <div class="col-sm-12"></div>
            <div id="div-verifier-number" class="col-sm-4">
              <label id="verifier-no-label" for="verifier_number">Verifier No</label>
              <div class="QA QA_277 form-control input-sm single-line verifer-textbox" id="verifier_number" contenteditable="true" tabIndex="270"><?php print $array_raw['QA_277'];?></div>
            </div>
            <div id="div-verifier-date" class="col-sm-6">
              <label for="verifier_date">Date</label>
              <div class="input-group date required">
                <input type='text' class="QA QA_278 form-control input-sm date-picker" id="verifer_date" name="verifer_date" readonly tabIndex="271" value="<?php print $array_raw['QA_278'];?>"/>
              </div>
            </div>
            <div id="div-row-verifier-number" class="col-sm-2">
            </div>
          </div>
        </div>
      </div>
    </section>

    <section id="form-footer" class="container">
      <div class="panel panel-danger">
        <div class="panel-heading">
          By hitting submit, I certify that the quantities shown as withdrawn from the tanks through the hoses listed above have been returned to the tanks for the appropriate grade.
        </div>
        <div class="panel-body" align="right">
          <button id="btn-reset" class="btn btn-default " role="button" tabIndex="273">Reset</button> <button id="btn-submit" class="btn btn-success" role="button" tabIndex="274">Submit</button>
        </div>
      </div>
    </section>

    <!--// Virtual numerical keyboard //-->
    <div id="virtual-keyboard">
      <div id="pad-head">Virtual Keyboard</div>
      <ul>
        <li class="btn-symbol">&#9003;</li>
        <li class="btn-symbol">&#8676;</li>
        <li class="btn-symbol">&#8677;</li>
        <li class="new-row">7</li>
        <li>8</li>
        <li>9</li>
        <li class="new-row">4</li>
        <li>5</li>
        <li>6</li>
        <li class="new-row">1</li>
        <li>2</li>
        <li>3</li>
        <li class="new-row">0</li>
        <li>.</li>
        <li class="btn-smll-font-size">Hide</li>
        <li class="new-row long-pad">Keyboard Swap</li>
      </ul>
    </div>

    <input type="hidden" id="activity_number" class="QA QA_002" value="<?php print $array_raw['QA_002'];?>"/>
    <input type="hidden" id="QA_280" class="QA QA_280" value="<?php print $array_raw['QA_280'];?>"/>
    <input type="hidden" id="increment_number" class="QA IncrementNumber" value="<?php print $array_raw['IncrementNumber'];?>"/>
    <input type="hidden" id="verifer-sign-dummy" value=""/>
    <input type="hidden" id="ex_gilbarco_veeder_root_lpg" class="ex_gilbarco_veeder_root_lpg" value="-1"/>
    <input type="hidden" id="ex_loc" value=""/>
  </form>

  <!-- jsRender templates -->
  <script id="script-general-characteristics" type="text/x-jsrender">
    {{for generalCharacteristicRows}}
      {{if showSeparatorLine}}
        <div class="row">
          <div class="col-sm-12">
            <hr class="divider" />
          </div>
        </div>
      {{else questionType1}}
        <div class="row">
          <div class="col-sm-6 left-most-col required">
            <div class="col-sm-4-custom">
              <label>{{:question_1}}</label>
            </div>
            <div class="col-sm-2-custom opt-control2">
              <span class="{{:spanAttributes_1}}-yes QQ btn clsyes" tabindex="{{:tabIndex_1}}">Yes</span>
              <span class="{{:spanAttributes_1}}-no QQ btn clsno" tabindex="{{:tabIndex_1}}">No</span>
              <span class="{{:spanAttributes_1}}-na QQ btn clsna" tabindex="{{:tabIndex_1}}">N&#47;A</span>
            </div>
          </div>
          <div class="col-sm-6 required">
            <div class="col-sm-4-custom">
              <label>{{:question_2}}</label>
            </div>
            <div class="col-sm-2-custom opt-control2">
              <span class="{{:spanAttributes_2}}-yes QQ btn clsyes" tabindex="{{:tabIndex_2}}">Yes</span>
              <span class="{{:spanAttributes_2}}-no QQ btn clsno" tabindex="{{:tabIndex_2}}">No</span>
              <span class="{{:spanAttributes_2}}-na QQ btn clsna" tabindex="{{:tabIndex_2}}">N&#47;A</span>
            </div>
          </div>
        </div>
      {{else questionType2}}
        <div class="row">
          <div class="col-sm-6 left-most-col required">
            <div class="col-sm-4-custom">
              <label>{{:question_1}}</label>
            </div>
            <div class="col-sm-2-custom opt-control2">
              <span class="{{:spanAttributes_1}}-yes QQ btn clsyes clspass" tabindex="{{:tabIndex_1}}">Pass</span>
              <span class="{{:spanAttributes_1}}-no QQ btn clsno clsfail" tabindex="{{:tabIndex_1}}">Fail</span>
            </div>
          </div>
          <div class="col-sm-6 required">
            <div class="col-sm-4-custom">
              <label>{{:question_2}}</label>
            </div>
            <div class="col-sm-2-custom opt-control2">
              <span class="{{:spanAttributes_2}}-yes QQ btn clsyes clspass" tabindex="{{:tabIndex_2}}">Pass</span>
              <span class="{{:spanAttributes_2}}-no QQ btn clsno clsfail" tabindex="{{:tabIndex_2}}">Fail</span>
            </div>
          </div>
        </div>
      {{else questionType3}}
        <div class="row">
          <div class="col-sm-6 left-most-col required">
            <div class="col-sm-4-custom">
              <label>{{:question_1}}</label>
            </div>
            <div class="col-sm-2-custom opt-control2">
              <span class="{{:spanAttributes_1}}-yes QQ btn clsyes" tabindex="{{:tabIndex_1}}">Yes</span>
              <span class="{{:spanAttributes_1}}-no QQ btn clsno" tabindex="{{:tabIndex_1}}">No</span>
              <span class="{{:spanAttributes_1}}-na QQ btn clsna" tabindex="{{:tabIndex_1}}">N&#47;A</span>
            </div>
          </div>
          <div class="col-sm-6 required">
            <div class="col-sm-4-custom">
              <label>{{:question_2}}</label>
            </div>
            <div class="col-sm-2-custom opt-control2">
              <span class="{{:spanAttributes_2}}-yes QQ btn clsyes clspass" tabindex="{{:tabIndex_2}}">Pass</span>
              <span class="{{:spanAttributes_2}}-no QQ btn clsno clsfail" tabindex="{{:tabIndex_2}}">Fail</span>
            </div>
          </div>
        </div>
      {{else questionType4}}
        <div class="row">
          <div class="col-sm-6 left-most-col required">
            <div class="col-sm-4-custom">
              <label>{{:question_1}}</label>
            </div>
            <div class="col-sm-2-custom opt-control2">
              <span class="{{:spanAttributes_1}}-yes QQ btn clsyes clspass" tabindex="{{:tabIndex_1}}">Pass</span>
              <span class="{{:spanAttributes_1}}-no QQ btn clsno clsfail" tabindex="{{:tabIndex_1}}">Fail</span>
              <span class="{{:spanAttributes_1}}-na QQ btn clsna" tabindex="{{:tabIndex_1}}">N&#47;A</span>
            </div>
          </div>
          <div class="col-sm-6">
          </div>
        </div>
      {{/if}}
    {{/for}}
  </script>

  <script id="script-table-test-report" type="text/x-jsrender">
    {{for tableTestReportWithSwitchColumns}}
      {{if emptyColumn}}
        <td class="empty-grid-column">
        </td>
      {{else fieldID && class}}
        <td class="{{:class}}">
          <div class="{{:fieldClass}} form-control input-sm single-line imask" id="{{:fieldID}}" contenteditable="true" data-ph="{{:dataPH}}" tabindex="{{:tabIndex}}"></div>
          <span class="unit">{{:unit}}</span>
        </td>
      {{else fieldID}}
        <td>
          <div class="{{:fieldClass}} form-control input-sm single-line imask" id="{{:fieldID}}" contenteditable="true" data-ph="{{:dataPH}}" tabindex="{{:tabIndex}}"></div>
          <span class="unit">{{:unit}}</span>
        </td>
      {{else labelID && class}}
        <td class="{{:class}}">
          <label class="unit-label">{{:unit}}</label>
          <label class="{{:fieldClass}} figure-label" id="{{:labelID}}">{{:value}}</label>
        </td>
      {{else labelID}}
        <td>
          <label class="unit-label">{{:unit}}</label>
          <label class="{{:fieldClass}} figure-label" id="{{:labelID}}">{{:value}}</label>
        </td>
      {{else eavRow}}
        <td colspan="3" class="sum">
          <label class="unit-label">%</label>
          <label class="QA QA_273 figure-label" id="tbl2-eav">0.00</label>
          <label>E<sub>AV</sub> = </label>
        </td>
        <td colspan="3" class="sum">
          <label class="unit-label">%</label>
          <label class="QA QA_274 figure-label" id="tbl2-eavc">0.00</label>
          <label>E<sub>AV,C</sub> = </label>
        </td>
        <td class="empty-grid-column">
        </td>
      {{else ecRow}}
        <td colspan="3" class="empty-grid-column">
        </td>
        <td colspan="3" class="sum">
          <label class="unit-label">%</label>
          <label class="QA QA_275 figure-label" id="tbl2-ec-compensated-switch">0.00</label>
          <label>E<sub>C</sub> = </label>
        </td>
        <td class="empty-grid-column">
        </td>
      {{/if}}
    {{/for}}
  </script>

  <!--// jsRender templates -->


  <!-- Latest compiled and minified JavaScript -->
  <script src="js/libraries/jquery-2.2.1.min.js"></script>
  <script src="bootstrap/bootstrap-3.3.6-dist/js/bootstrap.min.js"></script>
  <script src="js/libraries/moment.min.2.12.0.js"></script>
  <script src="js/libraries/jSignature.js"></script>
  <script src="js/libraries/jquery-ui-bootstrap-jquery-ui-bootstrap-71f2e47/js/jquery-ui-1.9.2.custom.min.js"></script>
  <script src="js/libraries/jsrender-master/jsrender.min.js"></script>
  <script src="js/libraries/RobinHerbots-jquery.inputmask-66fbc3b/dist/min/jquery.inputmask.bundle.min.js"></script>
  <!-- Virtual keyboard's libraries -->
  <script src="js/libraries/touch-punch/jquery.ui.touch-punch.min.js"></script>
  <script type="text/javascript">

    var is_loading = false;
    var actno = '-1';
    var inc = '';
    var diag, jdiag;
    var check = false;
    var storage = window.localStorage;
    // Virtual keyboard's variables
    var vk_moved = false;
    var current_node_id = '';
    var initial_vb_hooked = false;

    // Content for section General Characteristics
    var data_1 = new Array();
    data_1 = {generalCharacteristicRows: [
                                        {questionType1: 1, question_1: "Does the instrument comply with its Certificate(s) of Approval?", spanAttributes_1: "QA QA_029 QL_instrument-comply", tabIndex_1: "29",
                                          question_2: "Is the instrument being used in an appropriate manner?", spanAttributes_2: "QA QA_030 QA_instrument-appropriate", tabIndex_2: "30"},
                                        {showSeparatorLine: 1},
                                        {questionType1: 1, question_1: "Are all mandatory descriptive markings clearly and permanently marked on the data plate?", spanAttributes_1: "QA QA_031 QL_instrument-mandatory-markings",
                                        tabIndex_1: "31",
                                          question_2: "Is the data plate fixed on the dispenser?", spanAttributes_2: "QA QA_032 QL_instrument-data-plate-fixed", tabIndex_2: "32"},
                                        {showSeparatorLine: 1},
                                        {questionType1: 1, question_1: "Is the dispenser complete?", spanAttributes_1: "QA QA_033 QL_instrument-complete", tabIndex_1: "33",
                                          question_2: "Is the dispenser clean?", spanAttributes_2: "QA QA_034 QL_instrument-clean", tabIndex_2: "34"},
                                        {showSeparatorLine: 1},
                                        {questionType1: 1, question_1: "Is the dispenser operational?", spanAttributes_1: "QA QA_035 QL_instrument-operational", tabIndex_1: "35",
                                          question_2: "Is the operation of the dispenser free of any apparent obstructions?", spanAttributes_2: "QA QA_036 QL_instrument-operation-obstructions", tabIndex_2: "36"},
                                        {showSeparatorLine: 1},
                                        {questionType1: 1, question_1: "Is the dispenser firmly fixed on its foundations?", spanAttributes_1: "QA QA_037 QL_instrument-dispenser-foundations", tabIndex_1: "37",
                                          question_2: "Are all external panels secure?", spanAttributes_2: "QA QA_038 QL_instrument-external-panels", tabIndex_2: "38"},
                                        {showSeparatorLine: 1},
                                        {questionType1: 1, question_1: "Are the cover windows broken?", spanAttributes_1: "QA QA_039 QL_instrument-cover-windows", tabIndex_1: "39",
                                          question_2: "Does the operator (and where applicable, the customer) have a clear and unobstructed view of the indicating device and the entire measuring process?",
                                            spanAttributes_2: "QA QA_040 QL_instrument-measuring-process", tabIndex_2: "40"},
                                        {showSeparatorLine: 1},
                                        {questionType1: 1, question_1: "Do the indications of volume, unit price and total price correspond with the selected hose?", spanAttributes_1: "QA QA_041 QL_instrument-indication-corresponding",
                                          tabIndex_1: "41",
                                          question_2: "Are all indications clearly visible under all conditions day and night?", spanAttributes_2: "QA QA_042 QL_instrument-indication-visibility", tabIndex_2: "42"},
                                        {showSeparatorLine: 1},
                                        {questionType1: 1, question_1: "Are the hoses in reasonable condition, e.g. they are not badly chafed, split, or worn through to the fabric?",
                                        spanAttributes_1: "QA QA_043 QL_instrument-hose-condition", tabIndex_1: "43", question_2: "Are there any leaks?", spanAttributes_2: "QA QA_044 QL_instrument-leaks", tabIndex_2: "44"},
                                        {showSeparatorLine: 1},
                                        {questionType3: 1, question_1: "For self-service systems: do the dispenser number(s) correspond with the console?", spanAttributes_1: "QA QA_045 QL_instrument-self-service", tabIndex_1: "45",
                                          question_2: "Checking facility for indicating devices (clause 4.1)", spanAttributes_2: "QA QA_046 QL_checking-facility", tabIndex_2: "46"},
                                        {showSeparatorLine: 1},
                                        {questionType2: 1, question_1: "Zero setting (clause 4.2)", spanAttributes_1: "QA QA_047 QL_zero-setting", tabIndex_1: "47",
                                          question_2: "Price computing (clause 4.3)", spanAttributes_2: "QA QA_048 QL_price-computing", tabIndex_2: "48"},
                                        {showSeparatorLine: 1},
                                        {questionType2: 1, question_1: "Interlock (clause 4.4)", spanAttributes_1: "QA QA_049 QL_interlock", tabIndex_1: "49",
                                          question_2: "Density and temperature settings (clause 4.5)", spanAttributes_2: "QA QA_050 QL_density-temperature", tabIndex_2: "50"},
                                        {showSeparatorLine: 1},
                                        {questionType4: 1, question_1: "Pre-set indication (clause 4.6)", spanAttributes_1: "QA QA_051 QL_pre-set-indication", tabIndex_1: "51"}
                                      ]}


    // Content for table-test-report-with-switch
    var data_2 = new Array();
    data_2[0] = {tableTestReportWithSwitchColumns: [
                                                        {fieldID: "tmm-delivery-qmax-1", unit: "<sup>o</sup>C", noClass: "", fieldClass: "QA QA_066", dataPH: "0.0", tabIndex: "66" },
                                                        {fieldID: "tmm-delivery-qmax-2", unit: "<sup>o</sup>C", noClass: "", fieldClass: "QA QA_067", dataPH: "0.0", tabIndex: "81"},
                                                        {fieldID: "tmm-delivery-qmax-3", unit: "<sup>o</sup>C", noClass: "", fieldClass: "QA QA_068", dataPH: "0.0", tabIndex: "96"},
                                                        {fieldID: "tmm-delivery-qmax-spare", unit: "<sup>o</sup>C", noClass: "", fieldClass: "QA QA_069", dataPH: "0.0", tabIndex: "111"},
                                                        {emptyColumn: 1},
                                                        {fieldID: "tmm-delivery-qmin-1", unit: "<sup>o</sup>C", noClass: "", fieldClass: "QA QA_070", dataPH: "0.0", tabIndex: "133"},
                                                        {fieldID: "tmm-delivery-qmin-spare", unit: "<sup>o</sup>C", noClass: "", fieldClass: "QA QA_071", dataPH: "0.0", tabIndex: "148"}
                                                      ] };
    data_2[1] = {tableTestReportWithSwitchColumns: [
                                                        {fieldID: "pmm-delivery-qmax-1", unit: "kPa", noClass: "", fieldClass: "QA QA_072", dataPH: "0000000", tabIndex: "67"},
                                                        {fieldID: "pmm-delivery-qmax-2", unit: "kPa", noClass: "", fieldClass: "QA QA_073", dataPH: "0000000", tabIndex: "82"},
                                                        {fieldID: "pmm-delivery-qmax-3", unit: "kPa", noClass: "", fieldClass: "QA QA_074", dataPH: "0000000", tabIndex: "97"},
                                                        {fieldID: "pmm-delivery-qmax-spare", unit: "kPa", noClass: "", fieldClass: "QA QA_075", dataPH: "0000000", tabIndex: "112"},
                                                        {emptyColumn: 1},
                                                        {fieldID: "pmm-delivery-qmin-1", unit: "kPa", noClass: "", fieldClass: "QA QA_076", dataPH: "0000000", tabIndex: "134"},
                                                        {fieldID: "pmm-delivery-qmin-spare", unit: "kPa", noClass: "", fieldClass: "QA QA_077", dataPH: "0000000", tabIndex: "149"}
                                                      ] };
    data_2[2] = {tableTestReportWithSwitchColumns: [
                                                        {fieldID: "dp-delivery-qmax-1", unit: "kPa", noClass: "", fieldClass: "QA QA_078", dataPH: "0000000", tabIndex: "68"},
                                                        {fieldID: "dp-delivery-qmax-2", unit: "kPa", noClass: "", fieldClass: "QA QA_079", dataPH: "0000000", tabIndex: "83"},
                                                        {fieldID: "dp-delivery-qmax-3", unit: "kPa", noClass: "", fieldClass: "QA QA_080", dataPH: "0000000", tabIndex: "98"},
                                                        {fieldID: "dp-delivery-qmax-spare", unit: "kPa", noClass: "", fieldClass: "QA QA_081", dataPH: "0000000", tabIndex: "113"},
                                                        {emptyColumn: 1},
                                                        {fieldID: "dp-delivery-qmin-1", unit: "kPa", noClass: "", fieldClass: "QA QA_082", dataPH: "0000000", tabIndex: "135"},
                                                        {fieldID: "dp-delivery-qmin-spare", unit: "kPa", noClass: "", fieldClass: "QA QA_083", dataPH: "0000000", tabIndex: "150"}
                                                      ] };
    data_2[3] = {tableTestReportWithSwitchColumns: [
                                                        {fieldID: "vmm-delivery-qmax-1", unit: "L", class: "vref-factor", fieldClass: "QA QA_084", dataPH: "0.00", tabIndex: "69"},
                                                        {fieldID: "vmm-delivery-qmax-2", unit: "L", class: "vref-factor", fieldClass: "QA QA_085", dataPH: "0.00", tabIndex: "84"},
                                                        {fieldID: "vmm-delivery-qmax-3", unit: "L", class: "vref-factor", fieldClass: "QA QA_086", dataPH: "0.00", tabIndex: "99"},
                                                        {fieldID: "vmm-delivery-qmax-spare", unit: "L", class: "vref-factor", fieldClass: "QA QA_087", dataPH: "0.00", tabIndex: "114"},
                                                        {emptyColumn: 1},
                                                        {fieldID: "vmm-delivery-qmin-1", unit: "L", class: "vref-factor", fieldClass: "QA QA_088", dataPH: "0.00", tabIndex: "136"},
                                                        {fieldID: "vmm-delivery-qmin-spare", unit: "L", class: "vref-factor", fieldClass: "QA QA_089", dataPH: "0.00", tabIndex: "151"}
                                                      ] };
    data_2[4] = {tableTestReportWithSwitchColumns: [
                                                        {fieldID: "mfr-delivery-qmax-1", unit: "L/min", noClass: "", fieldClass: "QA QA_090", dataPH: "0000", tabIndex: "70"},
                                                        {fieldID: "mfr-delivery-qmax-2", unit: "L/min", noClass: "", fieldClass: "QA QA_091", dataPH: "0000", tabIndex: "85"},
                                                        {fieldID: "mfr-delivery-qmax-3", unit: "L/min", noClass: "", fieldClass: "QA QA_092", dataPH: "0000", tabIndex: "100"},
                                                        {fieldID: "mfr-delivery-qmax-spare", unit: "L/min", noClass: "", fieldClass: "QA QA_093", dataPH: "0000", tabIndex: "115"},
                                                        {emptyColumn: 1},
                                                        {fieldID: "mfr-delivery-qmin-1", unit: "L/min", noClass: "", fieldClass: "QA QA_094", dataPH: "0000", tabIndex: "137"},
                                                        {fieldID: "mfr-delivery-qmin-spare", unit: "L/min", noClass: "", fieldClass: "QA QA_095", dataPH: "0000", tabIndex: "152"}
                                                      ] };
    data_2[5] = {tableTestReportWithSwitchColumns: [
                                                        {fieldID: "mfmm-delivery-qmax-1", unit: "", class: "vref-factor", fieldClass: "QA QA_096", dataPH: "0.0000", tabIndex: "71"},
                                                        {fieldID: "mfmm-delivery-qmax-2", unit: "", class: "vref-factor", fieldClass: "QA QA_097", dataPH: "0.0000", tabIndex: "86"},
                                                        {fieldID: "mfmm-delivery-qmax-3", unit: "", class: "vref-factor", fieldClass: "QA QA_098", dataPH: "0.0000", tabIndex: "101"},
                                                        {fieldID: "mfmm-delivery-qmax-spare", unit: "", class: "vref-factor", fieldClass: "QA QA_099", dataPH: "0.0000", tabIndex: "116"},
                                                        {emptyColumn: 1},
                                                        {fieldID: "mfmm-delivery-qmin-1", unit: "", class: "vref-factor", fieldClass: "QA QA_100", dataPH: "0.0000", tabIndex: "138"},
                                                        {fieldID: "mfmm-delivery-qmin-spare", unit: "", class: "vref-factor", fieldClass: "QA QA_101", dataPH: "0.0000", tabIndex: "153"}
                                                      ] };
    data_2[6] = {tableTestReportWithSwitchColumns: [
                                                        {fieldID: "ctimm-delivery-qmax-1", unit: "", class: "vref-factor", fieldClass: "QA QA_102", dataPH: "0.0000", tabIndex: "72"},
                                                        {fieldID: "ctimm-delivery-qmax-2", unit: "", class: "vref-factor", fieldClass: "QA QA_103", dataPH: "0.0000", tabIndex: "87"},
                                                        {fieldID: "ctimm-delivery-qmax-3", unit: "", class: "vref-factor", fieldClass: "QA QA_104", dataPH: "0.0000", tabIndex: "102"},
                                                        {fieldID: "ctimm-delivery-qmax-spare", unit: "", class: "vref-factor", fieldClass: "QA QA_105", dataPH: "0.0000", tabIndex: "117"},
                                                        {emptyColumn: 1},
                                                        {fieldID: "ctimm-delivery-qmin-1", unit: "", class: "vref-factor", fieldClass: "QA QA_106", dataPH: "0.0000", tabIndex: "139"},
                                                        {fieldID: "ctimm-delivery-qmin-spare", unit: "", class: "vref-factor", fieldClass: "QA QA_107", dataPH: "0.0000", tabIndex: "154"}
                                                      ] };
    data_2[7] = {tableTestReportWithSwitchColumns: [
                                                        {fieldID: "cpimm-delivery-qmax-1", unit: "", class: "vref-factor", fieldClass: "QA QA_108", dataPH: "0.0000", tabIndex: "73"},
                                                        {fieldID: "cpimm-delivery-qmax-2", unit: "", class: "vref-factor", fieldClass: "QA QA_109", dataPH: "0.0000", tabIndex: "88"},
                                                        {fieldID: "cpimm-delivery-qmax-3", unit: "", class: "vref-factor", fieldClass: "QA QA_110", dataPH: "0.0000", tabIndex: "103"},
                                                        {fieldID: "cpimm-delivery-qmax-spare", unit: "", class: "vref-factor", fieldClass: "QA QA_111", dataPH: "0.0000", tabIndex: "118"},
                                                        {emptyColumn: 1},
                                                        {fieldID: "cpimm-delivery-qmin-1", unit: "", class: "vref-factor", fieldClass: "QA QA_112", dataPH: "0.0000", tabIndex: "140"},
                                                        {fieldID: "cpimm-delivery-qmin-spare", unit: "", class: "vref-factor", fieldClass: "QA QA_113", dataPH: "0.0000", tabIndex: "155"}
                                                      ] };
    data_2[8] = {tableTestReportWithSwitchColumns: [
                                                        {labelID: "vref-delivery-qmax-1", value: "0.00", unit: "L", class: "sum", fieldClass: "QA QA_114 two-decimal", tabIndex: "74"},
                                                        {labelID: "vref-delivery-qmax-2", value: "0.00", unit: "L", class: "sum", fieldClass: "QA QA_115 two-decimal", tabIndex: "89"},
                                                        {labelID: "vref-delivery-qmax-3", value: "0.00", unit: "L", class: "sum", fieldClass: "QA QA_116 two-decimal", tabIndex: "104"},
                                                        {labelID: "vref-delivery-qmax-spare", value: "0.00", unit: "L", class: "sum", fieldClass: "QA QA_117 two-decimal", tabIndex: "119"},
                                                        {emptyColumn: 1},
                                                        {labelID: "vref-delivery-qmin-1", value: "0.00", unit: "L", class: "sum", fieldClass: "QA QA_118 two-decimal", tabIndex: "141"},
                                                        {labelID: "vref-delivery-qmin-spare", value: "0.00", unit: "L", class: "sum", fieldClass: "QA QA_119 two-decimal", tabIndex: "156"}
                                                      ] };
    data_2[9] = {tableTestReportWithSwitchColumns: [
                                                        {fieldID: "tfd-delivery-qmax-1", unit: "<sup>o</sup>C", class: "tfd-tfdi", fieldClass: "QA QA_120", dataPH: "0.0", tabIndex: "75"},
                                                        {fieldID: "tfd-delivery-qmax-2", unit: "<sup>o</sup>C", class: "tfd-tfdi", fieldClass: "QA QA_121", dataPH: "0.0", tabIndex: "90"},
                                                        {fieldID: "tfd-delivery-qmax-3", unit: "<sup>o</sup>C", class: "tfd-tfdi", fieldClass: "QA QA_122", dataPH: "0.0", tabIndex: "105"},
                                                        {fieldID: "tfd-delivery-qmax-spare", unit: "<sup>o</sup>C", class: "tfd-tfdi", fieldClass: "QA QA_123", dataPH: "0.0", tabIndex: "120"},
                                                        {fieldID: "tfd-delivery-compensated-switch", unit: "<sup>o</sup>C", class: "tfd-tfdi", fieldClass: "QA QA_124", dataPH: "0.0", tabIndex: "126"},
                                                        {fieldID: "tfd-delivery-qmin-1", unit: "<sup>o</sup>C", class: "tfd-tfdi", fieldClass: "QA QA_125", dataPH: "0.0", tabIndex: "142"},
                                                        {fieldID: "tfd-delivery-qmin-spare", unit: "<sup>o</sup>C", class: "tfd-tfdi", fieldClass: "QA QA_126", dataPH: "0.0", tabIndex: "157"}
                                                      ] };
    data_2[10] = {tableTestReportWithSwitchColumns: [
                                                        {fieldID: "tfdfdi-delivery-qmax-1", dataPH: "0.0", unit: "<sup>o</sup>C", class: "sum", fieldClass: "QA QA_127", tabIndex: "76"},
                                                        {fieldID: "tfdfdi-delivery-qmax-2", dataPH: "0.0", unit: "<sup>o</sup>C", class: "sum", fieldClass: "QA QA_128", tabIndex: "91"},
                                                        {fieldID: "tfdfdi-delivery-qmax-3", dataPH: "0.0", unit: "<sup>o</sup>C", class: "sum", fieldClass: "QA QA_129", tabIndex: "106"},
                                                        {fieldID: "tfdfdi-delivery-qmax-spare", dataPH: "0.0", unit: "<sup>o</sup>C", class: "sum", fieldClass: "QA QA_130", tabIndex: "121"},
                                                        {fieldID: "tfdfdi-delivery-compensated-switch", dataPH: "0.0", unit: "<sup>o</sup>C", class: "sum", fieldClass: "QA QA_131", tabIndex: "127"},
                                                        {fieldID: "tfdfdi-delivery-qmin-1", dataPH: "0.0", unit: "<sup>o</sup>C", class: "sum", fieldClass: "QA QA_132", tabIndex: "143"},
                                                        {fieldID: "tfdfdi-delivery-qmin-spare", dataPH: "0.0", unit: "<sup>o</sup>C", class: "sum", fieldClass: "QA QA_133", tabIndex: "158"}
                                                      ] };
    data_2[11] = {tableTestReportWithSwitchColumns: [
                                                        {fieldID: "vfd-delivery-qmax-1", unit: "L", class: "vfdc-factor", fieldClass: "QA QA_134", dataPH: "0.00", tabIndex: "77"},
                                                        {fieldID: "vfd-delivery-qmax-2", unit: "L", class: "vfdc-factor", fieldClass: "QA QA_135", dataPH: "0.00", tabIndex: "92"},
                                                        {fieldID: "vfd-delivery-qmax-3", unit: "L", class: "vfdc-factor", fieldClass: "QA QA_136", dataPH: "0.00", tabIndex: "107"},
                                                        {fieldID: "vfd-delivery-qmax-spare", unit: "L", class: "vfdc-factor", fieldClass: "QA QA_137", dataPH: "0.00", tabIndex: "122"},
                                                        {fieldID: "vfd-delivery-compensated-switch", unit: "L", class: "vfdc-factor", fieldClass: "QA QA_138", dataPH: "0.00", tabIndex: "128"},
                                                        {fieldID: "vfd-delivery-qmin-1", unit: "L", class: "vfdc-factor", fieldClass: "QA QA_139", dataPH: "0.00", tabIndex: "144"},
                                                        {fieldID: "vfd-delivery-qmin-spare", unit: "L", class: "vfdc-factor", fieldClass: "QA QA_140", dataPH: "0.00", tabIndex: "159"}
                                                      ] };
    data_2[12] = {tableTestReportWithSwitchColumns: [
                                                        {emptyColumn: 1},
                                                        {emptyColumn: 1},
                                                        {emptyColumn: 1},
                                                        {emptyColumn: 1},
                                                        {fieldID: "vfd15-delivery-compensated-switch", unit: "L", noClass: "", fieldClass: "QA QA_141", dataPH: "0.00", tabIndex: "129"},
                                                        {emptyColumn: 1},
                                                        {emptyColumn: 1}
                                                      ] };
    data_2[13] = {tableTestReportWithSwitchColumns: [
                                                        {fieldID: "ctifd-delivery-qmax-1", unit: "", class: "vfdc-factor", fieldClass: "QA QA_142", dataPH: "0.0000", tabIndex: "78"},
                                                        {fieldID: "ctifd-delivery-qmax-2", unit: "", class: "vfdc-factor", fieldClass: "QA QA_143", dataPH: "0.0000", tabIndex: "93"},
                                                        {fieldID: "ctifd-delivery-qmax-3", unit: "", class: "vfdc-factor", fieldClass: "QA QA_144", dataPH: "0.0000", tabIndex: "108"},
                                                        {fieldID: "ctifd-delivery-qmax-spare", unit: "", class: "vfdc-factor", fieldClass: "QA QA_145", dataPH: "0.0000", tabIndex: "123"},
                                                        {fieldID: "ctifd-delivery-compensated-switch", unit: "", class: "vfdc-factor", fieldClass: "QA QA_146", dataPH: "0.0000", tabIndex: "130"},
                                                        {fieldID: "ctifd-delivery-qmin-1", unit: "", class: "vfdc-factor", fieldClass: "QA QA_147", dataPH: "0.0000", tabIndex: "145"},
                                                        {fieldID: "ctifd-delivery-qmin-spare", unit: "", class: "vfdc-factor", fieldClass: "QA QA_148", dataPH: "0.0000", tabIndex: "160"}
                                                      ] };
    data_2[14] = {tableTestReportWithSwitchColumns: [
                                                        {labelID: "vfdc-delivery-qmax-1", value: "0.00", unit: "L", class: "sum", fieldClass: "QA QA_149 two-decimal", tabIndex: "79"},
                                                        {labelID: "vfdc-delivery-qmax-2", value: "0.00", unit: "L", class: "sum", fieldClass: "QA QA_150 two-decimal", tabIndex: "94"},
                                                        {labelID: "vfdc-delivery-qmax-3", value: "0.00", unit: "L", class: "sum", fieldClass: "QA QA_151 two-decimal", tabIndex: "109"},
                                                        {labelID: "vfdc-delivery-qmax-spare", value: "0.00", unit: "L", class: "sum", fieldClass: "QA QA_152 two-decimal", tabIndex: "124"},
                                                        {labelID: "vfdc-delivery-compensated-switch", value: "0.00", unit: "L", class: "sum", fieldClass: "QA QA_153 two-decimal", tabIndex: "131"},
                                                        {labelID: "vfdc-delivery-qmin-1", value: "0.00", unit: "L", class: "sum", fieldClass: "QA QA_154 two-decimal", tabIndex: "146"},
                                                        {labelID: "vfdc-delivery-qmin-spare", value: "0.00", unit: "L", class: "sum", fieldClass: "QA QA_155 two-decimal", tabIndex: "161"}
                                                      ] };
    data_2[15] = {tableTestReportWithSwitchColumns: [
                                                        {labelID: "efd-delivery-qmax-1", value: "0.00", unit: "%", noClass: "", class: "sum", fieldClass: "QA QA_156 two-decimal", tabIndex: "80"},
                                                        {labelID: "efd-delivery-qmax-2", value: "0.00", unit: "%", noClass: "", class: "sum", fieldClass: "QA QA_157 two-decimal", tabIndex: "95"},
                                                        {labelID: "efd-delivery-qmax-3", value: "0.00", unit: "%", noClass: "", class: "sum", fieldClass: "QA QA_158 two-decimal", tabIndex: "110"},
                                                        {labelID: "efd-delivery-qmax-spare", value: "0.00", unit: "%", noClass: "", class: "sum", fieldClass: "QA QA_159 two-decimal", tabIndex: "125"},
                                                        {emptyColumn: 1},
                                                        {labelID: "efd-delivery-qmin-1", value: "0.00", unit: "%", noClass: "", class: "sum", fieldClass: "QA QA_160 two-decimal", tabIndex: "147"},
                                                        {labelID: "efd-delivery-qmin-spare", value: "0.00", unit: "%", noClass: "", class: "sum", fieldClass: "QA QA_161 two-decimal", tabIndex: "162"}
                                                      ] };
    data_2[16] = {tableTestReportWithSwitchColumns: [
                                                        {emptyColumn: 1},
                                                        {emptyColumn: 1},
                                                        {emptyColumn: 1},
                                                        {emptyColumn: 1},
                                                        {labelID: "ec-compensated-switch", value: "0.00", unit: "%", class: "sum", fieldClass: "QA QA_162", tabIndex: "132"},
                                                        {emptyColumn: 1},
                                                        {emptyColumn: 1}
                                                      ] };


    // Content for table-test-report-without-switch
    var data_3 = new Array();
    data_3[0] = {tableTestReportWithSwitchColumns: [
                                                        {fieldID: "tbl2-tmm-delivery-qmax-1", unit: "<sup>o</sup>C", noClass: "", fieldClass: "QA QA_168", dataPH: "0.0", tabIndex: "163"},
                                                        {fieldID: "tbl2-tmm-delivery-qmax-2", unit: "<sup>o</sup>C", noClass: "", fieldClass: "QA QA_169", dataPH: "0.0", tabIndex: "178"},
                                                        {fieldID: "tbl2-tmm-delivery-qmax-3", unit: "<sup>o</sup>C", noClass: "", fieldClass: "QA QA_170", dataPH: "0.0", tabIndex: "193"},
                                                        {fieldID: "tbl2-tmm-delivery-compensated-qmax-1", unit: "<sup>o</sup>C", noClass: "", fieldClass: "QA QA_171", dataPH: "0.0", tabIndex: "208"},
                                                        {fieldID: "tbl2-tmm-delivery-compensated-qmax-2", unit: "<sup>o</sup>C", noClass: "", fieldClass: "QA QA_172", dataPH: "0.0", tabIndex: "223"},
                                                        {fieldID: "tbl2-tmm-delivery-compensated-qmax-3", unit: "<sup>o</sup>C", noClass: "", fieldClass: "QA QA_173", dataPH: "0.0", tabIndex: "238"},
                                                        {fieldID: "tbl2-tmm-delivery-qmin", unit: "<sup>o</sup>C", noClass: "", fieldClass: "QA QA_174", dataPH: "0.0", tabIndex: "253"}
                                                      ] };
    data_3[1] = {tableTestReportWithSwitchColumns: [
                                                        {fieldID: "tbl2-pmm-delivery-qmax-1", unit: "kPa", noClass: "", fieldClass: "QA QA_175", dataPH: "0000000", tabIndex: "164"},
                                                        {fieldID: "tbl2-pmm-delivery-qmax-2", unit: "kPa", noClass: "", fieldClass: "QA QA_176", dataPH: "0000000", tabIndex: "179"},
                                                        {fieldID: "tbl2-pmm-delivery-qmax-3", unit: "kPa", noClass: "", fieldClass: "QA QA_177", dataPH: "0000000", tabIndex: "194"},
                                                        {fieldID: "tbl2-pmm-delivery-compensated-qmax-1", unit: "kPa", noClass: "", fieldClass: "QA QA_178", dataPH: "0000000", tabIndex: "209"},
                                                        {fieldID: "tbl2-pmm-delivery-compensated-qmax-2", unit: "kPa", noClass: "", fieldClass: "QA QA_179", dataPH: "0000000", tabIndex: "224"},
                                                        {fieldID: "tbl2-pmm-delivery-compensated-qmax-3", unit: "kPa", noClass: "", fieldClass: "QA QA_180", dataPH: "0000000", tabIndex: "239"},
                                                        {fieldID: "tbl2-pmm-delivery-qmin", unit: "kPa", noClass: "", fieldClass: "QA QA_181", dataPH: "0000000", tabIndex: "254"}
                                                      ] };
    data_3[2] = {tableTestReportWithSwitchColumns: [
                                                        {fieldID: "tbl2-dp-delivery-qmax-1", unit: "kPa", noClass: "", fieldClass: "QA QA_182", dataPH: "0000000", tabIndex: "165"},
                                                        {fieldID: "tbl2-dp-delivery-qmax-2", unit: "kPa", noClass: "", fieldClass: "QA QA_183", dataPH: "0000000", tabIndex: "180"},
                                                        {fieldID: "tbl2-dp-delivery-qmax-3", unit: "kPa", noClass: "", fieldClass: "QA QA_184", dataPH: "0000000", tabIndex: "195"},
                                                        {fieldID: "tbl2-dp-delivery-compensated-qmax-1", unit: "kPa", noClass: "", fieldClass: "QA QA_185", dataPH: "0000000", tabIndex: "210"},
                                                        {fieldID: "tbl2-dp-delivery-compensated-qmax-2", unit: "kPa", noClass: "", fieldClass: "QA QA_186", dataPH: "0000000", tabIndex: "225"},
                                                        {fieldID: "tbl2-dp-delivery-compensated-qmax-3", unit: "kPa", noClass: "", fieldClass: "QA QA_187", dataPH: "0000000", tabIndex: "240"},
                                                        {fieldID: "tbl2-dp-delivery-qmin", unit: "kPa", noClass: "", fieldClass: "QA QA_188", dataPH: "0000000", tabIndex: "255"}
                                                      ] };
    data_3[3] = {tableTestReportWithSwitchColumns: [
                                                        {fieldID: "tbl2-vmm-delivery-qmax-1", unit: "L", class: "vref-factor", fieldClass: "QA QA_189", dataPH: "0.00", tabIndex: "166"},
                                                        {fieldID: "tbl2-vmm-delivery-qmax-2", unit: "L", class: "vref-factor", fieldClass: "QA QA_190", dataPH: "0.00", tabIndex: "181"},
                                                        {fieldID: "tbl2-vmm-delivery-qmax-3", unit: "L", class: "vref-factor", fieldClass: "QA QA_191", dataPH: "0.00", tabIndex: "196"},
                                                        {fieldID: "tbl2-vmm-delivery-compensated-qmax-1", unit: "L", class: "vref-factor", fieldClass: "QA QA_192", dataPH: "0.00", tabIndex: "211"},
                                                        {fieldID: "tbl2-vmm-delivery-compensated-qmax-2", unit: "L", class: "vref-factor", fieldClass: "QA QA_193", dataPH: "0.00", tabIndex: "226"},
                                                        {fieldID: "tbl2-vmm-delivery-compensated-qmax-3", unit: "L", class: "vref-factor", fieldClass: "QA QA_194", dataPH: "0.00", tabIndex: "241"},
                                                        {fieldID: "tbl2-vmm-delivery-qmin", unit: "L", class: "vref-factor", fieldClass: "QA QA_195", dataPH: "0.00", tabIndex: "256"}
                                                      ] };
    data_3[4] = {tableTestReportWithSwitchColumns: [
                                                        {fieldID: "tbl2-mfr-delivery-qmax-1", unit: "L/min", noClass: "", fieldClass: "QA QA_196", dataPH: "0000", tabIndex: "167"},
                                                        {fieldID: "tbl2-mfr-delivery-qmax-2", unit: "L/min", noClass: "", fieldClass: "QA QA_197", dataPH: "0000", tabIndex: "182"},
                                                        {fieldID: "tbl2-mfr-delivery-qmax-3", unit: "L/min", noClass: "", fieldClass: "QA QA_198", dataPH: "0000", tabIndex: "197"},
                                                        {fieldID: "tbl2-mfr-delivery-compensated-qmax-1", unit: "L/min", noClass: "", fieldClass: "QA QA_199", dataPH: "0000", tabIndex: "212"},
                                                        {fieldID: "tbl2-mfr-delivery-compensated-qmax-2", unit: "L/min", noClass: "", fieldClass: "QA QA_200", dataPH: "0000", tabIndex: "227"},
                                                        {fieldID: "tbl2-mfr-delivery-compensated-qmax-3", unit: "L/min", noClass: "", fieldClass: "QA QA_201", dataPH: "0000", tabIndex: "242"},
                                                        {fieldID: "tbl2-mfr-delivery-qmin", unit: "L/min", noClass: "", fieldClass: "QA QA_202", dataPH: "0000", tabIndex: "257"}
                                                      ] };
    data_3[5] = {tableTestReportWithSwitchColumns: [
                                                        {fieldID: "tbl2-mfmm-delivery-qmax-1", unit: "", class: "vref-factor", fieldClass: "QA QA_203", dataPH: "0.0000", tabIndex: "168"},
                                                        {fieldID: "tbl2-mfmm-delivery-qmax-2", unit: "", class: "vref-factor", fieldClass: "QA QA_204", dataPH: "0.0000", tabIndex: "183"},
                                                        {fieldID: "tbl2-mfmm-delivery-qmax-3", unit: "", class: "vref-factor", fieldClass: "QA QA_205", dataPH: "0.0000", tabIndex: "198"},
                                                        {fieldID: "tbl2-mfmm-delivery-compensated-qmax-1", unit: "", class: "vref-factor", fieldClass: "QA QA_206", dataPH: "0.0000", tabIndex: "213"},
                                                        {fieldID: "tbl2-mfmm-delivery-compensated-qmax-2", unit: "", class: "vref-factor", fieldClass: "QA QA_207", dataPH: "0.0000", tabIndex: "228"},
                                                        {fieldID: "tbl2-mfmm-delivery-compensated-qmax-3", unit: "", class: "vref-factor", fieldClass: "QA QA_208", dataPH: "0.0000", tabIndex: "243"},
                                                        {fieldID: "tbl2-mfmm-delivery-qmin", unit: "", class: "vref-factor", fieldClass: "QA QA_209", dataPH: "0.0000", tabIndex: "258"}
                                                      ] };
    data_3[6] = {tableTestReportWithSwitchColumns: [
                                                        {fieldID: "tbl2-ctimm-delivery-qmax-1", unit: "", class: "vref-factor", fieldClass: "QA QA_210", dataPH: "0.0000", tabIndex: "169"},
                                                        {fieldID: "tbl2-ctimm-delivery-qmax-2", unit: "", class: "vref-factor", fieldClass: "QA QA_211", dataPH: "0.0000", tabIndex: "184"},
                                                        {fieldID: "tbl2-ctimm-delivery-qmax-3", unit: "", class: "vref-factor", fieldClass: "QA QA_212", tabIndex: "199"},
                                                        {fieldID: "tbl2-ctimm-delivery-compensated-qmax-1", unit: "", class: "vref-factor", fieldClass: "QA QA_213", dataPH: "0.0000", tabIndex: "214"},
                                                        {fieldID: "tbl2-ctimm-delivery-compensated-qmax-2", unit: "", class: "vref-factor", fieldClass: "QA QA_214", dataPH: "0.0000", tabIndex: "229"},
                                                        {fieldID: "tbl2-ctimm-delivery-compensated-qmax-3", unit: "", class: "vref-factor", fieldClass: "QA QA_215", dataPH: "0.0000", tabIndex: "244"},
                                                        {fieldID: "tbl2-ctimm-delivery-qmin", unit: "", class: "vref-factor", fieldClass: "QA QA_216", dataPH: "0.0000", tabIndex: "259"}
                                                      ] };
    data_3[7] = {tableTestReportWithSwitchColumns: [
                                                        {fieldID: "tbl2-cpimm-delivery-qmax-1", unit: "", class: "vref-factor", fieldClass: "QA QA_217", dataPH: "0.0000", tabIndex: "170"},
                                                        {fieldID: "tbl2-cpimm-delivery-qmax-2", unit: "", class: "vref-factor", fieldClass: "QA QA_218", dataPH: "0.0000", tabIndex: "185"},
                                                        {fieldID: "tbl2-cpimm-delivery-qmax-3", unit: "", class: "vref-factor", fieldClass: "QA QA_219", dataPH: "0.0000", tabIndex: "200"},
                                                        {fieldID: "tbl2-cpimm-delivery-compensated-qmax-1", unit: "", class: "vref-factor", fieldClass: "QA QA_220", dataPH: "0.0000", tabIndex: "215"},
                                                        {fieldID: "tbl2-cpimm-delivery-compensated-qmax-2", unit: "", class: "vref-factor", fieldClass: "QA QA_221", dataPH: "0.0000", tabIndex: "230"},
                                                        {fieldID: "tbl2-cpimm-delivery-compensated-qmax-3", unit: "", class: "vref-factor", fieldClass: "QA QA_222", dataPH: "0.0000", tabIndex: "245"},
                                                        {fieldID: "tbl2-cpimm-delivery-qmin", unit: "", class: "vref-factor", fieldClass: "QA QA_223", dataPH: "0.0000", tabIndex: "260"}
                                                      ] };
    data_3[8] = {tableTestReportWithSwitchColumns: [
                                                        {labelID: "tbl2-vref-delivery-qmax-1", value: "0.00", unit: "L", class: "sum", fieldClass: "QA QA_224 two-decimal", tabIndex: "171"},
                                                        {labelID: "tbl2-vref-delivery-qmax-2", value: "0.00", unit: "L", class: "sum", fieldClass: "QA QA_225 two-decimal", tabIndex: "186"},
                                                        {labelID: "tbl2-vref-delivery-qmax-3", value: "0.00", unit: "L", class: "sum", fieldClass: "QA QA_226 two-decimal", tabIndex: "201"},
                                                        {labelID: "tbl2-vref-delivery-compensated-qmax-1", value: "0.00", unit: "L", class: "sum", fieldClass: "QA QA_227 two-decimal", tabIndex: "216"},
                                                        {labelID: "tbl2-vref-delivery-compensated-qmax-2", value: "0.00", unit: "L", class: "sum", fieldClass: "QA QA_228 two-decimal", tabIndex: "231"},
                                                        {labelID: "tbl2-vref-delivery-compensated-qmax-3", value: "0.00", unit: "L", class: "sum", fieldClass: "QA QA_229 two-decimal", tabIndex: "246"},
                                                        {labelID: "tbl2-vref-delivery-qmin", value: "0.00", unit: "L", class: "sum", fieldClass: "QA QA_230 two-decimal", tabIndex: "261"}
                                                      ] };
    data_3[9] = {tableTestReportWithSwitchColumns: [
                                                        {fieldID: "tbl2-tfd-delivery-qmax-1", unit: "<sup>o</sup>C", class: "tfd-tfdi", fieldClass: "QA QA_231", dataPH: "0.0", tabIndex: "172"},
                                                        {fieldID: "tbl2-tfd-delivery-qmax-2", unit: "<sup>o</sup>C", class: "tfd-tfdi", fieldClass: "QA QA_232", dataPH: "0.0", tabIndex: "187"},
                                                        {fieldID: "tbl2-tfd-delivery-qmax-3", unit: "<sup>o</sup>C", class: "tfd-tfdi", fieldClass: "QA QA_233", dataPH: "0.0", tabIndex: "202"},
                                                        {fieldID: "tbl2-tfd-delivery-compensated-qmax-1", unit: "<sup>o</sup>C", class: "tfd-tfdi", fieldClass: "QA QA_234", dataPH: "0.0", tabIndex: "217"},
                                                        {fieldID: "tbl2-tfd-delivery-compensated-qmax-2", unit: "<sup>o</sup>C", class: "tfd-tfdi", fieldClass: "QA QA_235", dataPH: "0.0", tabIndex: "232"},
                                                        {fieldID: "tbl2-tfd-delivery-compensated-qmax-3", unit: "<sup>o</sup>C", class: "tfd-tfdi", fieldClass: "QA QA_236", dataPH: "0.0", tabIndex: "247"},
                                                        {fieldID: "tbl2-tfd-delivery-qmin", unit: "<sup>o</sup>C", class: "tfd-tfdi", fieldClass: "QA QA_237", dataPH: "0.0", tabIndex: "262"}
                                                      ] };
    data_3[10] = {tableTestReportWithSwitchColumns: [
                                                        {fieldID: "tbl2-tfdfdi-delivery-qmax-1", dataPH: "0.0", unit: "<sup>o</sup>C", class: "sum", fieldClass: "QA QA_238", tabIndex: "173"},
                                                        {fieldID: "tbl2-tfdfdi-delivery-qmax-2", dataPH: "0.0", unit: "<sup>o</sup>C", class: "sum", fieldClass: "QA QA_239", tabIndex: "188"},
                                                        {fieldID: "tbl2-tfdfdi-delivery-qmax-3", dataPH: "0.0", unit: "<sup>o</sup>C", class: "sum", fieldClass: "QA QA_240", tabIndex: "203"},
                                                        {fieldID: "tbl2-tfdfdi-delivery-compensated-qmax-1", dataPH: "0.0", unit: "<sup>o</sup>C", class: "sum", fieldClass: "QA QA_241", tabIndex: "218"},
                                                        {fieldID: "tbl2-tfdfdi-delivery-compensated-qmax-2", dataPH: "0.0", unit: "<sup>o</sup>C", class: "sum", fieldClass: "QA QA_242", tabIndex: "233"},
                                                        {fieldID: "tbl2-tfdfdi-delivery-compensated-qmax-3", dataPH: "0.0", unit: "<sup>o</sup>C", class: "sum", fieldClass: "QA QA_243", tabIndex: "248"},
                                                        {fieldID: "tbl2-tfdfdi-delivery-qmin", dataPH: "0.0", unit: "<sup>o</sup>C", class: "sum", fieldClass: "QA QA_244", tabIndex: "263"}
                                                      ] };
    data_3[11] = {tableTestReportWithSwitchColumns: [
                                                        {fieldID: "tbl2-vfd-delivery-qmax-1", unit: "L", class: "vfdc-factor", fieldClass: "QA QA_245", dataPH: "0.00", tabIndex: "174"},
                                                        {fieldID: "tbl2-vfd-delivery-qmax-2", unit: "L", class: "vfdc-factor", fieldClass: "QA QA_246", dataPH: "0.00", tabIndex: "189"},
                                                        {fieldID: "tbl2-vfd-delivery-qmax-3", unit: "L", class: "vfdc-factor", fieldClass: "QA QA_247", dataPH: "0.00", tabIndex: "204"},
                                                        {emptyColumn: 1},
                                                        {emptyColumn: 1},
                                                        {emptyColumn: 1},
                                                        {fieldID: "tbl2-vfd-delivery-qmin", unit: "L", class: "vfdc-factor", fieldClass: "QA QA_248", dataPH: "0.00", tabIndex: "264"}
                                                      ] };
    data_3[12] = {tableTestReportWithSwitchColumns: [
                                                        {emptyColumn: 1},
                                                        {emptyColumn: 1},
                                                        {emptyColumn: 1},
                                                        {fieldID: "tbl2-vfd15-delivery-compensated-qmax-1", unit: "L", class: "vfdc-factor", fieldClass: "QA QA_249", dataPH: "0.00", tabIndex: "219"},
                                                        {fieldID: "tbl2-vfd15-delivery-compensated-qmax-2", unit: "L", class: "vfdc-factor", fieldClass: "QA QA_250", dataPH: "0.00", tabIndex: "234"},
                                                        {fieldID: "tbl2-vfd15-delivery-compensated-qmax-3", unit: "L", class: "vfdc-factor", fieldClass: "QA QA_251", dataPH: "0.00", tabIndex: "249"},
                                                        {emptyColumn: 1},
                                                      ] };
    data_3[13] = {tableTestReportWithSwitchColumns: [
                                                        {fieldID: "tbl2-ctifd-delivery-qmax-1", unit: "", class: "vfdc-factor", fieldClass: "QA QA_252", dataPH: "0.0000", tabIndex: "175"},
                                                        {fieldID: "tbl2-ctifd-delivery-qmax-2", unit: "", class: "vfdc-factor", fieldClass: "QA QA_253", dataPH: "0.0000", tabIndex: "190"},
                                                        {fieldID: "tbl2-ctifd-delivery-qmax-3", unit: "", class: "vfdc-factor", fieldClass: "QA QA_254", dataPH: "0.0000", tabIndex: "205"},
                                                        {labelID: "tbl2-ctifd-delivery-compensated-qmax-1", value: "1.0000", class: "vfdc-factor sum", fieldClass: "QA QA_255", dataPH: "0.0000", tabIndex: "220"},
                                                        {labelID: "tbl2-ctifd-delivery-compensated-qmax-2", value: "1.0000", class: "vfdc-factor sum", fieldClass: "QA QA_256", dataPH: "0.0000", tabIndex: "235"},
                                                        {labelID: "tbl2-ctifd-delivery-compensated-qmax-3", value: "1.0000", class: "vfdc-factor sum", fieldClass: "QA QA_257", dataPH: "0.0000", tabIndex: "250"},
                                                        {fieldID: "tbl2-ctifd-delivery-qmin", unit: "", class: "vfdc-factor", fieldClass: "QA QA_258", dataPH: "0.0000", tabIndex: "265"}
                                                      ] };
    data_3[14] = {tableTestReportWithSwitchColumns: [
                                                        {labelID: "tbl2-vfdc-delivery-qmax-1", value: "0.00", unit: "L", class: "sum", fieldClass: "QA QA_259 two-decimal", tabIndex: "176"},
                                                        {labelID: "tbl2-vfdc-delivery-qmax-2", value: "0.00", unit: "L", class: "sum", fieldClass: "QA QA_260 two-decimal", tabIndex: "191"},
                                                        {labelID: "tbl2-vfdc-delivery-qmax-3", value: "0.00", unit: "L", class: "sum", fieldClass: "QA QA_261 two-decimal", tabIndex: "206"},
                                                        {labelID: "tbl2-vfdc-delivery-compensated-qmax-1", value: "0.00", unit: "L", class: "sum", fieldClass: "QA QA_262 two-decimal", tabIndex: "221"},
                                                        {labelID: "tbl2-vfdc-delivery-compensated-qmax-2", value: "0.00", unit: "L", class: "sum", fieldClass: "QA QA_263 two-decimal", tabIndex: "236"},
                                                        {labelID: "tbl2-vfdc-delivery-compensated-qmax-3", value: "0.00", unit: "L", class: "sum", fieldClass: "QA QA_264 two-decimal", tabIndex: "251"},
                                                        {labelID: "tbl2-vfdc-delivery-qmin", value: "0.00", unit: "L", class: "sum", fieldClass: "QA QA_265 two-decimal", tabIndex: "266"}
                                                      ] };
    data_3[15] = {tableTestReportWithSwitchColumns: [
                                                        {labelID: "tbl2-efd-delivery-qmax-1", value: "0.00", unit: "%", class: "sum", fieldClass: "QA QA_266 two-decimal", tabIndex: "177"},
                                                        {labelID: "tbl2-efd-delivery-qmax-2", value: "0.00", unit: "%", class: "sum", fieldClass: "QA QA_267 two-decimal", tabIndex: "192"},
                                                        {labelID: "tbl2-efd-delivery-qmax-3", value: "0.00", unit: "%", class: "sum", fieldClass: "QA QA_268 two-decimal", tabIndex: "207"},
                                                        {labelID: "tbl2-efd-delivery-compensated-qmax-1", value: "0.00", unit: "%", class: "sum", fieldClass: "QA QA_269 two-decimal", tabIndex: "222"},
                                                        {labelID: "tbl2-efd-delivery-compensated-qmax-2", value: "0.00", unit: "%", class: "sum", fieldClass: "QA QA_270 two-decimal", tabIndex: "237"},
                                                        {labelID: "tbl2-efd-delivery-compensated-qmax-3", value: "0.00", unit: "%", class: "sum", fieldClass: "QA QA_271 two-decimal", tabIndex: "252"},
                                                        {labelID: "tbl2-efd-delivery-qmin", value: "0.00", unit: "%", class: "sum", fieldClass: "QA QA_272 two-decimal", tabIndex: "267"}
                                                      ] };
    data_3[16] = {tableTestReportWithSwitchColumns: [
                                                        {eavRow: 1}
                                                      ] };
    data_3[17] = {tableTestReportWithSwitchColumns: [
                                                        {ecRow: 1}
                                                      ] };



    $(document).ready(function() {

      /**
       * JSRender function for section general-characteristics-questions
       */
      var tmpl = $.templates("#script-general-characteristics");
      var html = tmpl.render(data_1);
      $('#general-characteristics-questions').html(html);

      /**
       * JSRender function for table-test-report-with-switch
       */
      $.each( data_2, function( i ) {
        // $.each( data.tableTestReportWithSwitchColumns[i], function(key, val) {

        var tmpl = $.templates('#script-table-test-report');
        var html = tmpl.render(data_2[i]);

        table = document.getElementById('table-test-report-with-switch');
        var rowCount = table.rows.length;
        var tr = table.insertRow(rowCount);
        tr.innerHTML = html;
        if (i == 6 || i == 16) {
          var tr = table.insertRow(rowCount + 1);
          tr.className = 'thick-bar';
          td = tr.insertCell(0);
          td.colSpan = 7;
          if (i == 6) {
            td.className = 'blank-column'
          }
        }
      });

      /**
       * JSRender function for table-test-report-without-switch
       */
      $.each( data_3, function( i ) {
        // $.each( data.tableTestReportWithSwitchColumns[i], function(key, val) {

        var tmpl = $.templates('#script-table-test-report');
        var html = tmpl.render(data_3[i]);

        table = document.getElementById('table-test-report-without-switch');
        var rowCount = table.rows.length;
        var tr = table.insertRow(rowCount);
        tr.innerHTML = html;
        if (i == 6 || i == 17) {
          var tr = table.insertRow(rowCount + 1);
          tr.className = 'thick-bar';
          td = tr.insertCell(0);
          td.colSpan = 7;
          if (i == 6) {
            td.className = 'blank-column'
          }
        }
      });

      /**
       * HTML5 application cache for mobile
       */
      $(document).on("mobileinit", function () {

        $.mobile.allowCrossDomainPages = true; // cross domain page loading
        $.mobile.phonegapNavigationEnabled = true; //Android enabled mobile
        $.mobile.page.prototype.options.domCache = true; //page caching prefech rendering
        $.support.touchOverflow = true; //Android enhanced scrolling
        $.mobile.touchOverflowEnabled = true; // enhanced scrolling transition availible in iOS 5

        //register event to cache site for offline use
        cache = window.applicationCache;
        cache.addEventListener('updateready', cacheUpdatereadyListener, false);
        cache.addEventListener('error', cacheErrorListener, false);
        function cacheUpdatereadyListener() {
          window.applicationCache.update();
          window.applicationCache.swapCache();
        }
        function cacheErrorListener() {
          alert('site not availble offline')
        }
      });


    /**
     * HTML5 application cache
     */
/*
    var appCache = window.applicationCache;

    //appCache.update(); // Attempt to update the user's cache.

    if (appCache.status == window.applicationCache.UPDATEREADY) {
      appCache.update();
      appCache.swapCache();  // The fetch was successful, swap in the new cache.
    }
    // Check if a new cache is available on page load.
    window.addEventListener('load', function(e) {

      window.applicationCache.addEventListener('updateready', function(e) {
        if (window.applicationCache.status == window.applicationCache.UPDATEREADY) {
          // Browser downloaded a new app cache.
          if (confirm('A new version of this site is available. Load it?')) {
            window.location.reload();
          }
        } else {
          // Manifest didn't changed. Nothing new to server.
        }
      }, false);

    }, false);
*/

      hookupSignature();

      $('#btn-submit').click(function(e) {
        e.preventDefault();

        saveVal($('.QA_276'), '');

        // Check mandatory fields
        if ( ! $.trim($('.QA_002').val())) {
          alert('Form cannot be submmited. Activity number is required.');
          return false;
        }

        //var lzstring_param = LZString.compressToBase64(param);
        if (hostReachable() == true) {
          if (validateFormInputs()) {

            // Assign object value into an array variable
            var p_array = Array();
            var p_var = '';
            var param = '';
            var signature_param = '';
            var in_arr = [];
            $.each($('.QA'), function(obj) {
              if ($(this).hasClass('QA_276')) {
                signature_param = getVal('QA_276');
              } else {
                p_array = $(this).attr('class').split(/\s+/);
                p_var = p_array[1];
                // Avoid duplicate parameters
                if (jQuery.inArray(p_var, in_arr) < 0) {
                  if (param != '') {
                    param += '*;';
                  }
                  param += p_var + '|' + getVal(p_var);
                  in_arr.push(p_var);
                }
              }
            });

            // Record device submitted date time
            if (param != '') {
              param += '*;';
            }
            param += 'client_submitted_date|' + getDateTimeNow();

            $.post('postback_internal.php', { param: param, signature_param: signature_param, loc: $('#ex_loc').val() }, function(data) {
              afterSubmit(data.answer);
            },  'json');
          } else {
            $('html, body').animate({
                scrollTop: $('#submit-message-wrapper').offset().top
              }, 2000);
          }
        } else {
          alert('No connect, your data now saved on local storage instead. \nYou can submit again when your device have connection later.');
        }
      });

      $('#btn-reset').click(function(e) {

        var ans = confirm('All your current content will be erased, Continue?');
        if (ans == true) {
          $('#frm').find('#submit-message').remove();
          $('#frm').find('#submit-message-wrapper').remove();
          var temp_gvr_job_no = $('.QA_279').text();
          var temp_act_no = $('.QA_002').val();
          var temp_inc = $('.IncrementNumber').val();
          removeJob($('.QA_002').val(), $('.IncrementNumber').val(), false);
          $('.QA_002').val(temp_act_no);
          $('.IncrementNumber').val(temp_inc);
          $('.QA_279').text(temp_gvr_job_no);
          saveVal($('.QA_002'), '');
          saveVal($('.IncrementNumber'), '');
          saveVal($('.QA_279'), '');
          location.reload();
        }
        return false;
      });

      // Get device date time
      function getDateTimeNow() {
        var today = new Date();
        var dd = today.getDate();
        var mm = today.getMonth() + 1; //January is 0!
        var yyyy = today.getFullYear();
        var hh = today.getHours();
        var mmm = today.getMinutes();
        var nn = today.getSeconds();
        var str = '';
        if( dd < 10) {
            dd = '0' + dd;
        }
        if(mm < 10) {
            mm = '0' + mm;
        }
        if (parseInt(hh) < 10) {
          hh = '0' + hh;
        }
        // USA Date format mm/dd/yyyy
        today = mm+'/'+dd+'/'+yyyy+' ' + hh + ':' + mmm + ':' + nn;
        return today;
      }

      // contentEditable Placeholder
      $('div[data-ph]').on('keydown keypress input', function() {
        if (this.textContent) {
          this.dataset.divPlaceholderContent = 'true';
        } else {
          delete(this.dataset.divPlaceholderContent);
        }
      });

      $('div[data-ph]').focus(function(event) {

        if ($(this).attr('disabled')) {
          return false;
        }
        event.preventDefault();
        event.stopPropagation();

        var divHtml = $(this).html(); // notice "this" instead of a specific #myDiv
        var readonly = '';
        if ( ! $('#virtual-keyboard').hasClass('hide')) {
          readonly = ' readonly="readonly"';
        }
        switch ($(this).attr('data-ph')) {
          case '0000000000':
            var editableText = $('<input type="text" id="' + this.id + '" name="' + this.id + '" class="' + this.className
                            + ' numerical-input" pattern="[0-9]*" placeholder="0000000000" step="1" tabindex="' + $(this).attr('tabindex') + '" ' + readonly + '/>');
            editableText.val(divHtml);
            $(this).replaceWith(editableText);
            $('#' + this.id).inputmask("Regex", ({"regex":"^[0-9]{0,10}$"}) );
            break;
          case '0000000':
            var editableText = $('<input type="text" id="' + this.id + '" name="' + this.id + '" class="' + this.className
                            + ' numerical-input" pattern="[0-9]*" placeholder="0000000" step="1" tabindex="' + $(this).attr('tabindex') + '" ' + readonly + '/>');
            editableText.val(divHtml);
            $(this).replaceWith(editableText);
            $('#' + this.id).inputmask("Regex", ({"regex":"^[0-9]{0,7}$"}) );
            break;
          case '0000':
            var editableText = $('<input type="text" id="' + this.id + '" name="' + this.id + '" class="' + this.className
                            + ' numerical-input" pattern="[0-9]*" placeholder="0000" step="1" tabindex="' + $(this).attr('tabindex') + '" ' + readonly + '/>');
            editableText.val(divHtml);
            $(this).replaceWith(editableText);
            $('#' + this.id).inputmask("Regex", ({"regex":"^[0-9]{0,4}$"}) );
            break;
          case '000':
            var editableText = $('<input type="text" id="' + this.id + '" name="' + this.id + '" class="' + this.className
                            + ' numerical-input" pattern="[0-9]*" placeholder="000" step="1" tabindex="' + $(this).attr('tabindex') + '" ' + readonly + '/>');
            editableText.val(divHtml);
            $(this).replaceWith(editableText);
            $('#' + this.id).inputmask("Regex", ({"regex":"^[0-9]{0,3}$"}) );
            break;
          case '00':
            var editableText = $('<input type="text" id="' + this.id + '" name="' + this.id + '" class="' + this.className
                            + ' numerical-input" pattern="[0-9]*" placeholder="00" step="1" tabindex="' + $(this).attr('tabindex') + '" ' + readonly + '/>');
            editableText.val(divHtml);
            $(this).replaceWith(editableText);
            $('#' + this.id).inputmask("Regex", ({"regex":"^[0-9]{0,2}$"}) );
            break;
          case '0.00':
            var editableText = $('<input type="text" id="' + this.id + '" name="' + this.id + '" class="' + this.className
                            + ' numerical-input" pattern="[0-9]*" placeholder="0.00" step="0.01" tabindex="' + $(this).attr('tabindex') + '" ' + readonly + '/>');
            editableText.val(divHtml);
            $(this).replaceWith(editableText);
            $('#' + this.id).inputmask("Regex", ({"regex":"^[0-9]{0,5}[.]?[0-9]{0,2}$"}) );
            break;
          case '0.0':
            var editableText = $('<input type="text" id="' + this.id + '" name="' + this.id + '" class="' + this.className
                            + ' numerical-input" pattern="[0-9]*" placeholder="0.0" step="0.1" tabindex="' + $(this).attr('tabindex') + '" ' + readonly + '/>');
            editableText.val(divHtml);
            $(this).replaceWith(editableText);
            $('#' + this.id).inputmask("Regex", ({"regex":"^[0-9]{0,3}[.]?[0-9]{0,1}$"}) );
            break;
          default:
            var editableText = $('<input type="text" id="' + this.id + '" name="' + this.id + '" class="' + this.className
                            + ' numerical-input" pattern="[0-9]*" placeholder="0.0000" step="0.0001" tabindex="' + $(this).attr('tabindex') + '" ' + readonly + '/>');
            editableText.val(divHtml);
            $(this).replaceWith(editableText);
            $('#' + this.id).inputmask("Regex", ({"regex":"^[0-9]{0,3}[.]?[0-9]{0,4}$"}) );
        }

        if ($(this).attr('id') == 'totaliser-end' || $(this).attr('id') == 'totaliser-start') {
          $('#' + $(this).attr('id')).on('keyup', function(e) {
            saveVal($(this), '');
            calculate_totaliser();
          });
        }
        if ($('#'  + $(this).attr('id')).parent().hasClass('vref-factor')) {
          $('#'  + $(this).attr('id')).on('keyup', function(e) {
            calculate_vref(this);
          });
        }
        if ($('#'  + $(this).attr('id')).parent().hasClass('vfdc-factor')) {
          $('#'  + $(this).attr('id')).on('keyup', function(e) {
            calculate_vfdc(this);
          });
        }
        if ($(this).attr('id') == 'vfd15-delivery-compensated-switch') {
          $('#table-test-report-with-switch #vfd15-delivery-compensated-switch').on('keyup', function(e) {
            calculate_ec();
          });
        }

        $('#'  + $(this).attr('id')).on('blur', function(e) {
          saveVal($(this), '');
        });

/*
        $('input[type=text]').on('focus', function(e) {
          //var scrollLeft = (window.pageXOffset !== undefined) ? window.pageXOffset : (document.documentElement || document.body.parentNode || document.body).scrollLeft;
          //alert($('#fuel').scrollLeft() +'; ' + $(this).attr('class'));
//alert($(window).width());
          if ($(window).width() <= 1024) {
            var tmparr = $(this).attr('class').split(/\s+/);
            var pclass = tmparr[1].split('_');
            var cols = 0;
            var col = 0;

            if (parseInt(pclass[1]) > 167) {
              cols = parseInt(pclass[1]) - 167;
              col = cols % 7;
            } else if (parseInt(pclass[1]) > 66) {
              cols = parseInt(pclass[1]) - 65;
              col = cols % 7;
            }
//alert($('#fuel').scrollLeft());
            //alert(pclass[1]+'; col=' + col);
            var obj = $(this);
            if (col == 1 && $('#fuel').scrollLeft() > 0) {
              $('#fuel').scrollLeft( 0 );
            }
            if (col == 2 && $('#fuel').scrollLeft() > 155) {
              $('#fuel').scrollLeft( 0 );
            }
            if (col == 3 && $('#fuel').scrollLeft() > 325) {
              $('#fuel').animate({scrollLeft: 325}, 800);
            }
            if (col == 4 && $('#fuel').scrollLeft() > 470) {
              $('#fuel').animate({scrollLeft: 470}, 800);
            }
            if (col == 5 && $('#fuel').scrollLeft() > 625) {
              $('#fuel').animate({scrollLeft: 625}, 800);
            }
            $(this).focus();
          }
        });
*/
        // Virual numerical keyboard
        if ( ! initial_vb_hooked) {
          $('.QA').on('focus', function(e) {
            e.preventDefault();
            e.stopPropagation();
            hookVirtualKeyboard($(this));
            current_node_id = e.target.id;
          });
          initial_vb_hooked = true;
        } else {
          $('#' + $(this).attr('id')).on('focus', function(e) {
            e.preventDefault();
            e.stopPropagation();
            hookVirtualKeyboard($(this));
            current_node_id = e.target.id;
          });
        }

        current_node_id = editableText.attr('id');

        var evt = document.getElementById(editableText.attr('id'));
        evt.setSelectionRange(editableText.val().length, editableText.val().length);
        evt.focus();

      });

      $('.imask').inputmask("Regex", ({"regex":"^[0-9]{0,3}[.]?[0-9]{0,4}$"}) );

     $('#reference-standards-expiry').datepicker({
        dateFormat: 'dd/mm/yy',
        inline: true,
        changeMonth: true,
        changeYear: true,
        yearRange: '2000:' + ((new Date).getFullYear() + 10),
        onClose: function() {
                  saveVal($(this), '');
                  $(this).removeClass('warning');
                }
      });
      $('#test-date, #verifer_date').datepicker({
        dateFormat: 'dd/mm/yy',
        inline: true,
        changeMonth: true,
        changeYear: true,
        minDate: new Date(2015, 8 - 1, 1),
        maxDate: new Date(),
        defaultDate: new Date(),
        onClose: function() {
                  saveVal($(this), '');
                  $(this).removeClass('warning');
                }
      });

      <?php if ( ! empty($cert_expiry_date)):?>
        $('#reference-standards-expiry').datepicker('setDate', new Date('<?php print $cert_expiry_date_year;?>', '<?php print $cert_expiry_date_month;?>', '<?php print $cert_expiry_date_day;?>'));
      <?php endif;?>

      $('[contenteditable]').on('blur', function(e) {
        saveVal($(this), '');
      });
      $('.date-picker').on('input propertychange paste', function(e) {
        saveVal($(this), '');
      });

      $('.opt-control .QQ').click(function(e){
        var tmparr = $(this).attr('class').split(/\s+/);
        var pclass = tmparr[1].split('_');

        if ($(this).attr('name') == 'test-report-for-lpg-dispensers') {
          slideReportTable($(this).attr('value'));
          saveVal($(this), $(this).attr('value'));
        } else {
          $('.QA_'+pclass[1]).trigger('click');
          saveVal($(this), '');
        }

        $('.' + tmparr[1]).parent().removeClass('warning');

      });

      $('.opt-control2 .QQ').click(function(e) {
        var tmparr = $(this).attr('class').split(/\s+/);
        var pclass = tmparr[1].split('_');
        var $QS = '.QA_' + pclass[1];
        if ($(this).hasClass('clsyes') || $(this).hasClass('clsyesOn')) {
          $($QS + '.clsno').removeClass('clsnoOn clsno').addClass('clsno');
          $($QS + '.clsna').removeClass('clsnaOn clsna').addClass('clsna');
          $($QS + '.clsyes').removeClass('clsyesOn clsyes').addClass('clsyes clsyesOn');
        } else if ($(this).hasClass('clsno') || $(this).hasClass('clsnoOn')) {
          $($QS + '.clsyes').removeClass('clsyesOn clsyes').addClass('clsyes');
          $($QS + '.clsna').removeClass('clsnaOn clsna').addClass('clsna');
          $($QS + '.clsno').removeClass('clsnoOn clsno').addClass('clsno clsnoOn');
        } else if ($(this).hasClass('clsna') || $(this).hasClass('clsnaOn')) {
          $($QS + '.clsno').removeClass('clsnoOn clsno').addClass('clsno');
          $($QS + '.clsyes').removeClass('clsyesOn clsyes').addClass('clsyes');
          $($QS + '.clsna').removeClass('clsnaOn clsna').addClass('clsna clsnaOn');
        }

        if ( ! is_loading) {
          saveVal(tmparr[1], '');
        }

        $('.' + tmparr[1]).parent().parent().removeClass('warning');

      });

      $('.QA_006, .QA_007').click(function(e) {
        //alert(e.target.id);
        var tmparr = $(this).attr('class').split(/\s+/);
        var pclass = tmparr[1].split('_');

        switch (pclass[1]) {
          case '006':
            if ($('.QA_'+pclass[1]).hasClass('clsyesOn')) {
              $('.QA_'+pclass[1]).removeClass('clsyesOn');
              $('#general-characteristics').collapse('hide');
            } else {
              $('.QA_'+pclass[1]).addClass('clsyesOn');
              $('#general-characteristics').collapse('show');
            }
            ;
            break;
          case '007':
            if ($('.QA_'+pclass[1]).hasClass('clsyesOn')) {
              $('.QA_'+pclass[1]).removeClass('clsyesOn');
              $('#inspection').hide();
            } else {
              $('.QA_'+pclass[1]).addClass('clsyesOn');
              $('#inspection').show();
              if ( ! is_loading) {
                $('.QA_008').focus();
              }
            }
        }

        saveVal(tmparr[1], '');
      });

      /**
       * Virtual keyboard's functions
       */

      // Initialise
      $('#btn-keyboard-swap').click(function(e) {
        if ($('#virtual-keyboard').hasClass('hide')) {
          $('#virtual-keyboard').removeClass('hide');
          showCustomVirtualKeyboardInstantly(true);
        } else {
          $('#virtual-keyboard').removeClass('hide').addClass('hide');
          $('.numerical-input').removeAttr('readonly');
          showCustomVirtualKeyboardInstantly(false);
        }
        return false;
      });

      $('#virtual-keyboard li').on('click', function(e) {
        e.preventDefault();
        e.stopPropagation();

        var unicode_symbols_list = [
                                    '&#8677;',
                                    '&#8676;',
                                    '&#9003;'
                                    ];
        var s = $(this).html();
        if (unicode_symbols_list.indexOf(s.toUnicode()) >= 0) {
          switch (s.toUnicode()) {
            case '&#8677;':
              //$('.' + current_node_id).emulateTab();
              vkTraverseTextboxFocus(true);
              break;
            case '&#8676;':
              //$('.' + current_node_id).emulateTab(-1);
              vkTraverseTextboxFocus(false);
              break;
            case '&#9003;':
              var evt = document.getElementById(current_node_id);
              evt.setSelectionRange($('#' + current_node_id).val().length, $('#' + current_node_id).val().length);
              evt.focus();
              var str = $('#' + current_node_id).val();
              str = str.substr(0, str.length - 1);
              $('#' + current_node_id).val(str);
              saveVal($('#' + current_node_id), '');
              checkInnstantCalculation();
           }
        } else {
          switch ($.trim($(this).text())) {
            case 'Hide':
              $('#virtual-keyboard').hide();
              break;
            case 'Keyboard Swap':
              $('#virtual-keyboard').removeClass('hide').addClass('hide');
              showCustomVirtualKeyboardInstantly(false);
              break;
            default:
              var evt = document.getElementById(current_node_id);
              evt.setSelectionRange($('#' + current_node_id).val().length, $('#' + current_node_id).val().length);
              evt.focus();
              var new_digit = $(this).text();
              $('#' + current_node_id).val(
                function(i, orig_text) {
                  return orig_text + new_digit;
                }
              );
              saveVal($('#' + current_node_id), '');
              checkInnstantCalculation();
          }
        }
      });

      $('.QA').on('focus', function(e) {
        e.preventDefault();
        e.stopPropagation();
        current_node_id = e.target.id;
      });

      var slider = $( "<div id='slider'></div>" ).insertAfter( $('#pad-head') ).slider({
        min: 1,
        max: 10,
        range: "min",
        value: 10,
        change: function ( event, ui ) {
          updateSlider(ui.value);
        }
      });

      /**
       * Main
       */
<?php
if ($array_raw['QA_006'] == 'Yes'):?>
  $('.QA_006').removeClass('clsnoOn clsno').addClass('clsnoOn clsno').trigger('click');
<?php else:?>
  $('.QA_006').removeClass('clsyesOn clsyes').addClass('clsyes clsyesOn').trigger('click');
<?php endif;?>

<?php
if ($array_raw['QA_007'] == 'Yes'):?>
  $('.QA_007').removeClass('clsnoOn clsno').addClass('clsnoOn clsno').trigger('click');
<?php else:?>
  $('.QA_007').removeClass('clsyesOn clsyes').addClass('clsyes clsyesOn').trigger('click');
<?php endif;?>

switch ("<?php echo $array_raw['QA_029'];?>") {
  case 'Yes':
  case 'Pass':
    $('.QA_029.clsno').removeClass('clsnoOn clsno').addClass('clsno');
    $('.QA_029.clsna').removeClass('clsnaOn clsna').addClass('clsna');
    $('.QA_029.clsyes').removeClass('clsyesOn clsyes').trigger('click').addClass('clsyes clsyesOn');
    break;
  case 'No':
  case 'Fail':
    $('.QA_029.clsyes').removeClass('clsyesOn clsyes').addClass('clsyes');
    $('.QA_029.clsna').removeClass('clsnaOn clsna').addClass('clsna');
    $('.QA_029.clsno').removeClass('clsnoOn clsno').trigger('click').addClass('clsno clsnoOn');
    break;
  case 'N/A':
    $('.QA_029.clsno').removeClass('clsnoOn clsno').addClass('clsno');
    $('.QA_029.clsyes').removeClass('clsyesOn clsyes').addClass('clsyes');
    $('.QA_029.clsna').removeClass('clsnaOn clsna').trigger('click').addClass('clsna clsnaOn');
}
switch ("<?php echo $array_raw['QA_030'];?>") {
  case 'Yes':
  case 'Pass':
    $('.QA_030.clsno').removeClass('clsnoOn clsno').addClass('clsno');
    $('.QA_030.clsna').removeClass('clsnaOn clsna').addClass('clsna');
    $('.QA_030.clsyes').removeClass('clsyesOn clsyes').trigger('click').addClass('clsyes clsyesOn');
    break;
  case 'No':
  case 'Fail':
    $('.QA_030.clsyes').removeClass('clsyesOn clsyes').addClass('clsyes');
    $('.QA_030.clsna').removeClass('clsnaOn clsna').addClass('clsna');
    $('.QA_030.clsno').removeClass('clsnoOn clsno').trigger('click').addClass('clsno clsnoOn');
    break;
  case 'N/A':
    $('.QA_030.clsno').removeClass('clsnoOn clsno').addClass('clsno');
    $('.QA_030.clsyes').removeClass('clsyesOn clsyes').addClass('clsyes');
    $('.QA_030.clsna').removeClass('clsnaOn clsna').trigger('click').addClass('clsna clsnaOn');
}
switch ("<?php echo $array_raw['QA_031'];?>") {
  case 'Yes':
  case 'Pass':
    $('.QA_031.clsno').removeClass('clsnoOn clsno').addClass('clsno');
    $('.QA_031.clsna').removeClass('clsnaOn clsna').addClass('clsna');
    $('.QA_031.clsyes').removeClass('clsyesOn clsyes').trigger('click').addClass('clsyes clsyesOn');
    break;
  case 'No':
  case 'Fail':
    $('.QA_031.clsyes').removeClass('clsyesOn clsyes').addClass('clsyes');
    $('.QA_031.clsna').removeClass('clsnaOn clsna').addClass('clsna');
    $('.QA_031.clsno').removeClass('clsnoOn clsno').trigger('click').addClass('clsno clsnoOn');
    break;
  case 'N/A':
    $('.QA_031.clsno').removeClass('clsnoOn clsno').addClass('clsno');
    $('.QA_031.clsyes').removeClass('clsyesOn clsyes').addClass('clsyes');
    $('.QA_031.clsna').removeClass('clsnaOn clsna').trigger('click').addClass('clsna clsnaOn');
}
switch ("<?php echo $array_raw['QA_032'];?>") {
  case 'Yes':
  case 'Pass':
    $('.QA_032.clsno').removeClass('clsnoOn clsno').addClass('clsno');
    $('.QA_032.clsna').removeClass('clsnaOn clsna').addClass('clsna');
    $('.QA_032.clsyes').removeClass('clsyesOn clsyes').trigger('click').addClass('clsyes clsyesOn');
    break;
  case 'No':
  case 'Fail':
    $('.QA_032.clsyes').removeClass('clsyesOn clsyes').addClass('clsyes');
    $('.QA_032.clsna').removeClass('clsnaOn clsna').addClass('clsna');
    $('.QA_032.clsno').removeClass('clsnoOn clsno').trigger('click').addClass('clsno clsnoOn');
    break;
  case 'N/A':
    $('.QA_032.clsno').removeClass('clsnoOn clsno').addClass('clsno');
    $('.QA_032.clsyes').removeClass('clsyesOn clsyes').addClass('clsyes');
    $('.QA_032.clsna').removeClass('clsnaOn clsna').trigger('click').addClass('clsna clsnaOn');
}
switch ("<?php echo $array_raw['QA_033'];?>") {
  case 'Yes':
  case 'Pass':
    $('.QA_033.clsno').removeClass('clsnoOn clsno').addClass('clsno');
    $('.QA_033.clsna').removeClass('clsnaOn clsna').addClass('clsna');
    $('.QA_033.clsyes').removeClass('clsyesOn clsyes').trigger('click').addClass('clsyes clsyesOn');
    break;
  case 'No':
  case 'Fail':
    $('.QA_033.clsyes').removeClass('clsyesOn clsyes').addClass('clsyes');
    $('.QA_033.clsna').removeClass('clsnaOn clsna').addClass('clsna');
    $('.QA_033.clsno').removeClass('clsnoOn clsno').trigger('click').addClass('clsno clsnoOn');
    break;
  case 'N/A':
    $('.QA_033.clsno').removeClass('clsnoOn clsno').addClass('clsno');
    $('.QA_033.clsyes').removeClass('clsyesOn clsyes').addClass('clsyes');
    $('.QA_033.clsna').removeClass('clsnaOn clsna').trigger('click').addClass('clsna clsnaOn');
}
switch ("<?php echo $array_raw['QA_034'];?>") {
  case 'Yes':
  case 'Pass':
    $('.QA_034.clsno').removeClass('clsnoOn clsno').addClass('clsno');
    $('.QA_034.clsna').removeClass('clsnaOn clsna').addClass('clsna');
    $('.QA_034.clsyes').removeClass('clsyesOn clsyes').trigger('click').addClass('clsyes clsyesOn');
    break;
  case 'No':
  case 'Fail':
    $('.QA_034.clsyes').removeClass('clsyesOn clsyes').addClass('clsyes');
    $('.QA_034.clsna').removeClass('clsnaOn clsna').addClass('clsna');
    $('.QA_034.clsno').removeClass('clsnoOn clsno').trigger('click').addClass('clsno clsnoOn');
    break;
  case 'N/A':
    $('.QA_034.clsno').removeClass('clsnoOn clsno').addClass('clsno');
    $('.QA_034.clsyes').removeClass('clsyesOn clsyes').addClass('clsyes');
    $('.QA_034.clsna').removeClass('clsnaOn clsna').trigger('click').addClass('clsna clsnaOn');
}
switch ("<?php echo $array_raw['QA_035'];?>") {
  case 'Yes':
  case 'Pass':
    $('.QA_035.clsno').removeClass('clsnoOn clsno').addClass('clsno');
    $('.QA_035.clsna').removeClass('clsnaOn clsna').addClass('clsna');
    $('.QA_035.clsyes').removeClass('clsyesOn clsyes').trigger('click').addClass('clsyes clsyesOn');
    break;
  case 'No':
  case 'Fail':
    $('.QA_035.clsyes').removeClass('clsyesOn clsyes').addClass('clsyes');
    $('.QA_035.clsna').removeClass('clsnaOn clsna').addClass('clsna');
    $('.QA_035.clsno').removeClass('clsnoOn clsno').trigger('click').addClass('clsno clsnoOn');
    break;
  case 'N/A':
    $('.QA_035.clsno').removeClass('clsnoOn clsno').addClass('clsno');
    $('.QA_035.clsyes').removeClass('clsyesOn clsyes').addClass('clsyes');
    $('.QA_035.clsna').removeClass('clsnaOn clsna').trigger('click').addClass('clsna clsnaOn');
}
switch ("<?php echo $array_raw['QA_036'];?>") {
  case 'Yes':
  case 'Pass':
    $('.QA_036.clsno').removeClass('clsnoOn clsno').addClass('clsno');
    $('.QA_036.clsna').removeClass('clsnaOn clsna').addClass('clsna');
    $('.QA_036.clsyes').removeClass('clsyesOn clsyes').trigger('click').addClass('clsyes clsyesOn');
    break;
  case 'No':
  case 'Fail':
    $('.QA_036.clsyes').removeClass('clsyesOn clsyes').addClass('clsyes');
    $('.QA_036.clsna').removeClass('clsnaOn clsna').addClass('clsna');
    $('.QA_036.clsno').removeClass('clsnoOn clsno').trigger('click').addClass('clsno clsnoOn');
    break;
  case 'N/A':
    $('.QA_036.clsno').removeClass('clsnoOn clsno').addClass('clsno');
    $('.QA_036.clsyes').removeClass('clsyesOn clsyes').addClass('clsyes');
    $('.QA_036.clsna').removeClass('clsnaOn clsna').trigger('click').addClass('clsna clsnaOn');
}
switch ("<?php echo $array_raw['QA_037'];?>") {
  case 'Yes':
  case 'Pass':
    $('.QA_037.clsno').removeClass('clsnoOn clsno').addClass('clsno');
    $('.QA_037.clsna').removeClass('clsnaOn clsna').addClass('clsna');
    $('.QA_037.clsyes').removeClass('clsyesOn clsyes').trigger('click').addClass('clsyes clsyesOn');
    break;
  case 'No':
  case 'Fail':
    $('.QA_037.clsyes').removeClass('clsyesOn clsyes').addClass('clsyes');
    $('.QA_037.clsna').removeClass('clsnaOn clsna').addClass('clsna');
    $('.QA_037.clsno').removeClass('clsnoOn clsno').trigger('click').addClass('clsno clsnoOn');
    break;
  case 'N/A':
    $('.QA_037.clsno').removeClass('clsnoOn clsno').addClass('clsno');
    $('.QA_037.clsyes').removeClass('clsyesOn clsyes').addClass('clsyes');
    $('.QA_037.clsna').removeClass('clsnaOn clsna').trigger('click').addClass('clsna clsnaOn');
}
switch ("<?php echo $array_raw['QA_038'];?>") {
  case 'Yes':
  case 'Pass':
    $('.QA_038.clsno').removeClass('clsnoOn clsno').addClass('clsno');
    $('.QA_038.clsna').removeClass('clsnaOn clsna').addClass('clsna');
    $('.QA_038.clsyes').removeClass('clsyesOn clsyes').trigger('click').addClass('clsyes clsyesOn');
    break;
  case 'No':
  case 'Fail':
    $('.QA_038.clsyes').removeClass('clsyesOn clsyes').addClass('clsyes');
    $('.QA_038.clsna').removeClass('clsnaOn clsna').addClass('clsna');
    $('.QA_038.clsno').removeClass('clsnoOn clsno').trigger('click').addClass('clsno clsnoOn');
    break;
  case 'N/A':
    $('.QA_038.clsno').removeClass('clsnoOn clsno').addClass('clsno');
    $('.QA_038.clsyes').removeClass('clsyesOn clsyes').addClass('clsyes');
    $('.QA_038.clsna').removeClass('clsnaOn clsna').trigger('click').addClass('clsna clsnaOn');
}
switch ("<?php echo $array_raw['QA_039'];?>") {
  case 'Yes':
  case 'Pass':
    $('.QA_039.clsno').removeClass('clsnoOn clsno').addClass('clsno');
    $('.QA_039.clsna').removeClass('clsnaOn clsna').addClass('clsna');
    $('.QA_039.clsyes').removeClass('clsyesOn clsyes').trigger('click').addClass('clsyes clsyesOn');
    break;
  case 'No':
  case 'Fail':
    $('.QA_039.clsyes').removeClass('clsyesOn clsyes').addClass('clsyes');
    $('.QA_039.clsna').removeClass('clsnaOn clsna').addClass('clsna');
    $('.QA_039.clsno').removeClass('clsnoOn clsno').trigger('click').addClass('clsno clsnoOn');
    break;
  case 'N/A':
    $('.QA_039.clsno').removeClass('clsnoOn clsno').addClass('clsno');
    $('.QA_039.clsyes').removeClass('clsyesOn clsyes').addClass('clsyes');
    $('.QA_039.clsna').removeClass('clsnaOn clsna').trigger('click').addClass('clsna clsnaOn');
}
switch ("<?php echo $array_raw['QA_040'];?>") {
  case 'Yes':
  case 'Pass':
    $('.QA_040.clsno').removeClass('clsnoOn clsno').addClass('clsno');
    $('.QA_040.clsna').removeClass('clsnaOn clsna').addClass('clsna');
    $('.QA_040.clsyes').removeClass('clsyesOn clsyes').trigger('click').addClass('clsyes clsyesOn');
    break;
  case 'No':
  case 'Fail':
    $('.QA_040.clsyes').removeClass('clsyesOn clsyes').addClass('clsyes');
    $('.QA_040.clsna').removeClass('clsnaOn clsna').addClass('clsna');
    $('.QA_040.clsno').removeClass('clsnoOn clsno').trigger('click').addClass('clsno clsnoOn');
    break;
  case 'N/A':
    $('.QA_040.clsno').removeClass('clsnoOn clsno').addClass('clsno');
    $('.QA_040.clsyes').removeClass('clsyesOn clsyes').addClass('clsyes');
    $('.QA_040.clsna').removeClass('clsnaOn clsna').trigger('click').addClass('clsna clsnaOn');
}
switch ("<?php echo $array_raw['QA_041'];?>") {
  case 'Yes':
  case 'Pass':
    $('.QA_041.clsno').removeClass('clsnoOn clsno').addClass('clsno');
    $('.QA_041.clsna').removeClass('clsnaOn clsna').addClass('clsna');
    $('.QA_041.clsyes').removeClass('clsyesOn clsyes').trigger('click').addClass('clsyes clsyesOn');
    break;
  case 'No':
  case 'Fail':
    $('.QA_041.clsyes').removeClass('clsyesOn clsyes').addClass('clsyes');
    $('.QA_041.clsna').removeClass('clsnaOn clsna').addClass('clsna');
    $('.QA_041.clsno').removeClass('clsnoOn clsno').trigger('click').addClass('clsno clsnoOn');
    break;
  case 'N/A':
    $('.QA_041.clsno').removeClass('clsnoOn clsno').addClass('clsno');
    $('.QA_041.clsyes').removeClass('clsyesOn clsyes').addClass('clsyes');
    $('.QA_041.clsna').removeClass('clsnaOn clsna').trigger('click').addClass('clsna clsnaOn');
}
switch ("<?php echo $array_raw['QA_042'];?>") {
  case 'Yes':
  case 'Pass':
    $('.QA_042.clsno').removeClass('clsnoOn clsno').addClass('clsno');
    $('.QA_042.clsna').removeClass('clsnaOn clsna').addClass('clsna');
    $('.QA_042.clsyes').removeClass('clsyesOn clsyes').trigger('click').addClass('clsyes clsyesOn');
    break;
  case 'No':
  case 'Fail':
    $('.QA_042.clsyes').removeClass('clsyesOn clsyes').addClass('clsyes');
    $('.QA_042.clsna').removeClass('clsnaOn clsna').addClass('clsna');
    $('.QA_042.clsno').removeClass('clsnoOn clsno').trigger('click').addClass('clsno clsnoOn');
    break;
  case 'N/A':
    $('.QA_042.clsno').removeClass('clsnoOn clsno').addClass('clsno');
    $('.QA_042.clsyes').removeClass('clsyesOn clsyes').addClass('clsyes');
    $('.QA_042.clsna').removeClass('clsnaOn clsna').trigger('click').addClass('clsna clsnaOn');
}
switch ("<?php echo $array_raw['QA_043'];?>") {
  case 'Yes':
  case 'Pass':
    $('.QA_043.clsno').removeClass('clsnoOn clsno').addClass('clsno');
    $('.QA_043.clsna').removeClass('clsnaOn clsna').addClass('clsna');
    $('.QA_043.clsyes').removeClass('clsyesOn clsyes').trigger('click').addClass('clsyes clsyesOn');
    break;
  case 'No':
  case 'Fail':
    $('.QA_043.clsyes').removeClass('clsyesOn clsyes').addClass('clsyes');
    $('.QA_043.clsna').removeClass('clsnaOn clsna').addClass('clsna');
    $('.QA_043.clsno').removeClass('clsnoOn clsno').trigger('click').addClass('clsno clsnoOn');
    break;
  case 'N/A':
    $('.QA_043.clsno').removeClass('clsnoOn clsno').addClass('clsno');
    $('.QA_043.clsyes').removeClass('clsyesOn clsyes').addClass('clsyes');
    $('.QA_043.clsna').removeClass('clsnaOn clsna').trigger('click').addClass('clsna clsnaOn');
}
switch ("<?php echo $array_raw['QA_044'];?>") {
  case 'Yes':
  case 'Pass':
    $('.QA_044.clsno').removeClass('clsnoOn clsno').addClass('clsno');
    $('.QA_044.clsna').removeClass('clsnaOn clsna').addClass('clsna');
    $('.QA_044.clsyes').removeClass('clsyesOn clsyes').trigger('click').addClass('clsyes clsyesOn');
    break;
  case 'No':
  case 'Fail':
    $('.QA_044.clsyes').removeClass('clsyesOn clsyes').addClass('clsyes');
    $('.QA_044.clsna').removeClass('clsnaOn clsna').addClass('clsna');
    $('.QA_044.clsno').removeClass('clsnoOn clsno').trigger('click').addClass('clsno clsnoOn');
    break;
  case 'N/A':
    $('.QA_044.clsno').removeClass('clsnoOn clsno').addClass('clsno');
    $('.QA_044.clsyes').removeClass('clsyesOn clsyes').addClass('clsyes');
    $('.QA_044.clsna').removeClass('clsnaOn clsna').trigger('click').addClass('clsna clsnaOn');
}
switch ("<?php echo $array_raw['QA_045'];?>") {
  case 'Yes':
  case 'Pass':
    $('.QA_045.clsno').removeClass('clsnoOn clsno').addClass('clsno');
    $('.QA_045.clsna').removeClass('clsnaOn clsna').addClass('clsna');
    $('.QA_045.clsyes').removeClass('clsyesOn clsyes').trigger('click').addClass('clsyes clsyesOn');
    break;
  case 'No':
  case 'Fail':
    $('.QA_045.clsyes').removeClass('clsyesOn clsyes').addClass('clsyes');
    $('.QA_045.clsna').removeClass('clsnaOn clsna').addClass('clsna');
    $('.QA_045.clsno').removeClass('clsnoOn clsno').trigger('click').addClass('clsno clsnoOn');
    break;
  case 'N/A':
    $('.QA_045.clsno').removeClass('clsnoOn clsno').addClass('clsno');
    $('.QA_045.clsyes').removeClass('clsyesOn clsyes').addClass('clsyes');
    $('.QA_045.clsna').removeClass('clsnaOn clsna').trigger('click').addClass('clsna clsnaOn');
}
switch ("<?php echo $array_raw['QA_046'];?>") {
  case 'Yes':
  case 'Pass':
    $('.QA_046.clsno').removeClass('clsnoOn clsno').addClass('clsno');
    $('.QA_046.clsyes').removeClass('clsyesOn clsyes').trigger('click').addClass('clsyes clsyesOn');
    break;
  case 'No':
  case 'Fail':
    $('.QA_046.clsyes').removeClass('clsyesOn clsyes').addClass('clsyes');
    $('.QA_046.clsno').removeClass('clsnoOn clsno').trigger('click').addClass('clsno clsnoOn');
}
switch ("<?php echo $array_raw['QA_047'];?>") {
  case 'Yes':
  case 'Pass':
    $('.QA_047.clsno').removeClass('clsnoOn clsno').addClass('clsno');
    $('.QA_047.clsyes').removeClass('clsyesOn clsyes').trigger('click').addClass('clsyes clsyesOn');
    break;
  case 'No':
  case 'Fail':
    $('.QA_047.clsyes').removeClass('clsyesOn clsyes').addClass('clsyes');
    $('.QA_047.clsno').removeClass('clsnoOn clsno').trigger('click').addClass('clsno clsnoOn');
}
switch ("<?php echo $array_raw['QA_048'];?>") {
  case 'Yes':
  case 'Pass':
    $('.QA_048.clsno').removeClass('clsnoOn clsno').addClass('clsno');
    $('.QA_048.clsyes').removeClass('clsyesOn clsyes').trigger('click').addClass('clsyes clsyesOn');
    break;
  case 'No':
  case 'Fail':
    $('.QA_048.clsyes').removeClass('clsyesOn clsyes').addClass('clsyes');
    $('.QA_048.clsno').removeClass('clsnoOn clsno').trigger('click').addClass('clsno clsnoOn');
}
switch ("<?php echo $array_raw['QA_049'];?>") {
  case 'Yes':
  case 'Pass':
    $('.QA_049.clsno').removeClass('clsnoOn clsno').addClass('clsno');
    $('.QA_049.clsyes').removeClass('clsyesOn clsyes').trigger('click').addClass('clsyes clsyesOn');
    break;
  case 'No':
  case 'Fail':
    $('.QA_049.clsyes').removeClass('clsyesOn clsyes').addClass('clsyes');
    $('.QA_049.clsno').removeClass('clsnoOn clsno').trigger('click').addClass('clsno clsnoOn');
}
switch ("<?php echo $array_raw['QA_050'];?>") {
  case 'Yes':
  case 'Pass':
    $('.QA_050.clsno').removeClass('clsnoOn clsno').addClass('clsno');
    $('.QA_050.clsyes').removeClass('clsyesOn clsyes').trigger('click').addClass('clsyes clsyesOn');
    break;
  case 'No':
  case 'Fail':
    $('.QA_050.clsyes').removeClass('clsyesOn clsyes').addClass('clsyes');
    $('.QA_050.clsno').removeClass('clsnoOn clsno').trigger('click').addClass('clsno clsnoOn');
}
switch ("<?php echo $array_raw['QA_051'];?>") {
  case 'Yes':
  case 'Pass':
    $('.QA_051.clsno').removeClass('clsnoOn clsno').addClass('clsno');
    $('.QA_051.clsna').removeClass('clsnaOn clsna').addClass('clsna');
    $('.QA_051.clsyes').removeClass('clsyesOn clsyes').trigger('click').addClass('clsyes clsyesOn');
    break;
  case 'No':
  case 'Fail':
    $('.QA_051.clsyes').removeClass('clsyesOn clsyes').addClass('clsyes');
    $('.QA_051.clsna').removeClass('clsnaOn clsna').addClass('clsna');
    $('.QA_051.clsno').removeClass('clsnoOn clsno').trigger('click').addClass('clsno clsnoOn');
    break;
  case 'N/A':
    $('.QA_051.clsno').removeClass('clsnoOn clsno').addClass('clsno');
    $('.QA_051.clsyes').removeClass('clsyesOn clsyes').addClass('clsyes');
    $('.QA_051.clsna').removeClass('clsnaOn clsna').trigger('click').addClass('clsna clsnaOn');
}

//slideReportTable("<?php // echo $array_raw['QA_052'];?>");
if ("<?php echo $array_raw['QA_052'];?>" == 'with-switch') {
  $('.QA_test-report-for-lpg-dispensers-yes').trigger('click');
} else if ("<?php echo $array_raw['QA_052'];?>" == 'without-switch') {
  $('.QA_test-report-for-lpg-dispensers-no').trigger('click');
}

/* with switch */
$('.QA_056').val("<?php echo $array_raw['QA_056'];?>");
$('.QA_057').val("<?php echo $array_raw['QA_057'];?>");
$('.QA_058').val("<?php echo $array_raw['QA_058'];?>");

$('.QA_059').text("<?php echo $array_raw['QA_059'];?>");
$('.QA_059').val("<?php echo $array_raw['QA_059'];?>");

$('.QA_060').val("<?php echo $array_raw['QA_060'];?>");
$('.QA_061').val("<?php echo $array_raw['QA_061'];?>");
$('.QA_062').val("<?php echo $array_raw['QA_062'];?>");
$('.QA_063').val("<?php echo $array_raw['QA_063'];?>");
$('.QA_064').val("<?php echo $array_raw['QA_064'];?>");
$('.QA_065').val("<?php echo $array_raw['QA_065'];?>");

$('.QA_066').val("<?php echo $array_raw['QA_066'];?>");
$('.QA_067').val("<?php echo $array_raw['QA_067'];?>");
$('.QA_068').val("<?php echo $array_raw['QA_068'];?>");
$('.QA_069').val("<?php echo $array_raw['QA_069'];?>");
$('.QA_070').val("<?php echo $array_raw['QA_070'];?>");
$('.QA_071').val("<?php echo $array_raw['QA_071'];?>");
$('.QA_072').val("<?php echo $array_raw['QA_072'];?>");
$('.QA_073').val("<?php echo $array_raw['QA_073'];?>");
$('.QA_074').val("<?php echo $array_raw['QA_074'];?>");
$('.QA_075').val("<?php echo $array_raw['QA_075'];?>");
$('.QA_076').val("<?php echo $array_raw['QA_076'];?>");
$('.QA_077').val("<?php echo $array_raw['QA_077'];?>");
$('.QA_078').val("<?php echo $array_raw['QA_078'];?>");
$('.QA_079').val("<?php echo $array_raw['QA_079'];?>");
$('.QA_080').val("<?php echo $array_raw['QA_080'];?>");
$('.QA_081').val("<?php echo $array_raw['QA_081'];?>");
$('.QA_082').val("<?php echo $array_raw['QA_082'];?>");
$('.QA_083').val("<?php echo $array_raw['QA_083'];?>");
$('.QA_084').val("<?php echo $array_raw['QA_084'];?>");
$('.QA_085').val("<?php echo $array_raw['QA_085'];?>");
$('.QA_086').val("<?php echo $array_raw['QA_086'];?>");
$('.QA_087').val("<?php echo $array_raw['QA_087'];?>");
$('.QA_088').val("<?php echo $array_raw['QA_088'];?>");
$('.QA_089').val("<?php echo $array_raw['QA_089'];?>");
$('.QA_090').val("<?php echo $array_raw['QA_090'];?>");
$('.QA_091').val("<?php echo $array_raw['QA_091'];?>");
$('.QA_092').val("<?php echo $array_raw['QA_092'];?>");
$('.QA_093').val("<?php echo $array_raw['QA_093'];?>");
$('.QA_094').val("<?php echo $array_raw['QA_094'];?>");
$('.QA_095').val("<?php echo $array_raw['QA_095'];?>");
$('.QA_096').val("<?php echo $array_raw['QA_096'];?>");
$('.QA_097').val("<?php echo $array_raw['QA_097'];?>");
$('.QA_098').val("<?php echo $array_raw['QA_098'];?>");
$('.QA_099').val("<?php echo $array_raw['QA_099'];?>");
$('.QA_100').val("<?php echo $array_raw['QA_100'];?>");
$('.QA_101').val("<?php echo $array_raw['QA_101'];?>");
$('.QA_102').val("<?php echo $array_raw['QA_102'];?>");
$('.QA_103').val("<?php echo $array_raw['QA_103'];?>");
$('.QA_104').val("<?php echo $array_raw['QA_104'];?>");
$('.QA_105').val("<?php echo $array_raw['QA_105'];?>");
$('.QA_106').val("<?php echo $array_raw['QA_106'];?>");
$('.QA_107').val("<?php echo $array_raw['QA_107'];?>");
$('.QA_108').val("<?php echo $array_raw['QA_108'];?>");
$('.QA_109').val("<?php echo $array_raw['QA_109'];?>");
$('.QA_110').val("<?php echo $array_raw['QA_110'];?>");
$('.QA_111').val("<?php echo $array_raw['QA_111'];?>");
$('.QA_112').val("<?php echo $array_raw['QA_112'];?>");
$('.QA_113').val("<?php echo $array_raw['QA_113'];?>");

$('.QA_114').text("<?php echo $array_raw['QA_114'];?>");
$('.QA_114').val("<?php echo $array_raw['QA_114'];?>");
$('.QA_115').text("<?php echo $array_raw['QA_115'];?>");
$('.QA_115').val("<?php echo $array_raw['QA_115'];?>");
$('.QA_116').text("<?php echo $array_raw['QA_116'];?>");
$('.QA_116').val("<?php echo $array_raw['QA_116'];?>");
$('.QA_117').text("<?php echo $array_raw['QA_117'];?>");
$('.QA_117').val("<?php echo $array_raw['QA_117'];?>");
$('.QA_118').text("<?php echo $array_raw['QA_118'];?>");
$('.QA_118').val("<?php echo $array_raw['QA_118'];?>");
$('.QA_119').text("<?php echo $array_raw['QA_119'];?>");
$('.QA_119').val("<?php echo $array_raw['QA_119'];?>");

$('.QA_120').val("<?php echo $array_raw['QA_120'];?>");
$('.QA_121').val("<?php echo $array_raw['QA_121'];?>");
$('.QA_122').val("<?php echo $array_raw['QA_122'];?>");
$('.QA_123').val("<?php echo $array_raw['QA_123'];?>");
$('.QA_124').val("<?php echo $array_raw['QA_124'];?>");
$('.QA_125').val("<?php echo $array_raw['QA_125'];?>");
$('.QA_126').val("<?php echo $array_raw['QA_126'];?>");
$('.QA_127').val("<?php echo $array_raw['QA_127'];?>");
$('.QA_128').val("<?php echo $array_raw['QA_128'];?>");
$('.QA_129').val("<?php echo $array_raw['QA_129'];?>");
$('.QA_130').val("<?php echo $array_raw['QA_130'];?>");
$('.QA_131').val("<?php echo $array_raw['QA_131'];?>");
$('.QA_132').val("<?php echo $array_raw['QA_132'];?>");
$('.QA_133').val("<?php echo $array_raw['QA_133'];?>");
$('.QA_134').val("<?php echo $array_raw['QA_134'];?>");
$('.QA_135').val("<?php echo $array_raw['QA_135'];?>");
$('.QA_136').val("<?php echo $array_raw['QA_136'];?>");
$('.QA_137').val("<?php echo $array_raw['QA_137'];?>");
$('.QA_138').val("<?php echo $array_raw['QA_138'];?>");
$('.QA_139').val("<?php echo $array_raw['QA_139'];?>");
$('.QA_140').val("<?php echo $array_raw['QA_140'];?>");
$('.QA_141').val("<?php echo $array_raw['QA_141'];?>");
$('.QA_142').val("<?php echo $array_raw['QA_142'];?>");
$('.QA_143').val("<?php echo $array_raw['QA_143'];?>");
$('.QA_144').val("<?php echo $array_raw['QA_144'];?>");
$('.QA_145').val("<?php echo $array_raw['QA_145'];?>");
$('.QA_146').val("<?php echo $array_raw['QA_146'];?>");
$('.QA_147').val("<?php echo $array_raw['QA_147'];?>");
$('.QA_148').val("<?php echo $array_raw['QA_148'];?>");

$('.QA_149').text("<?php echo $array_raw['QA_149'];?>");
$('.QA_149').val("<?php echo $array_raw['QA_149'];?>");
$('.QA_150').text("<?php echo $array_raw['QA_150'];?>");
$('.QA_150').val("<?php echo $array_raw['QA_150'];?>");
$('.QA_151').text("<?php echo $array_raw['QA_151'];?>");
$('.QA_151').val("<?php echo $array_raw['QA_151'];?>");
$('.QA_152').text("<?php echo $array_raw['QA_152'];?>");
$('.QA_152').val("<?php echo $array_raw['QA_152'];?>");
$('.QA_153').text("<?php echo $array_raw['QA_153'];?>");
$('.QA_153').val("<?php echo $array_raw['QA_153'];?>");
$('.QA_154').text("<?php echo $array_raw['QA_154'];?>");
$('.QA_154').val("<?php echo $array_raw['QA_154'];?>");
$('.QA_155').text("<?php echo $array_raw['QA_155'];?>");
$('.QA_155').val("<?php echo $array_raw['QA_155'];?>");
$('.QA_156').text("<?php echo $array_raw['QA_156'];?>");
$('.QA_156').val("<?php echo $array_raw['QA_156'];?>");
$('.QA_157').text("<?php echo $array_raw['QA_157'];?>");
$('.QA_157').val("<?php echo $array_raw['QA_157'];?>");
$('.QA_158').text("<?php echo $array_raw['QA_158'];?>");
$('.QA_158').val("<?php echo $array_raw['QA_158'];?>");
$('.QA_159').text("<?php echo $array_raw['QA_159'];?>");
$('.QA_159').val("<?php echo $array_raw['QA_159'];?>");
$('.QA_160').text("<?php echo $array_raw['QA_160'];?>");
$('.QA_160').val("<?php echo $array_raw['QA_160'];?>");
$('.QA_161').text("<?php echo $array_raw['QA_161'];?>");
$('.QA_161').val("<?php echo $array_raw['QA_161'];?>");
$('.QA_162').text("<?php echo $array_raw['QA_162'];?>");
$('.QA_162').val("<?php echo $array_raw['QA_162'];?>");

/* without switch */
$('.QA_168').val("<?php echo $array_raw['QA_168'];?>");
$('.QA_169').val("<?php echo $array_raw['QA_169'];?>");
$('.QA_170').val("<?php echo $array_raw['QA_170'];?>");
$('.QA_171').val("<?php echo $array_raw['QA_171'];?>");
$('.QA_172').val("<?php echo $array_raw['QA_172'];?>");
$('.QA_173').val("<?php echo $array_raw['QA_173'];?>");
$('.QA_174').val("<?php echo $array_raw['QA_174'];?>");
$('.QA_175').val("<?php echo $array_raw['QA_175'];?>");
$('.QA_176').val("<?php echo $array_raw['QA_176'];?>");
$('.QA_177').val("<?php echo $array_raw['QA_177'];?>");
$('.QA_178').val("<?php echo $array_raw['QA_178'];?>");
$('.QA_179').val("<?php echo $array_raw['QA_179'];?>");
$('.QA_180').val("<?php echo $array_raw['QA_180'];?>");
$('.QA_181').val("<?php echo $array_raw['QA_181'];?>");
$('.QA_182').val("<?php echo $array_raw['QA_182'];?>");
$('.QA_183').val("<?php echo $array_raw['QA_183'];?>");
$('.QA_184').val("<?php echo $array_raw['QA_184'];?>");
$('.QA_185').val("<?php echo $array_raw['QA_185'];?>");
$('.QA_186').val("<?php echo $array_raw['QA_186'];?>");
$('.QA_187').val("<?php echo $array_raw['QA_187'];?>");
$('.QA_188').val("<?php echo $array_raw['QA_188'];?>");
$('.QA_189').val("<?php echo $array_raw['QA_189'];?>");
$('.QA_190').val("<?php echo $array_raw['QA_190'];?>");
$('.QA_191').val("<?php echo $array_raw['QA_191'];?>");
$('.QA_192').val("<?php echo $array_raw['QA_192'];?>");
$('.QA_193').val("<?php echo $array_raw['QA_193'];?>");
$('.QA_194').val("<?php echo $array_raw['QA_194'];?>");
$('.QA_195').val("<?php echo $array_raw['QA_195'];?>");
$('.QA_196').val("<?php echo $array_raw['QA_196'];?>");
$('.QA_197').val("<?php echo $array_raw['QA_197'];?>");
$('.QA_198').val("<?php echo $array_raw['QA_198'];?>");
$('.QA_199').val("<?php echo $array_raw['QA_199'];?>");
$('.QA_200').val("<?php echo $array_raw['QA_200'];?>");
$('.QA_201').val("<?php echo $array_raw['QA_201'];?>");
$('.QA_202').val("<?php echo $array_raw['QA_202'];?>");
$('.QA_203').val("<?php echo $array_raw['QA_203'];?>");
$('.QA_204').val("<?php echo $array_raw['QA_204'];?>");
$('.QA_205').val("<?php echo $array_raw['QA_205'];?>");
$('.QA_206').val("<?php echo $array_raw['QA_206'];?>");
$('.QA_207').val("<?php echo $array_raw['QA_207'];?>");
$('.QA_208').val("<?php echo $array_raw['QA_208'];?>");
$('.QA_209').val("<?php echo $array_raw['QA_209'];?>");
$('.QA_210').val("<?php echo $array_raw['QA_210'];?>");
$('.QA_211').val("<?php echo $array_raw['QA_211'];?>");
$('.QA_212').val("<?php echo $array_raw['QA_212'];?>");
$('.QA_213').val("<?php echo $array_raw['QA_213'];?>");
$('.QA_214').val("<?php echo $array_raw['QA_214'];?>");
$('.QA_215').val("<?php echo $array_raw['QA_215'];?>");
$('.QA_216').val("<?php echo $array_raw['QA_216'];?>");
$('.QA_217').val("<?php echo $array_raw['QA_217'];?>");
$('.QA_218').val("<?php echo $array_raw['QA_218'];?>");
$('.QA_219').val("<?php echo $array_raw['QA_219'];?>");
$('.QA_220').val("<?php echo $array_raw['QA_220'];?>");
$('.QA_221').val("<?php echo $array_raw['QA_221'];?>");
$('.QA_222').val("<?php echo $array_raw['QA_222'];?>");
$('.QA_223').val("<?php echo $array_raw['QA_223'];?>");

$('.QA_224').text("<?php echo $array_raw['QA_224'];?>");
$('.QA_224').val("<?php echo $array_raw['QA_224'];?>");
$('.QA_225').text("<?php echo $array_raw['QA_225'];?>");
$('.QA_225').val("<?php echo $array_raw['QA_225'];?>");
$('.QA_226').text("<?php echo $array_raw['QA_226'];?>");
$('.QA_226').val("<?php echo $array_raw['QA_226'];?>");
$('.QA_227').text("<?php echo $array_raw['QA_227'];?>");
$('.QA_227').val("<?php echo $array_raw['QA_227'];?>");
$('.QA_228').text("<?php echo $array_raw['QA_228'];?>");
$('.QA_228').val("<?php echo $array_raw['QA_228'];?>");
$('.QA_229').text("<?php echo $array_raw['QA_229'];?>");
$('.QA_229').val("<?php echo $array_raw['QA_229'];?>");
$('.QA_230').text("<?php echo $array_raw['QA_230'];?>");
$('.QA_230').val("<?php echo $array_raw['QA_230'];?>");

$('.QA_231').val("<?php echo $array_raw['QA_231'];?>");
$('.QA_232').val("<?php echo $array_raw['QA_232'];?>");
$('.QA_233').val("<?php echo $array_raw['QA_233'];?>");
$('.QA_234').val("<?php echo $array_raw['QA_234'];?>");
$('.QA_235').val("<?php echo $array_raw['QA_235'];?>");
$('.QA_236').val("<?php echo $array_raw['QA_236'];?>");
$('.QA_237').val("<?php echo $array_raw['QA_237'];?>");
$('.QA_238').val("<?php echo $array_raw['QA_238'];?>");
$('.QA_239').val("<?php echo $array_raw['QA_239'];?>");
$('.QA_240').val("<?php echo $array_raw['QA_240'];?>");
$('.QA_241').val("<?php echo $array_raw['QA_241'];?>");
$('.QA_242').val("<?php echo $array_raw['QA_242'];?>");

$('.QA_243').val("<?php echo $array_raw['QA_243'];?>");
$('.QA_244').val("<?php echo $array_raw['QA_244'];?>");
$('.QA_245').val("<?php echo $array_raw['QA_245'];?>");
$('.QA_246').val("<?php echo $array_raw['QA_246'];?>");
$('.QA_247').val("<?php echo $array_raw['QA_247'];?>");
$('.QA_248').val("<?php echo $array_raw['QA_248'];?>");
$('.QA_249').val("<?php echo $array_raw['QA_249'];?>");
$('.QA_250').val("<?php echo $array_raw['QA_250'];?>");
$('.QA_251').val("<?php echo $array_raw['QA_251'];?>");
$('.QA_252').val("<?php echo $array_raw['QA_252'];?>");
$('.QA_253').val("<?php echo $array_raw['QA_253'];?>");
$('.QA_254').val("<?php echo $array_raw['QA_254'];?>");

$('.QA_255').text("<?php echo $array_raw['QA_255'];?>");
$('.QA_255').val("<?php echo $array_raw['QA_255'];?>");
$('.QA_256').text("<?php echo $array_raw['QA_256'];?>");
$('.QA_256').val("<?php echo $array_raw['QA_256'];?>");
$('.QA_257').text("<?php echo $array_raw['QA_257'];?>");
$('.QA_257').val("<?php echo $array_raw['QA_257'];?>");

$('.QA_258').val("<?php echo $array_raw['QA_258'];?>");

$('.QA_259').text("<?php echo $array_raw['QA_259'];?>");
$('.QA_259').val("<?php echo $array_raw['QA_259'];?>");
$('.QA_260').text("<?php echo $array_raw['QA_260'];?>");
$('.QA_260').val("<?php echo $array_raw['QA_260'];?>");
$('.QA_261').text("<?php echo $array_raw['QA_261'];?>");
$('.QA_261').val("<?php echo $array_raw['QA_261'];?>");
$('.QA_262').text("<?php echo $array_raw['QA_262'];?>");
$('.QA_262').val("<?php echo $array_raw['QA_262'];?>");
$('.QA_263').text("<?php echo $array_raw['QA_263'];?>");
$('.QA_263').val("<?php echo $array_raw['QA_263'];?>");
$('.QA_264').text("<?php echo $array_raw['QA_264'];?>");
$('.QA_264').val("<?php echo $array_raw['QA_264'];?>");
$('.QA_265').text("<?php echo $array_raw['QA_265'];?>");
$('.QA_265').val("<?php echo $array_raw['QA_265'];?>");
$('.QA_266').text("<?php echo $array_raw['QA_266'];?>");
$('.QA_266').val("<?php echo $array_raw['QA_266'];?>");
$('.QA_267').text("<?php echo $array_raw['QA_267'];?>");
$('.QA_267').val("<?php echo $array_raw['QA_267'];?>");
$('.QA_268').text("<?php echo $array_raw['QA_268'];?>");
$('.QA_268').val("<?php echo $array_raw['QA_268'];?>");
$('.QA_269').text("<?php echo $array_raw['QA_269'];?>");
$('.QA_269').val("<?php echo $array_raw['QA_269'];?>");
$('.QA_270').text("<?php echo $array_raw['QA_270'];?>");
$('.QA_270').val("<?php echo $array_raw['QA_270'];?>");
$('.QA_271').text("<?php echo $array_raw['QA_271'];?>");
$('.QA_271').val("<?php echo $array_raw['QA_271'];?>");
$('.QA_272').text("<?php echo $array_raw['QA_272'];?>");
$('.QA_272').val("<?php echo $array_raw['QA_272'];?>");
$('.QA_273').text("<?php echo $array_raw['QA_273'];?>");
$('.QA_273').val("<?php echo $array_raw['QA_273'];?>");
$('.QA_274').text("<?php echo $array_raw['QA_274'];?>");
$('.QA_274').val("<?php echo $array_raw['QA_274'];?>");
$('.QA_275').text("<?php echo $array_raw['QA_275'];?>");
$('.QA_275').val("<?php echo $array_raw['QA_275'];?>");

clearSignature();
var rawsign = $('#verifer-sign').jSignature('getData');
if ("<?php echo $array_raw['QA_276'];?>" != rawsign) {
  try {
    $('#verifer-sign').jSignature('importData', "<?php echo $array_raw['QA_276'];?>");
    $('#verifer-sign-dummy').val('1');
  } catch(err) {
    // Do nothing
  }
}

$('.QA').each(function(index) {
  if ($(this).attr('data-ph') != '' && $(this).hasClass('imask')) {
    $(this).attr('data-ph', '');
    $(this).val($(this).val());
    $(this).html($(this).val());
  }
});
//      if (obtainedGPS()) {
        hostReachable();
//        actno = getQueryStrByName('actno');
        inc = getQueryStrByName('inc');
        if (inc != '') {
          document.title = 'LPG ' + inc;
        }
//        if (actno) {
          if (actnoExistsOnLocalStorage()) {
//            $('.QA_002').val(actno);
//            $('.IncrementNumber').val(inc);
            saveVal($('.QA_002'), '');
            saveVal($('.IncrementNumber'), '');
            if ($('.QA_279').text().trim() != '') {
              saveVal($('.QA_279'), '');
            }
            saveVal($('.QA_280'), '');
//            loadVals();
            saveVals();
            $('.QA_003').focus();
          } else {
            if ($('.QA_279').text().trim() == '') {
              invalidGVRJobNumberOccurred();
            } else {
              addJob();
              saveVals();
              $('.QA_003').focus();
            }
          }
//        } else {
//          showDialogbox();
//          if ($('.QA_279').text().trim() != '') {
//            $('.QA_003').focus();
//          }
//        }

//      } else {
//        disableControls();
//      }
    });

    /**
     * Function to hook up signature panel to library
     */
    function hookupSignature() {
      // Signature canvas
      $('#verifer-sign').jSignature({
        'background-color': 'transparent',
        'decor-color': 'transparent',
        'width': '480',
        'height':'140'
      });
      $('#verifer-sign').jSignature('reset');
      $('#btn-clear-signature').click(function(e) {
        clearSignature();
      });
      $('.jSignature').each(function( index ) {
        this.addEventListener('mousedown', doMouseDown, false);
      });
      $('.jSignature').each(function( index ) {
        this.addEventListener('touchstart', doMouseDown, false);
      });
    }

    function doMouseDown() {
      $('#verifer-sign-dummy').val('1');
      $('#verifer-sign').removeClass('warning');
    }

    /**
     * Function get query string by name
     */
    function getQueryStrByName(name) {
      url = window.location.href;
      name = name.replace(/[\[\]]/g, "\\$&");
      var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)", "i"), results = regex.exec(url);
      if (!results) return '';
      if (!results[2]) return '';
      return decodeURIComponent(results[2].replace(/\+/g, " "));
    }

    /**
     * Function to get geograhpic location
     */
    function getLocation() {
      if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(showPosition);
        return true;
      } else {
        //alert('Sorry! System cannot proceed without the location information.');
        //return false;
        return true;
      }
    }

    function showPosition(position) {
      var latit=position.coords.latitude;
      var longtit=position.coords.longitude;
      $('#ex_loc').val(latit + '|' + longtit);
      //if ($('#ex_loc').val() == '') {
      //  alert('Sorry! System cannot proceed without the location information.');
      //}
      return ;
    }

    /**
     * Function check and get geographic location
     */
    function obtainedGPS() {
      // ********** Note: Glenn (The CEO of Verified P/L) requests that no GPS will not block the online form at present therefore the return flag is always true. **********
      var b_obtained_gps = true;
      var gps = getQueryStrByName('gps');
      if (gps) {
        var array_position = gps.split('|');
        if (array_position.length > 1) {
          var latit = array_position[0];
          var longtit = array_position[1];
          $('#ex_loc').val(latit + '|' + longtit);
        }
        //if ($('#ex_loc').val() == '') {
        //  alert('Sorry! System cannot proceed without the location information.');
        //} else {
          b_obtained_gps = true;
        //}
      } else if (getLocation()) {
        b_obtained_gps = true;
      }
      return b_obtained_gps;
    }

    window.mobileAndTabletcheck = function() {
      (function(a){if(/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino|android|ipad|playbook|silk/i.test(a)||/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0,4)))check = true})(navigator.userAgent||navigator.vendor||window.opera);
      return check;
    }

    function hostReachable() {

      // Handle IE and more capable browsers
      var xhr = new ( window.ActiveXObject || XMLHttpRequest)( "Microsoft.XMLHTTP" );
      var status;

      // Open new request as a HEAD to the root hostname with a random param to bust the cache
      xhr.open( "HEAD", "//" + window.location.hostname + "?rand=" + Math.floor((1 + Math.random()) * 0x10000), false);

      // Issue request and handle response
      try {
        xhr.send();
        //return ( xhr.status >= 200 && (xhr.status < 300 || xhr.status === 304));
        $('#btn-submit').removeClass('btn-success btn-danger').addClass('btn-success');
        return true;
      } catch (error) {
        $('#btn-submit').removeClass('btn-success btn-danger').addClass('btn-danger');
        return false;
      }
    }

    /**
     * Function to check for actno if exists on local storage
     */
    function actnoExistsOnLocalStorage() {
      var qa_count = 1;
      var found = false;
      $.each($('.QA'), function(obj) {
        if (storage.getItem('lpg_' + actno + ':' + inc + '.QA_' + ('000' + qa_count.toString()).slice(-3))) {
          found = true;
        }
        ++qa_count;
      });
      return found;
    }

    /**
     * Function to extract Activity Number (actno) and Increment Number (inc)
     */
    function extractActNoAndInc(str) {
      var temp_str = str;
      var array_act_no = temp_str.split(':');
      var temp_act_no = '';
      var temp_inc = '';
      if (array_act_no.length > 1) {
        temp_act_no = array_act_no[0];
        temp_inc = array_act_no[1];
      } else {
        temp_act_no = array_act_no[0];
      }
      return Array(temp_act_no, temp_inc);
    }

    /**
     * Show dialog
     */
    function showDialogbox() {
      var exjob = $('#ex_gilbarco_veeder_root_lpg').val();
      if (storage.getItem('sys.ex_gilbarco_veeder_root_lpg') != null) {
        exjob = storage.getItem('sys.ex_gilbarco_veeder_root_lpg');
      }
      var glist = '<li class="qx qx_01 list-group-item list-group-item-success">Blank Form&nbsp;'
                + '<input type="checkbox" class="cqx cqx_01 floatright noborder" value="-1" checked /></li>';
      if (exjob != '-1') {
        var arr = exjob.split(',');
        for (i = 1; i < arr.length; i++) {
          var array_act_no = extractActNoAndInc(arr[i]);
          var temp_actno = array_act_no[0];
          var temp_inc = array_act_no[1];
          var display_act_no = temp_actno;
          if (temp_inc) {
            display_act_no = display_act_no + ':' + temp_inc;
          }
          glist += '<li class="qx qx_' + ('00' + (i + 1)).substr(-2) + ' list-group-item list-group-item-success">'
                  + display_act_no + '&nbsp;<input type="checkbox" class="cqx cqx_' + ('00' + (i + 1)).substr(-2)
                  + ' floatright noborder" value="' + temp_actno + ':' + temp_inc + '" /></li>';
        }
      }
      diag = $('<div id="fra"><div id="list-group-fra"><ul class="list-group">' + glist
                + '</ul></div><div class="floatright loadjob-buttons"><button id="one" class="btn btn-success ra_size">Remove</button>&nbsp;'
                + '<button id="two" class="btn btn-success ra_size">Load</button>&nbsp;<div class="loading"><img src="images/utilities/ajax-loader.gif"/> Loading please wait...</div></div></div>')
               .dialog({
                    modal: true,
                    height: 400,
                    width: 600,
                    closeOnEscape: false,
                    open: function(event, ui) { $('.ui-dialog-titlebar-close', ui.dialog | ui).hide(); },
                    id: 'mam',
                    css: {'padding' : '0px', 'spacing' : '0px'},
                    title: 'Maintain Form Data'
                });

      $('.qx').click(function(e) {
        var classList = $(this).attr('class').split(/\s+/);
        $.each($('.cqx'),function(obj) {
          this.checked = false;
        });
        $('.c' + classList[1])[0].checked = true;
      });

      $('#one').click(function(e) {

        $.each($('.cqx'),function(obj) {
          if (this.checked == true && $(this).val() != '-1') {
            var array_act_no = extractActNoAndInc($(this).val());
            var temp_actno = array_act_no[0];
            var temp_inc = array_act_no[1];
            removeJob(temp_actno, temp_inc, true);
          }
        });
        diag.dialog('close');
        location.reload();
      });

      $('#two').click(function(e) {
        $('.loading').css('visibility', 'visible');
        $.each($('.cqx'),function(obj) {
          if (this.checked == true) {
            //var cList = $(this).attr('class').split(/\s+/);
            var array_act_no = extractActNoAndInc($(this).val());
            actno = array_act_no[0];
            inc = array_act_no[1];

            if (actno == '-1') {
              diag.dialog('close');
              jdiag = $('<div id="jobnos"><div id="dialog-error-msg">&nbsp;</div><input type="text" id="jobans" value="" class="ra_text"/>&nbsp;'
                        + '<button id="jok" class="btn btn-success">OK</button></div>')
              .dialog({
                modal : true,
                closeOnEscape : false,
                open : function(event, ui) { $('.ui-dialog-titlebar-close', ui.dialog | ui).hide(); $('#jobans').focus(); },
                height : 180,
                width : 620,
                id :'jobdiag',
                css : {'padding' : '0px', 'spacing' : '0px'},
                title : 'Client Order/Job No.'
              });

              $('#jok').click(function(e) {
                if ($('#jobans').val().length > 2) {
                  var array_act_no = extractActNoAndInc($('#jobans').val());
                  actno = array_act_no[0];
                  inc = array_act_no[1];
                  if (actnoExistsOnLocalStorage()) {
                    $('#dialog-error-msg').html('Activity number / Job number "' + $('#jobans').val() + '" has already existed. Please try again!');
                    $('#dialog-error-msg').show();
                    $('#jobans').focus();
                  } else {
                    $('#dialog-error-msg').html('');
                    $('#dialog-error-msg').hide();
                    clearVals();
                    addJob();
                    $('.QA_279').text($('#jobans').val());
                    saveVal($('.QA_279'), '');
                    jdiag.dialog('close');
                    if ($('.QA_279').text().trim() == '') {
                      invalidGVRJobNumberOccurred();
                    } else {
                      $('.QA_003').focus();
                    }
                  }
                }
              });

              $('#jobans').keypress(function (e) {
                if (e.which == 13) {
                  $('#jok').trigger('click');
                  return false;
                }
              });
            } else {
              if (storage.getItem('lpg_' + actno + ':' + inc + '.QA_002') == actno) {
                loadVals();
              } else {
                clearVals();
              }
              diag.dialog('close');
              if ($('.QA_279').text().trim() == '') {
                invalidGVRJobNumberOccurred();
              } else {
                $('.QA_003').focus();
              }
            }
            $('.QA_002').val(actno);
            $('.IncrementNumber').val(inc);
            storage.setItem('lpg_' + actno + ':' + inc + '.QA_002', actno);
            storage.setItem('lpg_' + actno + ':' + inc + '.IncrementNumber', inc);
            return ;
          }
        });
      });
    }

    function clearSignature() {

      //  var canvas = document.getElementsByClassName('jSignature');
      //  var context = canvas.getContext('2d');
      $('.jSignature').each(function( index ) {
        //var context = $(this).find("canvas").get(0).getContext("2d");
        var context = this.getContext('2d');
        context.clearRect(0, 0, this.width, this.height);
      });

      $('#verifer-sign-dummy').val('');

      $('#verifer-sign').removeClass('warning');

      saveVal($('#verifer-sign'), '');

    }

    function slideReportTable(value) {

      $('#table-test-reports-menu').slideDown( 'slow', 'swing' );

      table = document.getElementById('table-test-reports-menu');

      // Remove any last thick bar
      var tr = document.getElementById('table-test-reports-menu-thick-bar');
      if (tr != null) {
        tr.parentNode.removeChild(tr);
      }
      var tr = document.getElementById('row-last-2');
      if (tr != null) {
        tr.parentNode.removeChild(tr);
        var tr = document.getElementById('row-last-1b');
        tr.parentNode.removeChild(tr);
      }
      var tr = document.getElementById('row-last-1a');
      if (tr != null) {
        tr.parentNode.removeChild(tr);
      }

      if (value == 'with-switch') {

        $('#table-test-report-header').slideDown( 'slow', 'swing' );
        $('#table-test-report-without-switch').slideUp( 'slow', 'swing' );
        $('#table-test-report-with-switch').slideDown( 'slow', 'swing' );

        var rowCount = table.rows.length;
        var tr = table.insertRow(rowCount);

        tr.id = 'row-last-1a';
        var td = tr.insertCell(0);
        td.colSpan = 3;
        td.innerHTML ='<label for="">E<sub>C</sub>    [(V<sub>FD15</sub> - V<sub>FD,C</sub>) / V<sub>FD,C</sub> * 100]</label>';

        $('#p-note').html('The correction factor MF<sub>MM</sub> is the meter factor obtained from a traceable measurement report.');
      } else {
        $('#table-test-report-header').slideDown( 'slow', 'swing' );
        $('#table-test-report-with-switch').slideUp( 'slow', 'swing' );
        $('#table-test-report-without-switch').slideDown( 'slow', 'swing' );

        var rowCount = table.rows.length;
        var tr = table.insertRow(rowCount);

        tr.id = 'row-last-2';
        var td = tr.insertCell(0);
        td.colSpan = 3;
        td.innerHTML = '<label for="">&nbsp;</label>';

        var rowCount = table.rows.length;
        var tr = table.insertRow(rowCount);

        tr.id = 'row-last-1b';
        var td = tr.insertCell(0);
        td.colSpan = 3;
        td.innerHTML = '<label for="">E<sub>C</sub>    [(E<sub>AV,C</sub> - E<sub>AV</sub>) / V<sub>REF</sub> * 100]</label>';

        $('#p-note').html(
          'The correction factor MF<sub>MM</sub> is the meter factor obtained from a traceable measurement report.<br/>'
          + 'For compensated delivery the conversion factor C<sub>tIFD</sub> must be set to unity.'
          );
      }

      var rowCount = table.rows.length;
      var tr = table.insertRow(rowCount);
      tr.id = 'table-test-reports-menu-thick-bar';
      tr.className = 'thick-bar';
      td = tr.insertCell(0);
      td.colSpan = 3;

      $('.precise-link').show();
      //$('#section-scroller a:last').show();
    }

    /**
     * disable controls
     */
    function disableControls() {

      $('#form-footer button').prop('disabled', true);

      $.each($('.QA'), function(obj) {

        p_array = $(this).attr('class').split(/\s+/);
        p_var = p_array[1];
        var p_tag = $('.' + p_var).prop('tagName');
        switch (p_tag) {
          case 'DIV':
            if (p_var == 'QA_142') {
              $('.jSignature').hide();
            } else {
              $('.' + p_var).removeAttr('contenteditable');
              $('.' + p_var).attr('disabled', true);
              $('.' + p_var).attr('readonly', true);
            }
            break;
          case 'SPAN':
            $('.' + p_var).prop('disabled', true);
            $('.' + p_var).unbind('click');
            break;
          case 'INPUT':
            $('.' + p_var).prop('disabled', true);
            break;
          case 'SELECT':
            $('.' + p_var).prop('disabled', true);
        }

      });
    }

    /**
     * Validate form inputs
     */
    function validateFormInputs() {

      var has_error = false;
      var $div_item;
      var p_tag;
      var in_arr = [];
      var p_array = Array();

      $('#frm').find('#submit-message').remove();
      $('#frm').find('#submit-message-wrapper').remove();
      $.each($('.QA'), function(obj) {
        p_array = $(this).attr('class').split(/\s+/);
        p_var = p_array[1];
        if (jQuery.inArray(p_var, in_arr) < 0) {
          p_tag = $('.' + p_var).prop('tagName');
          if (p_tag == 'SPAN') {
            $div_item = $('.' + p_var).parent().parent();
          } else {
            $div_item = $('.' + p_var).parent();
          }
          $div_item.removeClass('warning');
          $('.' + p_var).removeClass('warning');
          if ($div_item.parent().parent().attr('id') == 'general-characteristics-questions') {
            if ($('.QA_006').hasClass('clsyesOn')) {
              if (($div_item.hasClass('required')) && ( ! $.trim(getVal(p_var)))) {
                $div_item.addClass('warning');
                has_error = true;
              }
            }
          } else {
            //if (p_var == 'QA_276' && $('#verifer-sign').jSignature('getData', 'native').length == 0) {
            if (p_var == 'QA_276' &&  ! $('#verifer-sign-dummy').val()) {
              $('.' + p_var).addClass('warning');
              has_error = true;
            } else if (($div_item.hasClass('required')) && ( ! $.trim(getVal(p_var)))) {
              if ($('.' + p_var).prop('type') == 'radio') {
                $('.' + p_var).parent().addClass('warning');
              } else {
                $('.' + p_var).addClass('warning');
              }
              has_error = true;
            }
          }
          in_arr.push(p_var);
        }
      });

      if (has_error) {
        $('<div id="submit-message-wrapper">&nbsp;</div>').insertBefore('#form-head');
        $('<div></div>')
            .attr({
                'id' : 'submit-message',
                'class' : 'container warning'
            })
            .append('The form contains error(s). Please correct the following field(s) and submit again.<br/><br/>')
            .insertAfter('#submit-message-wrapper');
        return false;
      } else {
        return true;
      }
    }

    /**
     * GVR Job Number not found thus disabled the form
     */
    function invalidGVRJobNumberOccurred() {
      disableControls();
      $('#frm').find('#submit-message').remove();
      $('#frm').find('#submit-message-wrapper').remove();
      $('.QA_279').addClass('warning');
      $('<div id="submit-message-wrapper">&nbsp;</div>').insertBefore('#form-head');
      $('<div></div>')
          .attr({
              'id' : 'submit-message',
              'class' : 'container warning'
          })
          .append('The required GVR Job Number cannot be found. Form has been disabled.<br/><br/>')
          .insertAfter('#submit-message-wrapper');

      $('html, body').animate({
          scrollTop: $('#submit-message-wrapper').offset().top
        }, 2000);
    }

    /**
     * Below are functions for local storage
     */

    function clearVals() {

      // Clear all input controls
      var p_array = Array();
      $.each($('.QA'), function(obj) {
        //class_attr = $(this).attr('class');
        //array_class_attr = class_attr.split(' ');
        p_array = $(this).attr('class').split(/\s+/);
        p_var = p_array[1];

        var p_tag = $('.' + p_var).prop('tagName');
        switch (p_tag) {
          case 'DIV':
            if (p_var == 'QA_276') {
              clearSignature();
            } else {
              $('.' + p_var).text('');
            }
            break;
          case 'SPAN':
            $('.' + p_var + '.clsno').removeClass('clsnoOn clsno').addClass('clsno');
            $('.' + p_var + '.clsna').removeClass('clsnaOn clsna').addClass('clsna');
            $('.' + p_var + '.clsyes').removeClass('clsyesOn clsyes').addClass('clsyes');
            break;
          case 'LABEL':
            if (p_var == 'QA_255' || p_var == 'QA_256' || p_var == 'QA_257') {
              $('.' + p_var).text('1.000');
            } else {
              if ($('.' + p_var).hasClass('no-decimal')) {
                $('.' + p_var).text('0');
              } else if ($('.' + p_var).hasClass('two-decimal')) {
                $('.' + p_var).text('0.00');
              } else {
                $('.' + p_var).text('0.0000');
              }
            }
            break;
          case 'INPUT':
            var p_type = $('.' + p_var).prop('type');
            if (p_type == 'text' || p_type == 'number' || p_type == 'hidden') {
              if ($('.' + p_var).hasClass('date-picker')) {
                if (p_var != 'QA_028') {
                  $('.' + p_var).datepicker('setDate', new Date());
                } else {
                  $('.' + p_var).val('');
                }
                saveVal($('#' + $('.' + p_var).attr('id')), '');
              } else {
                $('.' + p_var).val('');
              }
            } else {
              // Radio button or checkbox
              $('input[name="' + $('.' + p_var).attr('name') + '"]').prop('checked', false);
            }
        }
      });

      $('#inspection').hide();
      $('#table-test-reports-menu').slideUp( '0.01' );
      $('#table-test-report-without-switch').slideUp( '0.01' );
      $('#table-test-report-with-switch').slideUp( '0.01' );

      hookupSignature();

      return true;
    }

    function getVal(p_var) {
      var val = '';

      var p_tag = $('.' + p_var).prop('tagName');
      if (p_tag == 'DIV') {
        if (p_var == 'QA_276') {
          val = $('#verifer-sign').jSignature('getData');
        } else {
          val = $('.' + p_var).text();
        }
      } else if (p_tag == 'SPAN') {
        if ($('.' + p_var).hasClass('clsyesOn')) {
          if ($('.' + p_var).hasClass('clspass')) {
            val = 'Pass';
          } else {
            val = 'Yes';
          }
        } else if ($('.' + p_var).hasClass('clsnoOn')) {
          if ($('.' + p_var).hasClass('clsfail')) {
            val = 'Fail';
          } else {
            val = 'No';
          }
        } else if ($('.' + p_var).hasClass('clsnaOn')) {
          val = 'N/A';
        }
      } else if (p_tag == 'LABEL') {
        val = $('.' + p_var).html();
      } else if (p_tag == 'INPUT') {
        var p_type = $('.' + p_var).prop('type');
        if (p_type == 'text' || p_type == 'number' || p_type == 'hidden') {
          val = $('.' + p_var).val();
        } else {
          // Radio button or checkbox
          val = $('input[name="' + $('.' + p_var).attr('name') + '"]:checked').val();
        }
      }

      return val;
    }

    function loadVals() {

      is_loading = true;

      var p_var = '';
      var p_array = Array();

      $.each($('.QA'), function(obj) {
        p_array = $(this).attr('class').split(/\s+/);
        p_var = p_array[1];
        val = storage.getItem('lpg_' + actno + ':' + inc + '.' + p_var);
        var p_tag = $('.' + p_var).prop('tagName');

        switch (p_tag) {
          case 'DIV':
            if (p_var == 'QA_276') {
              //$('#verifer-sign').jSignature("importData", 'data:base30');
              //$('#verifer-sign').jSignature('reset').jSignature('setData', val);
              // Check for null
              clearSignature();
              var rawsign = $('#verifer-sign').jSignature('getData');
              if (val != rawsign) {
                try {
                  $('#verifer-sign').jSignature('importData', val);
                  $('#verifer-sign-dummy').val('1');
                  saveVal($('.' + p_var), '');
                } catch(err) {
                  // Do nothing
                }
              }
            } else {
              if (val) {
                $('.' + p_var).attr('data-ph', '');
                $('.' + p_var).val(val);
                $('.' + p_var).html(val);
              }
            }
            break;
          case 'SPAN':
            switch (val) {
              case 'Yes':
              case 'Pass':
                $('.' + p_var + '.clsno').removeClass('clsnoOn clsno').addClass('clsno');
                $('.' + p_var + '.clsna').removeClass('clsnaOn clsna').addClass('clsna');
                $('.' + p_var + '.clsyes').removeClass('clsyesOn clsyes').trigger('click').addClass('clsyes clsyesOn');
                break;
              case 'No':
              case 'Fail':
                $('.' + p_var + '.clsyes').removeClass('clsyesOn clsyes').addClass('clsyes');
                $('.' + p_var + '.clsna').removeClass('clsnaOn clsna').addClass('clsna');
                $('.' + p_var + '.clsno').removeClass('clsnoOn clsno').trigger('click').addClass('clsno clsnoOn');
                break;
              case 'N/A':
                $('.' + p_var + '.clsno').removeClass('clsnoOn clsno').addClass('clsno');
                $('.' + p_var + '.clsyes').removeClass('clsyesOn clsyes').addClass('clsyes');
                $('.' + p_var + '.clsna').removeClass('clsnaOn clsna').trigger('click').addClass('clsna clsnaOn');
            }
            break;
          case 'LABEL':
            if (p_var == 'QA_255' || p_var == 'QA_256' || p_var == 'QA_257') {
              $('.' + p_var).text('1.000');
              saveVal($('.' + p_var), '');
            } else {
              if (val) {
                $('.' + p_var).text(val);
              } else {
                if ($('.' + p_var).hasClass('no-decimal')) {
                  $('.' + p_var).text('0');
                } else if ($('.' + p_var).hasClass('two-decimal')) {
                  $('.' + p_var).text('0.00');
                } else {
                  $('.' + p_var).text('0.0000');
                }
                saveVal($('.' + p_var), '');
              }
            }
            break;
          case 'INPUT':
            var unit = '';
            var p_type = $('.' + p_var).prop('type');
            if (p_type == 'text' || p_type == 'number' || p_type == 'hidden') {
              $('.' + p_var).val(val);
            } else {
              // Radio button or checkbox
              $('input[name="' + $('.' + p_var).attr('name') + '"][value="' + val + '"]').prop('checked', 'checked').trigger("click");
            }
        }

      });

      // Load slider value
      var val = storage.getItem('gilbarconmi_vk_opacity');
      if (val) {
        //$slider = $('#slider');
        $('#slider').slider('value', parseInt(val));
        //$('#slider').slider('option', 'change').call($('#slider'));
      };

      is_loading = false;

      return true;
    }

    function saveVal(obj, val) {

      if (actno) {
        var p_var = '';
        var p_array = Array();

        if (typeof(obj) == 'object') {
          p_array = obj.attr('class').split(/\s+/);
          p_var = p_array[1];
        } else {
          p_var = obj;
        }
        if (val) {
          storage.setItem('lpg_' + actno + ':' + inc + '.' + p_var, val);
        } else {
          storage.setItem('lpg_' + actno + ':' + inc + '.' + p_var, getVal(p_var));
        }
      }
    }

    function saveVals() {

      $.each($('.QA'), function(obj) {
        p_array = $(this).attr('class').split(/\s+/);
        p_var = p_array[1];
        saveVal(p_var, '');
      });

      // Save slider value
      storage.setItem('gilbarconmi_vk_opacity', $('#slider').slider('value'));

    }

    function addJob() {
      var found = false;
      var i = 0;
      var str = '';
      var tmparray = Array();

      if (storage.getItem('sys.ex_gilbarco_veeder_root_lpg') != null) {
        str = storage.getItem('sys.ex_gilbarco_veeder_root_lpg');
        tmparray = str.split(',');
        while (i < tmparray.length && ! found) {
          var temp_array_act_no = extractActNoAndInc(tmparray[i]);
          if (temp_array_act_no[0] == actno && temp_array_act_no[1] == inc) {
            found = true;
          } else {
            ++i;
          }
        }
        if ( ! found) {
          $('#ex_gilbarco_veeder_root_lpg').val(storage.getItem('sys.ex_gilbarco_veeder_root_lpg') + ',' + actno + ':' + inc);
        }
      } else {
        str = $('#ex_gilbarco_veeder_root_lpg').val();
        tmparray = str.split(',');
        while (i < tmparray.length && ! found) {
          var temp_array_act_no = extractActNoAndInc(tmparray[i]);
          if (temp_array_act_no[0] == actno && temp_array_act_no[1] == inc) {
            found = true;
          } else {
            ++i;
          }
        }
        if ( ! found) {
         $('#ex_gilbarco_veeder_root_lpg').val($('#ex_gilbarco_veeder_root_lpg').val() + ',' + actno + ':' + inc);
        }
      }
      storage.setItem('sys.ex_gilbarco_veeder_root_lpg', $('#ex_gilbarco_veeder_root_lpg').val());
      $('.QA_002').val(actno);
      $('.IncrementNumber').val(inc);
      storage.setItem('lpg_' + actno + ':' + inc + '.QA_002', actno);
      storage.setItem('lpg_' + actno + ':' + inc + '.IncrementNumber', inc);
    }

    function removeJob(val_1, val_2, remove_all) {

      var tmp_actno_ = actno;
      var tmp_inc_ = inc;
      actno = val_1;
      inc = val_2;
      clearVals();
      actno = tmp_actno_;
      inc = tmp_inc_;
      if (remove_all) {
        actlist = storage.getItem('sys.ex_gilbarco_veeder_root_lpg').replace(',' + val_1 + ':' + val_2, '');
        storage.setItem('sys.ex_gilbarco_veeder_root_lpg', actlist);
        $('#ex_gilbarco_veeder_root_lpg').val(actlist);
      }
      for (var key in storage) {
        var tmp_array_act_no = extractActNoAndInc(key);
        var tmp_array_act_no_1 = tmp_array_act_no[1].split('.');
        if ('lpg_' + val_1 == tmp_array_act_no[0] && val_2 == tmp_array_act_no_1[0]) {
          if (( ! remove_all) && (key == actno + ':' + inc + '.QA_002')) {
            // Do nothing
          } else {
            storage.removeItem(key);
          }
        }
      }
    }

    function afterSubmit(ans) {
      var arr = ans.split('|');
      if (arr[0] == 'OK') {
        alert('Submit completed, your reference no. is ' + arr[1]);

        clearVals();
        removeJob(actno, inc, true);
        // Generate and show Excel sheet
        //window.open(arr[3]);
        // Generate and show PDF
        //window.open(arr[4]);
        //alert("Please check pop up for both output's Excel sheet and PDF are enabled on your browser.");
        //document.location = arr[2] + '/index.php';
        document.location = 'http://www.verified.com.au/fp/Rules.php';
      } else {
        alert('Submit problem, please try again later!');
      }
      return ;
    }

    //binding keydown event on the contenteditable div
    $('div.QA').keydown(function(e){ check_charcount($(this).attr('id'), 254, e); });
    function check_charcount(content_id, max, e) {
      if(e.which != 8 && $('#'+content_id).text().length > max) {
         e.preventDefault();
      }
    }

    /**
     * Below are functions for test report's calculations
     */

    /**
     * Function to truncate number with decimal places
     */
    Number.prototype.toFixedDown = function(digits) {
      var factor_number = 1;
      if (parseFloat(this.toString()) < 0) {
        factor_number = -1;
      }
      var re = new RegExp("(\\d+\\.\\d{" + digits + "})(\\d)"),
          m = this.toString().match(re);
      return m ? parseFloat(m[1]) * factor_number : this.valueOf();
    };

    function get_object_id_pattern(obj) {

      // Object ID's segment position by separator character '-'
      var start_seq = 1;
      var id = '';
      var array = obj.id.split('-');

      if (array[0] == 'tbl2') {
        start_seq = 2;
      }
      // Get the display object's ID
      for (var j = start_seq; j < array.length; j++) {
        id += '-' + array[j];
      }

      return id;
    }

    function calculate_totaliser() {

      var totaliser_end = parseFloat($('#totaliser-end').val());
      if (isNaN(totaliser_end)) {
        totaliser_end = 0;
      }
      var totaliser_start = parseFloat($('#totaliser-start').val());
      if (isNaN(totaliser_start)) {
        totaliser_start = 0;
      }

      $('#total-vol-used').text(totaliser_end - totaliser_start);
      saveVal($('#total-vol-used'), '');
    }

    function calculate_vref(obj) {
      // Formula: Vref = Vmm * Ctimm * Cpimm * MFmm

      var array = obj.id.split('-');
      var prefix = '';
      if (array[0] == 'tbl2') {
        prefix = 'tbl2-';
      }
      var id = get_object_id_pattern(obj);

      var vmm = parseFloat($('#' + prefix + 'vmm' + id).val());
      if (isNaN(vmm)) {
        vmm = 0;
      }
      var mfmm = parseFloat($('#' + prefix + 'mfmm' + id).val());
      if (isNaN(mfmm)) {
        mfmm = 0;
      }
      var ctimm = parseFloat($('#' + prefix + 'ctimm' + id).val());
      if (isNaN(ctimm)) {
        ctimm = 0;
      }
      var cpimm = parseFloat($('#' + prefix + 'cpimm' + id).val());
      if (isNaN(cpimm)) {
        cpimm = 0;
      }

      vref = vmm * mfmm * ctimm * cpimm;
      vref = vref.toFixedDown(2);


      // Update label
      $('#' + prefix + 'vref' + id).text(vref.toFixed(2));

      saveVal($('#' + prefix + 'vref' +id), '');

      calculate_vfdc(obj)
      calculate_efd(prefix, id);
      calculate_eav_and_ec();
      calculate_ec();

    }

    function sum_tdf_minus_tfdi(data, prefix) {
/*
      // Formula: tfd - tfdi

      // Initialise
      var sum = 0;

      // Object ID's segment position by separator character '-'
      var start_seq = 1;
      if (prefix) {
        start_seq = 2;
      }

      var tdfi = parseFloat($('#temperature-displayed-by-dispenser').val());
      if (isNaN(tdfi)) {
        tdfi = 0;
      }

      // Lopp through each column
      $.each(data, function(i) {

        var tfd = parseFloat($('#' + data[i]).val());
        if (isNaN(tfd)) {
          tfd = 0;
        }
        // Apply formula
        sum = tfd - tdfi;
        //sum = sum.toFixedDown(4);

        // Get the display object's ID
        var id = prefix + 'tfdfdi';
        var array = data[i].split('-');
        for (var j = start_seq; j < array.length; j++) {
          id += '-' + array[j];
        }

        // Update label
        $('#' + id).text(sum.toFixed(1));

        saveVal($('#' + id), '');

      });
*/
    }

    function calculate_vfdc(obj) {
      // Formula: Vfdc = Vfd * Ctifd

      var array = obj.id.split('-');
      var prefix = '';
      if (array[0] == 'tbl2') {
        prefix = 'tbl2-';
      }
      var id = get_object_id_pattern(obj);

      var vfdc = 0;
      var vfd = 0;
      var ctifd = 0;

      if (prefix) {
        if (array[3] == 'compensated') {
          vfd = parseFloat($('#' + prefix + 'vfd15' + id).val());
          ctifd = parseFloat($('#' + prefix + 'ctifd' + id).text());
        } else {
          vfd = parseFloat($('#' + prefix + 'vfd' + id).val());
          ctifd = parseFloat($('#' + prefix + 'ctifd' + id).val());
        }
      } else {
        vfd = parseFloat($('#' + prefix + 'vfd' + id).val());
        ctifd = parseFloat($('#' + prefix + 'ctifd' + id).val());
      }
      if (isNaN(vfd)) {
        vfd = 0;
      }
      if (isNaN(ctifd)) {
        ctifd = 0;
      }

      vfdc = vfd * ctifd;
      vfdc = vfdc.toFixedDown(2);

      // Update label
      $('#' + prefix + 'vfdc' + id).text(vfdc.toFixed(2));
      saveVal($('#' + prefix + 'vfdc' + id), '');
      calculate_efd(prefix, id);
      calculate_eav_and_ec();
      calculate_ec();

    }

    function calculate_efd(prefix, id) {

      if (prefix + 'efd' + id != 'efd-delivery-compensated-switch') {
        // Formula: [(Vfdc - Vref) / Vref * 100]
        var vfdc = parseFloat($('#' + prefix + 'vfdc' + id).text());
        if (isNaN(vfdc)) {
          vfdc = 0;
        }
        var vref = parseFloat($('#' + prefix + 'vref' + id).text());
        if (isNaN(vref)) {
          vref = 0;
        }

        var sum = ((vfdc - vref) * 100) / vref;
        if (isNaN(sum)) {
          sum = 0;
        }
        sum = sum.toFixedDown(2);

        // Update label
        $('#' + prefix + 'efd' + id).text(sum.toFixed(2));
        saveVal($('#' + prefix + 'efd' + id), '');
      }
    }

    function calculate_ec() {

      // Formula: [(Vfd15 - Vfdc) / Vfdc * 100]
      var vfd15 = parseFloat($('#vfd15-delivery-compensated-switch').val());
      if (isNaN(vfd15)) {
        vfd15 = 0;
      }
      var vfdc = parseFloat($('#vfdc-delivery-compensated-switch').text());
      if (isNaN(vfdc)) {
        vfdc = 0;
      }
      var sum = ((vfd15 - vfdc) * 100) / vfdc;
      if (isNaN(sum)) {
        sum = 0;
      }
      sum = sum.toFixedDown(2);

      // Update label
      $('#ec-compensated-switch').text(sum.toFixed(2));

      saveVal($('#ec-compensated-switch'), '');
    }

    function calculate_eav_and_ec() {
      // Calculate Eav
      // Formula: x = [(a + b + c) / 3]
      var tbl2_efd_delivery_qmax_1 = parseFloat($('#tbl2-efd-delivery-qmax-1').text());
      if (isNaN(tbl2_efd_delivery_qmax_1)) {
        tbl2_efd_delivery_qmax_1 = 0;
      }
      var tbl2_efd_delivery_qmax_2 = parseFloat($('#tbl2-efd-delivery-qmax-2').text());
      if (isNaN(tbl2_efd_delivery_qmax_2)) {
        tbl2_efd_delivery_qmax_2 = 0;
      }
      var tbl2_efd_delivery_qmax_3 = parseFloat($('#tbl2-efd-delivery-qmax-3').text());
      if (isNaN(tbl2_efd_delivery_qmax_3)) {
        tbl2_efd_delivery_qmax_3 = 0;
      }
      var sum = (tbl2_efd_delivery_qmax_1 + tbl2_efd_delivery_qmax_2 + tbl2_efd_delivery_qmax_3) / 3;
      //sum = sum.toFixedDown(2);

      // Update label
      $('#tbl2-eav').text(sum.toFixed(2));

      saveVal($('#tbl2-eav'), '');

      // Calculate Eavc
      // Formula: y = [(a + b + c) / 3]
      var tbl2_efd_compensated_delivery_qmax_1 = parseFloat($('#tbl2-efd-delivery-compensated-qmax-1').text());
      if (isNaN(tbl2_efd_compensated_delivery_qmax_1)) {
        tbl2_efd_compensated_delivery_qmax_1 = 0;
      }
      var tbl2_efd_compensated_delivery_qmax_2 = parseFloat($('#tbl2-efd-delivery-compensated-qmax-2').text());
      if (isNaN(tbl2_efd_compensated_delivery_qmax_2)) {
        tbl2_efd_compensated_delivery_qmax_2 = 0;
      }
      var tbl2_efd_compensated_delivery_qmax_3 = parseFloat($('#tbl2-efd-delivery-compensated-qmax-3').text());
      if (isNaN(tbl2_efd_compensated_delivery_qmax_3)) {
        tbl2_efd_compensated_delivery_qmax_3 = 0;
      }
      var sum = (tbl2_efd_compensated_delivery_qmax_1 + tbl2_efd_compensated_delivery_qmax_2 + tbl2_efd_compensated_delivery_qmax_3) / 3;
      //sum = sum.toFixedDown(2);

      // Update label
      $('#tbl2-eavc').text(sum.toFixed(2));

      saveVal($('#tbl2-eavc'), '');

      // Calculate Ec
      // Formula: y - x
      var eav = parseFloat($('#tbl2-eav').text());
      if (isNaN(eav)) {
        eav = 0;
      }
      var eavc = parseFloat($('#tbl2-eavc').text());
      if (isNaN(eavc)) {
        eavc = 0;
      }
      var sum = eavc - eav;
      //sum = sum.toFixedDown(2);

      // Update label
      $('#tbl2-ec-compensated-switch').text(sum.toFixed(2));

      saveVal($('#tbl2-ec-compensated-switch'), '');
    }

    /**
     * Below are functions for virtual keyboard
     */

    $( init );
    function init() {
      $('#virtual-keyboard').draggable({
        start: function(event, ui) {

        },
        stop: function(event, ui) {
          // Show dropped position
          vk_moved = true;
        }
      });
    }

    String.prototype.toUnicode = function () {
      var uni = [],
        i = this.length;
      while (i--) {
        uni[i] = this.charCodeAt(i);
      }
      return "&#" + uni.join(';&#') + ";";
    };

    function showCustomVirtualKeyboardInstantly(show) {
      if ($('#virtual-keyboard').hasClass('hide')) {
        $('.numerical-input').removeAttr('readonly');
        $('#' + current_node_id).blur();
        //var evt = document.getElementById($('.' + current_node_id).attr('id'));
        //evt.setSelectionRange($('.' + current_node_id).val().length, $('.' + current_node_id).val().length);
        //evt.focus();
        $('#' + current_node_id).focus();
      } else {
        if (show) {
          $('.numerical-input').attr('readonly','readonly');
          var half_screen_left = $(window).width() / 2;
          if ( ! vk_moved) {
            var p = $('#' + current_node_id).offset();
            if (p.left > half_screen_left) {
              $('#virtual-keyboard').css('top', ($(window).scrollTop() + 150) + 'px');
              $('#virtual-keyboard').css('left', '100px');
            } else {
              $('#virtual-keyboard').css('top', ($(window).scrollTop() + 50) + 'px');
              $('#virtual-keyboard').css('left', (half_screen_left + 100) + 'px');
            }
          } else {
            $('#virtual-keyboard').css('top', ($(window).scrollTop() + 150) + 'px');
          }
          $('#virtual-keyboard').show();
        } else {
          //$('.numerical-input').removeAttr('readonly');
          $('#virtual-keyboard').hide();
          $('#' + current_node_id).blur();
          //var evt = document.getElementById($('.' + current_node_id).attr('id'));
          //evt.setSelectionRange($('.' + current_node_id).val().length, $('.' + current_node_id).val().length);
          //evt.focus();
          $('#' + current_node_id).focus();
        }
      }
    }

    function showCustomVirtualKeyboard(show) {
      if ($('#virtual-keyboard').hasClass('hide')) {
          $('.numerical-input').removeAttr('readonly');
        //$('#' + current_node_id).focus();
      } else {
        if (show) {
          var half_screen_left = $(window).width() / 2;
          $('.numerical-input').attr('readonly','readonly');
          if ( ! vk_moved) {
            var p = $('#' + current_node_id).offset();
            if (p.left > half_screen_left) {
              $('#virtual-keyboard').css('top', ($(window).scrollTop() + 150) + 'px');
              $('#virtual-keyboard').css('left', '100px');
            } else {
              $('#virtual-keyboard').css('top', ($(window).scrollTop() + 50) + 'px');
              $('#virtual-keyboard').css('left', (half_screen_left + 100) + 'px');
            }
          } else {
            $('#virtual-keyboard').css('top', ($(window).scrollTop() + 150) + 'px');
          }
          $('#virtual-keyboard').show();
        } else {
          //$('.numerical-input').removeAttr('readonly');
          $('#virtual-keyboard').hide();
          //$('#' + current_node_id).focus();
        }
      }
    }

    function hookVirtualKeyboard(obj) {

      if (obj.hasClass('imask')) {
        showCustomVirtualKeyboard(true);
        $('#btn-keyboard-swap').show();
        $('#btn-keyboard-swap').offset({ top: $(window).scrollTop() + 50, left: $(window).scrollLeft() + 100});
        drag_drop_initial = false;
      } else {
        showCustomVirtualKeyboard(false);
        $('#btn-keyboard-swap').hide();
      }
    }

    function vkFindNextVisibleInput(n_next_pclass) {
      var need_scroll_to = false;

      if ($('input[name=test-report-for-lpg-dispensers]:checked').val() == 'with-switch') {
        if (n_next_pclass > 160) {
          n_next_pclass = 268;
          need_scroll_to = true;
        }
      } else {
        if (n_next_pclass > 265) {
          n_next_pclass = 268;
          need_scroll_to = true;
        }
      }
      return Array(n_next_pclass, need_scroll_to);
    }
    function vkTraverseTextboxFocusInRange(n_next_pclass, fowarding) {
      if (fowarding && (n_next_pclass <= 279)) {
        return true;
      } else if ( ! fowarding && (n_next_pclass >= 1)) {
        return true;
      }
      return false;
    }
    function vkTraveseTextboxIncrement(n_next_pclass, fowarding) {
      var found = false;
      var found2 = false;
      if (fowarding) {
        n_next_pclass = n_next_pclass + 1;
        while ( ! found2 && vkTraverseTextboxFocusInRange(n_next_pclass, fowarding)) {
          if ($('[tabindex="' + n_next_pclass + '"]').attr('class') != null) {
            found2 = true;
          } else {
            n_next_pclass = n_next_pclass + 1;
          }
        }
      } else {
        n_next_pclass = n_next_pclass - 1;
        while ( ! found2 && vkTraverseTextboxFocusInRange(n_next_pclass, fowarding)) {
          if ($('[tabindex="' + n_next_pclass + '"]').attr('class') != null) {
            found2 = true;
          } else {
            n_next_pclass = n_next_pclass - 1;
          }
        }
      }
      var array_return = vkFindNextVisibleInput(n_next_pclass);
      n_next_pclass = array_return[0];
      var need_scroll_to = array_return[1];
      var p_array = $('[tabindex="' + n_next_pclass + '"]').attr('class').split(/\s+/);
      var next_pclass = p_array[1];
      var p_tag = $('.' + next_pclass).prop('tagName');
      switch (p_tag) {
        case 'DIV':
          found = true;
          break;
        case 'SELECT':
          found = true;
          break;
        case 'INPUT':
          var p_type = $('.' + next_pclass).prop('type');
          if (p_type == 'text' || p_type == 'number') {
            found = true;
          }
          break;
        case 'TEXTAREA':
          found = true;
      }
      return Array(found, n_next_pclass, next_pclass, need_scroll_to);
    }
    function vkTraverseTextboxFocus(fowarding) {
      var n_next_pclass = $('#' + current_node_id).attr('tabindex');
      var array_return = vkTraveseTextboxIncrement(parseFloat(n_next_pclass), fowarding)
      var found = array_return[0];
      var n_next_pclass = array_return[1];
      var next_pclass = array_return[2];
      var need_scroll_to = array_return[3];
      while ( ! found && vkTraverseTextboxFocusInRange(n_next_pclass, fowarding)) {
        array_return = vkTraveseTextboxIncrement(n_next_pclass, fowarding)
        found = array_return[0];
        n_next_pclass = array_return[1];
        next_pclass = array_return[2];
        need_scroll_to = array_return[3];
      }
      current_node_id = $('.' + next_pclass).attr('id');
      if (need_scroll_to) {
        $('html, body').animate({
            scrollTop: $('#' + current_node_id).offset().top
          }, 2000);
      }
      $('#' + current_node_id).focus();
    }

    function checkInnstantCalculation() {
      if (current_node_id == 'totaliser-end' || current_node_id == 'totaliser-start') {
        calculate_totaliser();
      }
      if ($('#'  + current_node_id).parent().hasClass('vref-factor')) {
        calculate_vref($('#' + current_node_id)[0]);
      }
      if ($('#'  + current_node_id).parent().hasClass('vfdc-factor')) {
        calculate_vfdc($('#' + current_node_id)[0]);
      }
      if (current_node_id == 'vfd15-delivery-compensated-switch') {
        calculate_ec();
      }
    }

    function updateSlider(val) {
      $('#virtual-keyboard').css('background-color', 'rgba(217, 237, 247, ' + (val * 0.1) + ')');
      $('#virtual-keyboard ul li').css('background-color', 'rgba(204, 204, 204, ' + (val * 0.1) + ')');
      storage.setItem('gilbarconmi_vk_opacity', val);
    }

    $(window).scroll(function() {
      $('#btn-keyboard-swap').offset({ top: $(window).scrollTop() + 50, left: $(window).scrollLeft() + 100});
      var p = $('#virtual-keyboard').offset();
      var half_screen_left = $(window).width() / 2;
      if (p.left > half_screen_left) {
        $('#virtual-keyboard').offset({ top: parseFloat($(window).scrollTop()) + 50,
          left: p.left});
      } else {
        $('#virtual-keyboard').offset({ top: parseFloat($(window).scrollTop()) + 150,
          left: p.left});
      }
    });
  </script>

</body>
</html><?php
/* End of file index-internal.php */
/* Location: ./index-internal.php */